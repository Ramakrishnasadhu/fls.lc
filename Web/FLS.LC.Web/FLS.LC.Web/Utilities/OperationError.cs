﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Utilities
{
    public class OperationError
    {
        public OperationError()
        {

        }
        public string Details { get; set; }

        public OperationError(string details) => (Details) = (details);
    }

    public class OperationErrorTemp
    {
        public string details { get; set; }

    }
}

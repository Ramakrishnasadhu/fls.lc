﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Utilities
{
    public static class OperationErrorDictionary
    {
        public static class User
        {
            public static OperationError UserInValid() =>
               new OperationError("Unfortunately the user specified is In-Valid User! or may be inactive.");

            public static OperationError UserDoesNotExist() =>
               new OperationError("Unfortunately the user specified in the database does not exist.");

            public static OperationError UserRegistrationStatus() =>
               new OperationError("Email Sent Successfully.");

        }
    }
}

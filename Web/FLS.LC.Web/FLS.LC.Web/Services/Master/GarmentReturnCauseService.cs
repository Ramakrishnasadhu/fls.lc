﻿using FLS.LC.Web.Extensions;
using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using FLS.LC.Web.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master
{
    public class GarmentReturnCauseService : IGarmentReturnCause
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _iWebHostEnvironment;
        public GarmentReturnCauseService(HttpClient httpClient, IConfiguration configuration, IWebHostEnvironment iWebHostEnvironment)
        {
            _httpClient = httpClient;
            _configuration = configuration;
            _iWebHostEnvironment = iWebHostEnvironment;
        }
        public async Task<List<GarmentReturnCauseResponseDto>> GetRegisteredGarmentReturnCauseAsync()
        {
            try
            {
                var response = await _httpClient.GetAsync("GarmentReturnCause/GetGarmentReturnCause");
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                    }
                }
                else if (response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.NoContent)
                    {
                        return new List<GarmentReturnCauseResponseDto>();
                    }
                    else
                    {
                        return await response.ReadContentAs<List<GarmentReturnCauseResponseDto>>();
                    }
                }
                {

                }
                return null;
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }

        public async Task<bool> RegisterGarmentReturnCauseAsync(GarmentReturnCausePostDto garmentReturnCausePostDto)
        {
            var garmentReturnCausePost = new GarmentReturnCausePostDto()
            {
                garmentReturnCauseOldValue = garmentReturnCausePostDto.garmentReturnCauseOldValue,
                garmentReturnCauseNewValue = garmentReturnCausePostDto.garmentReturnCauseNewValue,
                GrecId = garmentReturnCausePostDto.GrecId
            };
            try
            {
                var response = await _httpClient.PostAsJson("GarmentReturnCause/GarmentReturnCause", garmentReturnCausePost);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                        return false;
                    }
                }
                else if (response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.NoContent)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }
    }
}

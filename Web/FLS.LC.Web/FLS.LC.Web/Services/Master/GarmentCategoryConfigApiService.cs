﻿using FLS.LC.Web.Extensions;
using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using FLS.LC.Web.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master
{
    public class GarmentCategoryConfigApiService : IGarmentCategoryConfig
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _iWebHostEnvironment;
        public GarmentCategoryConfigApiService(HttpClient httpClient, IConfiguration configuration, IWebHostEnvironment iWebHostEnvironment)
        {
            _httpClient = httpClient;
            _configuration = configuration;
            _iWebHostEnvironment = iWebHostEnvironment;
        }
        public async Task<OperationResponse> GarmentCategoryRegisterAsync(GarmentsCategoryDto GarmentsCategoryDto)
        {
            try
            {
                var response = await _httpClient.PostAsJson("GarmentCategory/ProcessGarmentCategory", GarmentsCategoryDto);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                        return new OperationResponse().SetAsFailureResponse(operationError);
                    }
                }
                return new OperationResponse();
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }

        public async Task<IReadOnlyCollection<string>> RegisteredGarmentsListCategoryAsync(GarmentCategoryDto GarmentCategoryDto)
        {
            var response = await _httpClient.PostAsJson("GarmentCategory/RegisteredGarmentCategory", GarmentCategoryDto);

            if (response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    return new List<string>().AsReadOnly();
                }
                else
                {
                    return await response.ReadContentAs<List<string>>();
                }
            }
            return null;
        }
        public async Task<IReadOnlyCollection<string>> RegisteredGarmentServiceListAsync(GarmentCategoryDto GarmentCategoryDto)
        {
            var response = await _httpClient.PostAsJson("GarmentCategory/RegisteredGarmentListService", GarmentCategoryDto);

            if (response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    return new List<string>().AsReadOnly();
                }
                else
                {
                    return await response.ReadContentAs<List<string>>();
                }
            }
            return null;
        }

        public async Task<GarmentspriceConfigurationReadDto> RetriveSelectedGarmentCategoryAsync(RequiredGarmentDto requiredGarmentDto)
        {
            var response = await _httpClient.PostAsJson("GarmentCategory/GetRequiredGarment", requiredGarmentDto);

            if (response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    return new GarmentspriceConfigurationReadDto();
                }
                else
                {
                    return await response.ReadContentAs<GarmentspriceConfigurationReadDto>();
                }
            }
            return null;
        }

        public async Task<IReadOnlyCollection<string>> SearchRegisteredGarmentServiceListAsync(GarmentCategoryDto GarmentCategoryDto)
        {
            var response = await _httpClient.PostAsJson("GarmentCategory/SearchRegisteredGarmentNamesListService", GarmentCategoryDto);

            if (response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    return new List<string>().AsReadOnly();
                }
                else
                {
                    return await response.ReadContentAs<List<string>>();
                }
            }

            return null;
        }

        }
}
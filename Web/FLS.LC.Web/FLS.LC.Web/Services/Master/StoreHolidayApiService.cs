﻿using FLS.LC.Web.Extensions;
using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using FLS.LC.Web.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master
{
    public class StoreHolidayApiService : IStoreHoliday
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _iWebHostEnvironment;
        public StoreHolidayApiService(HttpClient httpClient, IConfiguration configuration, IWebHostEnvironment iWebHostEnvironment)
        {
            _httpClient = httpClient;
            _configuration = configuration;
            _iWebHostEnvironment = iWebHostEnvironment;
        }
        public async Task<List<StoreHolidayResponseDto>> GetRegisteredHolidayAsync(string userEmail)
        {
            try
            {
                var UserStoreHolidayDto = new StoreHolidayRequestDto()
                {
                    UsthUsremail = userEmail,
                };
                var response = await _httpClient.PostAsJson("StoreHoliday/GetStoreHoliday", UserStoreHolidayDto);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                    }
                }
                else if (response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.NoContent)
                    {
                        return new List<StoreHolidayResponseDto>();
                    }
                    else
                    {
                        return await response.ReadContentAs<List<StoreHolidayResponseDto>>();
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }

        public async Task<List<StoreHolidayResponseDto>> GetStoreRegisteredHolidayAsync(string userEmail, string userStore)
        {
            try
            {
                var UserStoreHolidayDto = new StoreHolidayRequestDto()
                {
                    UsthUsremail = userEmail,
                    UsthUstname = userStore
                };
                var response = await _httpClient.PostAsJson("StoreHoliday/GetStoreHolidayByStore", UserStoreHolidayDto);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                    }
                }
                else if (response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.NoContent)
                    {
                        return new List<StoreHolidayResponseDto>();
                    }
                    else
                    {
                        return await response.ReadContentAs<List<StoreHolidayResponseDto>>();
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }

        public async Task<bool> PostStoreWeekOffHolidayAsync(StoreWeekOffHolidayRequestDto storeWeekOffHolidayRequestDto)
        {
            try
            {
                var UserStoreWeekOffHolidayRequestDto = new StoreWeekOffHolidayRequestDto()
                {
                    UsthUsremail = storeWeekOffHolidayRequestDto.UsthUsremail,
                    UsthUstname = storeWeekOffHolidayRequestDto.UsthUstname,
                    UsthWeekOffHoliday = storeWeekOffHolidayRequestDto.UsthWeekOffHoliday
                };
                var response = await _httpClient.PostAsJson("StoreHoliday/PostStoreWeekOffHoliday", UserStoreWeekOffHolidayRequestDto);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                    }
                }
                else if (response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.NoContent)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                {

                }
                return false;
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }
        public async Task<bool> PostStoreHolidayAsync(StoreHolidayRequestDto storeHolidayRequestDto)
        {
            try
            {
                var UserStoreHolidayRequestDto = new StoreHolidayRequestDto()
                {
                    UsthUsremail = storeHolidayRequestDto.UsthUsremail,
                    UsthUstname = storeHolidayRequestDto.UsthUstname,
                    UsthHolidayDate = storeHolidayRequestDto.UsthHolidayDate,
                    UsthHolidayDescription = storeHolidayRequestDto.UsthHolidayDescription
                };
                var response = await _httpClient.PostAsJson("StoreHoliday/AddStoreHoliday", UserStoreHolidayRequestDto);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                    }
                }
                else if (response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.NoContent)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                {

                }
                return false;
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }

        public async Task<StoreHolidayWeekOffResponseDto> GetStoreWeekOffHolidayAsync(string userEmail, string userStore)
        {
            try
            {
                var UserStoreHolidayDto = new StoreHolidayRequestDto()
                {
                    UsthUsremail = userEmail,
                    UsthUstname = userStore
                };
                var response = await _httpClient.PostAsJson("StoreHoliday/GetStoreWeekOffHolidayAsync", UserStoreHolidayDto);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                    }
                }
                else if (response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.NoContent)
                    {
                        return new StoreHolidayWeekOffResponseDto();
                    }
                    else
                    {
                        return await response.ReadContentAs<StoreHolidayWeekOffResponseDto>();
                    }
                }
                {

                }
                return null;
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }

    }
}

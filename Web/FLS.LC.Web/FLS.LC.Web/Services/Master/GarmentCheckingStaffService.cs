﻿using FLS.LC.Web.Extensions;
using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using FLS.LC.Web.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master
{
    public class GarmentCheckingStaffService : IGarmentCheckingStaffConfig
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _iWebHostEnvironment;
        public GarmentCheckingStaffService(HttpClient httpClient, IConfiguration configuration, IWebHostEnvironment iWebHostEnvironment)
        {
            _httpClient = httpClient;
            _configuration = configuration;
            _iWebHostEnvironment = iWebHostEnvironment;
        }

        public async Task<List<GarmentCheckingStaffRequestDto>> GetRegisteredGarmentRemarksAsync()
        {
            try
            {
                var response = await _httpClient.GetAsync("GarmentCheckingStaff/GetRegisteredGarmentCheckingStaff");
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                    }
                }
                else if (response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.NoContent)
                    {
                        return new List<GarmentCheckingStaffRequestDto>();
                    }
                    else
                    {
                        return await response.ReadContentAs<List<GarmentCheckingStaffRequestDto>>();
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }


        public async Task<bool> RegisterGarmentCheckingStaffAsync(GarmentCheckingStaffRequestDto GarmentCheckingStaffRequestDto)
        {
            try
            {
                var response = await _httpClient.PostAsJson("GarmentCheckingStaff/GarmentCheckingStaff", GarmentCheckingStaffRequestDto);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                    }
                }
                else if (response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.NoContent)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }
    }
}

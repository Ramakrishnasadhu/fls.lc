﻿using FLS.LC.Web.Extensions;
using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using FLS.LC.Web.Utilities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master
{
    public class StoreApiService : IStoreApiService
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;

        public StoreApiService(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _configuration = configuration;
        }
        public async Task<IReadOnlyCollection<UserStoreDetailDtotemp>> SelectedStoreAsync(string UserEmail, string usrStoreName)
        {
            var UserStoreDto = new UserStoreDto()
            {
                UsrEmail = UserEmail,
                UstStoreName = usrStoreName
            };
            var response = await _httpClient.PostAsJson("Store/SelectedStore", UserStoreDto);

            if (response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    return new List<UserStoreDetailDtotemp>().AsReadOnly();
                }
                else
                {
                    return await response.ReadContentAs<List<UserStoreDetailDtotemp>>();
                }
            }
            return null;
        }

        public async Task<IReadOnlyCollection<UserStoreDetailDtotemp>> RegisteredStoresAsync(string UserEmail)
        {
            var UserStoreDto = new UserStoreDto()
            {
                UsrEmail = UserEmail
            };
            var response = await _httpClient.PostAsJson("Store/RegisteredStores", UserStoreDto);

            if (response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    return new List<UserStoreDetailDtotemp>().AsReadOnly();
                }
                else
                {
                    return await response.ReadContentAs<List<UserStoreDetailDtotemp>>();
                }
            }
            return null;
        }
        public async Task<OperationResponse> InActivateSelectedStore(string usrEmail, string usrStoreName)
        {
            try
            {
                var UserStoreDto = new UserStoreDto()
                {
                    UsrEmail = usrEmail,
                    UstStoreName = usrStoreName
                };
                var response = await _httpClient.PostAsJson("Store/InActivateSelectedStore", UserStoreDto);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                        return new OperationResponse().SetAsFailureResponse(operationError);
                    }
                }
                return new OperationResponse();
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }
        public async Task<OperationResponse> StoreRegisterAsync(viewUserStore viewUserStore)
        {
            try
            {
                MultipartFormDataContent multipartFormDataContent = new MultipartFormDataContent();

                StreamContent streamContent = new StreamContent(viewUserStore.UserStore.UstLogo.OpenReadStream());
                multipartFormDataContent.Add(streamContent, "LogoImage", viewUserStore.UserStore.UstLogo.FileName);

                if (viewUserStore.UserStore.UstCode == null)
                    multipartFormDataContent.Add(new StringContent("1"), "Operation");
                else
                {
                    multipartFormDataContent.Add(new StringContent("2"), "Operation");
                    multipartFormDataContent.Add(new StringContent(viewUserStore.UserStore.UstCode), "UstCode");
                }
                multipartFormDataContent.Add(new StringContent(viewUserStore.UserStore.UsrEmail), "UsrEmail");
                multipartFormDataContent.Add(new StringContent(viewUserStore.UserStore.UstWebsiteUrl), "UstWebsiteUrl");
                multipartFormDataContent.Add(new StringContent(viewUserStore.UserStore.UstName), "UstName");
                multipartFormDataContent.Add(new StringContent(viewUserStore.UserStore.UstBusinessName), "UstBusinessName");
                multipartFormDataContent.Add(new StringContent(viewUserStore.UserStore.UstAddress), "UstAddress");
                multipartFormDataContent.Add(new StringContent(Convert.ToString(viewUserStore.UserStore.UstMobileNo)), "UstMobileNo");
                multipartFormDataContent.Add(new StringContent(viewUserStore.UserStore.UstEmail), "UstEmail");
                multipartFormDataContent.Add(new StringContent(viewUserStore.UserStore.UstAreaLocation), "UstAreaLocation");
                multipartFormDataContent.Add(new StringContent(viewUserStore.UserStore.UstCity), "UstCity");
                multipartFormDataContent.Add(new StringContent(viewUserStore.UserStore.UstState), "UstState");
                multipartFormDataContent.Add(new StringContent(Convert.ToString(viewUserStore.UserStore.UstOpeningTime)), "UstOpeningTime");
                multipartFormDataContent.Add(new StringContent(viewUserStore.UserStore.UstClosingTime), "UstClosingTime");
                multipartFormDataContent.Add(new StringContent(viewUserStore.UserStore.UstBusinessSlogan), "UstBusinessSlogan");
                multipartFormDataContent.Add(new StringContent(viewUserStore.UserStore.UstWeekOff), "UstWeekOff");

                var response = await _httpClient.PostAsync("Store/AddEditDelete", multipartFormDataContent);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                        return new OperationResponse().SetAsFailureResponse(operationError);
                    }
                }
                return new OperationResponse();
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }



    }
}

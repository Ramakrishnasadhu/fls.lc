﻿using FLS.LC.Web.Extensions;
using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using FLS.LC.Web.Utilities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master
{
    public class PriceListConfigApiService : IPriceListConfig
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;
        public PriceListConfigApiService(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _configuration = configuration;
        }
        public async Task<OperationResponse> priceListImportAsync(GarmentspriceConfigurationDto GarmentspriceConfigurationDto)
        {
            try
            {
                MultipartFormDataContent multipartFormDataContent = new MultipartFormDataContent();
                StreamContent streamContent = new StreamContent(GarmentspriceConfigurationDto.GpcImprtPriceList.OpenReadStream());
                multipartFormDataContent.Add(streamContent, "GpcImprtPriceList", GarmentspriceConfigurationDto.GpcImprtPriceList.FileName);
                multipartFormDataContent.Add(new StringContent(GarmentspriceConfigurationDto.GpcUstname), "GpcUstname");
                multipartFormDataContent.Add(new StringContent(GarmentspriceConfigurationDto.GpcUsremail), "GpcUsremail");
                _httpClient.Timeout = TimeSpan.FromMinutes(60);
                var response = await _httpClient.PostAsync("GarmentConfig/ImportGarmentsPriceList", multipartFormDataContent);

                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                        return new OperationResponse().SetAsFailureResponse(operationError);
                    }
                }
                return new OperationResponse();
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }

        public async Task<updateRegisteredPriceListByStoreDto> updateRegisteredPriceListByStore(string usrEmail, string usrStoreName, bool isIncrementChecked, bool isDecrementChecked, string pricePercentage)
        {
            var updateRegisteredPriceListByStoreDto = new updateRegisteredPriceListByStoreDto()
            {
                usrEmail = usrEmail,
                usrStoreName = usrStoreName,
                isDecrementChecked = isDecrementChecked,
                isIncrementChecked = isIncrementChecked,
                pricePercentage = pricePercentage
            };
            var response = await _httpClient.PostAsJson("GarmentConfig/updateRegisteredPriceListByStore", updateRegisteredPriceListByStoreDto);
            if (response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    return new updateRegisteredPriceListByStoreDto();
                }
                else
                {
                    return await response.ReadContentAs<updateRegisteredPriceListByStoreDto>();
                }
            }
            return null;

        }
        public async Task<IReadOnlyCollection<string>> GetRegisteredPriceListByStoreAsync(string usrEmail, string usrStoreName)
        {
            var GarmentListDto = new GarmentListDto()
            {
                GlcUsremail = usrEmail,
                GlcUsrstore = usrStoreName
            };
            var response = await _httpClient.PostAsJson("GarmentConfig/RegisteredPriceListByStore", GarmentListDto);
            if (response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    return new List<string>().AsReadOnly();
                }
                else
                {
                    return await response.ReadContentAs<List<string>>();
                }
            }
            return null;

        }
    }
}

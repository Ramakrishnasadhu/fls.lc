﻿using FLS.LC.Web.Extensions;
using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using FLS.LC.Web.Utilities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master
{
    public class StickerConfigurationApiService : IStickerConfiguration
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;
        public StickerConfigurationApiService(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _configuration = configuration;
        }
        public async Task<IReadOnlyCollection<StickerConfigurationDto>> SelectedStoreStickerConfigDetailsAsync(string UserEmail, string usrStoreName)
        {
            var UserStoreStickerDto = new UserStoreDto()
            {
                UsrEmail = UserEmail,
                UstStoreName = usrStoreName
            };
            var response = await _httpClient.PostAsJson("StickerConfig/SelectedStoreStickerConfig", UserStoreStickerDto);

            if (response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    return new List<StickerConfigurationDto>().AsReadOnly();
                }
                else
                {
                    return await response.ReadContentAs<List<StickerConfigurationDto>>();
                }
            }
            return null;
        }
        public async Task<List<string>> GetAllStoreStickerConfiConfigAsync(StickerConfigDto StickerConfigDto)
        {
            try
            {
                var response = await _httpClient.PostAsJson("StickerConfig/AllStickerConfig", StickerConfigDto);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }
                }
                return await response.ReadContentAs<List<string>>();
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }
        public async Task<OperationResponse> StickerConfigurationAsync(StickerConfigurationObject StickerConfigurationObject)
        {
            try
            {
                var response = await _httpClient.PostAsJson("StickerConfig/PostStickerConfig", StickerConfigurationObject);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                        return new OperationResponse().SetAsFailureResponse(operationError);
                    }
                }
                return new OperationResponse();
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }
    }
}

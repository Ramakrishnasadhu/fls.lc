﻿using FLS.LC.Web.Extensions;
using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using FLS.LC.Web.Utilities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master
{
    public class OrderConfigApiService : IOrderConfig
    {

        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;
        public OrderConfigApiService(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _configuration = configuration;
        }


        public async Task<IReadOnlyCollection<OrderScreenConfigurationDto>> SelectedStoreConfigDetailsAsync(string UserEmail, string usrStoreName)
        {
            var UserStoresOrderConfigDto = new UserStoresOrderConfigDto()
            {
                UsrEmail = UserEmail,
                 UstStoreName = usrStoreName
            };
            var response = await _httpClient.PostAsJson("OrderConfig/SelectedStoreOrderConfigDetails", UserStoresOrderConfigDto);

            if (response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    return new List<OrderScreenConfigurationDto>().AsReadOnly();
                }
                else
                {
                    return await response.ReadContentAs<List<OrderScreenConfigurationDto>>();
                }
            }
            return null;
        }

        public async Task<IReadOnlyCollection<OrderScreenConfigurationDto>> GetAllStoreOrderConfigAsync(string UserEmail)
        {
            var UserStoresOrderConfigDto = new UserStoresOrderConfigDto()
            {
                UsrEmail = UserEmail,
            };
            var response = await _httpClient.PostAsJson("OrderConfig/GetAllStoreOrderConfig", UserStoresOrderConfigDto);

            if (response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    return new List<OrderScreenConfigurationDto>().AsReadOnly();
                }
                else
                {
                    return await response.ReadContentAs<List<OrderScreenConfigurationDto>>();
                }
            }
            return null;
        }

        public async Task<OperationResponse> PostOrderConfigAsync(OrderConfigDto OrderConfigDto)
        {
            try
            {
                var response = await _httpClient.PostAsJson("OrderConfig/AddEditDelete", OrderConfigDto);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                        return new OperationResponse().SetAsFailureResponse(operationError);
                    }
                }
                return new OperationResponse();
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }

    }
}

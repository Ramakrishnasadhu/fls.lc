﻿using FLS.LC.Web.Extensions;
using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using FLS.LC.Web.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master
{
    public class GarmentConfigApiService : IGarmentConfig
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _iWebHostEnvironment;

        public GarmentConfigApiService(HttpClient httpClient, IConfiguration configuration, IWebHostEnvironment iWebHostEnvironment)
        {
            _httpClient = httpClient;
            _configuration = configuration;
            _iWebHostEnvironment = iWebHostEnvironment;
        }
        public async Task<OperationResponse> GarmentRegisterAsync(GarmentslistConfigurationInput GarmentslistConfigurationInput)
        {
            try
            {
                MultipartFormDataContent multipartFormDataContent = new MultipartFormDataContent();
                string filename = GarmentslistConfigurationInput.GlcGarmentImage.ToString();
                string wwwRootPath = _iWebHostEnvironment.WebRootPath;
               // string filename = Path.GetFileName(imagePath);
                byte[] file = File.ReadAllBytes(wwwRootPath+ @"\images\Garments\" + filename);
                Stream stream = new MemoryStream(file);
                StreamContent streamContent = new StreamContent(stream);
                multipartFormDataContent.Add(streamContent, "GlcGarmentImage", filename);
                multipartFormDataContent.Add(new StringContent(GarmentslistConfigurationInput.GlcUstname), "GlcUstname");
                multipartFormDataContent.Add(new StringContent(GarmentslistConfigurationInput.GlcUsremail), "GlcUsremail");
                multipartFormDataContent.Add(new StringContent(GarmentslistConfigurationInput.GlcGarmentName), "GlcGarmentName");
                multipartFormDataContent.Add(new StringContent(GarmentslistConfigurationInput.GlcGarmentServiceName), "GlcGarmentServiceName");
                multipartFormDataContent.Add(new StringContent(GarmentslistConfigurationInput.GlcGarmentShortName), "GlcGarmentShortName");
                multipartFormDataContent.Add(new StringContent(GarmentslistConfigurationInput.GlcGarmentMeasurementUnit), "GlcGarmentMeasurementUnit");
                multipartFormDataContent.Add(new StringContent(GarmentslistConfigurationInput.GlcGarmentCategory), "GlcGarmentCategory");
                multipartFormDataContent.Add(new StringContent(Convert.ToString(GarmentslistConfigurationInput.GlcGarmentNoOfGarments)), "GlcGarmentNoOfGarments");
                multipartFormDataContent.Add(new StringContent(Convert.ToString(GarmentslistConfigurationInput.GlcGarmentIsSingle)), "GlcGarmentIsSingle");
                multipartFormDataContent.Add(new StringContent(Convert.ToString(GarmentslistConfigurationInput.GlcGarmentIsInuse)), "GpcIsenable");
                var response = await _httpClient.PostAsync("GarmentConfig/PostGarment", multipartFormDataContent);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                        return new OperationResponse().SetAsFailureResponse(operationError);
                    }
                }
                return new OperationResponse();
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }

        public async Task<bool> UpdateServicePriceByGarmentAsync(UpdateServicePriceByGarmentDto updateServicePriceByGarmentDto)
        {
            {
                try
                {
                    var response = await _httpClient.PostAsJson("GarmentConfig/UpdateServicePriceByGarment", updateServicePriceByGarmentDto);
                    if (!response.IsSuccessStatusCode)
                    {
                        if (response.StatusCode == HttpStatusCode.InternalServerError)
                        {
                            throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                        }
                    }
                    else if (response.IsSuccessStatusCode)
                    {
                        if (response.StatusCode == HttpStatusCode.NoContent)
                        {
                            return false;
                        }
                        else
                        {

                            return true;
                        }
                    }
                    return false;
                }
                catch (Exception ex)
                {
                    throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
                }
            }
        }
        public async Task<IReadOnlyCollection<RegistredGarmentslistDto>> RegisteredGarmentsByStoreAsync(GarmentListDto GarmentListDto)
        {
            try
            {
                var response = await _httpClient.PostAsJson("GarmentConfig/AllRegisteredGarments", GarmentListDto);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }
                }
                else if(response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.NoContent)
                    {
                        return new List<RegistredGarmentslistDto>().AsReadOnly();
                    }
                    else
                    {
                        
                        return await response.ReadContentAs<List<RegistredGarmentslistDto>>();
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }
    }
}

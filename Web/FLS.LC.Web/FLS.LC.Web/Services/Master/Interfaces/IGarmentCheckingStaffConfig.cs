﻿using FLS.LC.Web.Models.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master.Interfaces
{
    public interface IGarmentCheckingStaffConfig
    {
        Task<List<GarmentCheckingStaffRequestDto>> GetRegisteredGarmentRemarksAsync();

        Task<bool> RegisterGarmentCheckingStaffAsync(GarmentCheckingStaffRequestDto GarmentCheckingStaffRequestDto);
    }
}

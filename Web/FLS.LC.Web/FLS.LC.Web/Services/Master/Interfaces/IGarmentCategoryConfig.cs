﻿using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master.Interfaces
{
    public interface IGarmentCategoryConfig
    {
        Task<IReadOnlyCollection<string>> SearchRegisteredGarmentServiceListAsync(GarmentCategoryDto GarmentCategoryDto);
        Task<OperationResponse> GarmentCategoryRegisterAsync(GarmentsCategoryDto GarmentsCategoryDto);

        Task<GarmentspriceConfigurationReadDto> RetriveSelectedGarmentCategoryAsync(RequiredGarmentDto requiredGarmentDto);
        Task<IReadOnlyCollection<string>> RegisteredGarmentsListCategoryAsync(GarmentCategoryDto GarmentCategoryDto);
        Task<IReadOnlyCollection<string>> RegisteredGarmentServiceListAsync(GarmentCategoryDto GarmentCategoryDto);


    }
}

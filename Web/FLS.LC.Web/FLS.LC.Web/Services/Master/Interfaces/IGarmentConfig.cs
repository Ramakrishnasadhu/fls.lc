﻿using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master.Interfaces
{
    public interface IGarmentConfig
    {
        Task<OperationResponse> GarmentRegisterAsync(GarmentslistConfigurationInput GarmentslistConfigurationInput);
        Task<IReadOnlyCollection<RegistredGarmentslistDto>> RegisteredGarmentsByStoreAsync(GarmentListDto GarmentListDto);

        Task<bool> UpdateServicePriceByGarmentAsync(UpdateServicePriceByGarmentDto updateServicePriceByGarmentDto);
        
    }
}

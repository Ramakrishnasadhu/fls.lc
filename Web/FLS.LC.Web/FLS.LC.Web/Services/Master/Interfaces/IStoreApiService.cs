﻿using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master.Interfaces
{
    public interface IStoreApiService
    {
        Task<IReadOnlyCollection<UserStoreDetailDtotemp>> RegisteredStoresAsync(string UserEmail);
        Task<IReadOnlyCollection<UserStoreDetailDtotemp>> SelectedStoreAsync(string usrEmail,string usrStoreName);
        Task<OperationResponse> InActivateSelectedStore(string usrEmail, string usrStoreName);
        Task<OperationResponse> StoreRegisterAsync(viewUserStore viewUserStore);
    }
}

﻿using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master.Interfaces
{
    public interface IGarmentRemark
    {
        Task<List<GarmentRemarkRequestDto>> GetRegisteredGarmentRemarksAsync();
        Task<bool> RegisterGarmentRemarkAsync(string garmentDescriptionOldValue,string garmentDescriptionNewValue,int garmentRemarkId);

    }
}

﻿using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master.Interfaces
{
    public interface IStickerConfiguration
    {
        Task<OperationResponse> StickerConfigurationAsync(StickerConfigurationObject StickerConfigurationObject);
        Task<List<string>> GetAllStoreStickerConfiConfigAsync(StickerConfigDto StickerConfigDto);
        Task<IReadOnlyCollection<StickerConfigurationDto>> SelectedStoreStickerConfigDetailsAsync(string usrEmail, string usrStoreName);
    }
}

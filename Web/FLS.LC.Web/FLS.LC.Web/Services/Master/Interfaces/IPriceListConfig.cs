﻿using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master.Interfaces
{
    public interface IPriceListConfig
    {
        Task<OperationResponse> priceListImportAsync(GarmentspriceConfigurationDto GarmentspriceConfigurationDto);
        Task<IReadOnlyCollection<string>> GetRegisteredPriceListByStoreAsync(string usrEmail, string usrStoreName);

        Task<updateRegisteredPriceListByStoreDto> updateRegisteredPriceListByStore(string usrEmail, string usrStoreName, bool isIncrementChecked, bool isDecrementChecked, string pricePercentage);
        


    }
}

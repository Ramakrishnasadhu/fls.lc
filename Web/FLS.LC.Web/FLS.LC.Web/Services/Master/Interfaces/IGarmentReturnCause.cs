﻿using FLS.LC.Web.Models.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master.Interfaces
{
    public interface IGarmentReturnCause
    {
        Task<List<GarmentReturnCauseResponseDto>> GetRegisteredGarmentReturnCauseAsync();
        Task<bool> RegisterGarmentReturnCauseAsync(GarmentReturnCausePostDto garmentReturnCausePostDto);
    }
}

﻿using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master.Interfaces
{
    public interface IOrderConfig
    {
        Task<IReadOnlyCollection<OrderScreenConfigurationDto>> GetAllStoreOrderConfigAsync(string usrEmail);
        Task<OperationResponse> PostOrderConfigAsync(OrderConfigDto OrderConfigDto);
        Task<IReadOnlyCollection<OrderScreenConfigurationDto>> SelectedStoreConfigDetailsAsync(string usrEmail, string usrStoreName);
    }
}

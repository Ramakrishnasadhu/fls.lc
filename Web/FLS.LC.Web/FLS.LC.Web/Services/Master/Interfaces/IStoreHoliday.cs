﻿using FLS.LC.Web.Models.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Master.Interfaces
{
    public interface IStoreHoliday
    {
        Task<List<StoreHolidayResponseDto>> GetStoreRegisteredHolidayAsync(string userEmail, string userStore);
        Task<List<StoreHolidayResponseDto>> GetRegisteredHolidayAsync(string userEmail);
        Task<StoreHolidayWeekOffResponseDto> GetStoreWeekOffHolidayAsync(string userEmail, string userStore);
        Task<bool> PostStoreHolidayAsync(StoreHolidayRequestDto storeHolidayRequestDto);
        Task<bool> PostStoreWeekOffHolidayAsync(StoreWeekOffHolidayRequestDto storeWeekOffHolidayRequestDto);
    }
}

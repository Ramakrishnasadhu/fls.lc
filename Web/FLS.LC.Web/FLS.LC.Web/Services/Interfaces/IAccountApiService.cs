﻿using FLS.LC.Web.Models;
using FLS.LC.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services.Interfaces
{
    public interface IAccountApiService
    {
        Task<OperationResponse> UserRegisterAsync(UserReg UserReg);
        Task<UserModel> RegisteredUsersAsync(int pageIndex);
        Task<OperationResponse> ForgotPasswordAsync(string userEmail);
        Task<OperationResponse> ChangePasswordAsync(string userEmail, string currentPassword, string newPassword);
        Task<OperationResponse> userLoginAsync(string userEmail,string password);
        Task<OperationResponse> userFirstLoginAsync(string userEmail);


    }
}

﻿using FLS.LC.Web.Extensions;
using FLS.LC.Web.Models;
using FLS.LC.Web.Services.Interfaces;
using FLS.LC.Web.Utilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace FLS.LC.Web.Services
{
    public class AccountApiService : IAccountApiService
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;

        public AccountApiService(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _configuration = configuration;
        }
        public async Task<UserModel> RegisteredUsersAsync(int pageIndex)
        {
            UserModel Users = new UserModel();
            Users.CurrentPageIndex = pageIndex;
            var response = await _httpClient.PostAsJson("Account/RegisteredUser", Users);

            return await response.ReadContentAs<UserModel>();
        }
        public async Task<OperationResponse> ChangePasswordAsync(string userEmail, string currentPassword, string newPassword)
        {
            try
            {
                UserModel changeUserPassword = new UserModel();
                changeUserPassword.UserEmail = userEmail;
                changeUserPassword.CurrentPassword = currentPassword;
                changeUserPassword.NewPassword = newPassword;

                var response = await _httpClient.PostAsJson("Account/ChangePassword", changeUserPassword);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                        return new OperationResponse().SetAsFailureResponse(operationError);
                    }
                }
                return new OperationResponse();
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }

        public async Task<OperationResponse> userFirstLoginAsync(string userEmail)
        {
            try
            {
                UserModel LoginUser = new UserModel();
                LoginUser.UserEmail = userEmail;
                var response = await _httpClient.PostAsJson("Account/FirstLogin", LoginUser);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                        return new OperationResponse().SetAsFailureResponse(operationError);
                    }
                }
                return new OperationResponse();
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }

        public async Task<OperationResponse> userLoginAsync(string userEmail, string password)
        {
            try
            {
                UserModel LoginUser = new UserModel();
                LoginUser.UserEmail = userEmail;
                LoginUser.Password = password;
                var response = await _httpClient.PostAsJson("Account/LoginUser", LoginUser);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                        return new OperationResponse().SetAsFailureResponse(operationError);
                    }
                }
                return new OperationResponse();
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }
        public async Task<OperationResponse> ForgotPasswordAsync(string userEmail)
        {
            try
            {
                UserModel LoginUser = new UserModel();
                LoginUser.UserEmail = userEmail;
                var response = await _httpClient.PostAsJson("Account/ForgotPassword", LoginUser);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                        return new OperationResponse().SetAsFailureResponse(operationError);
                    }
                }
                return new OperationResponse();
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }
        public async Task<OperationResponse> UserRegisterAsync(UserReg UserReg)
        {
            try
            {
                var response = await _httpClient.PostAsJson("Account/UserRegister", UserReg);
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        throw new ApplicationException($"Something went wrong calling the API: {response.ReasonPhrase}");
                    }

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var operationError = await response.ReadContentAs<OperationError>();
                        return new OperationResponse().SetAsFailureResponse(operationError);
                    }
                }
                return new OperationResponse();
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Something went wrong calling the API: {ex.Message}");
            }
        }
    }
}

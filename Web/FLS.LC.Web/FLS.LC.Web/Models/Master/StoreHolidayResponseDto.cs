﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class StoreHolidayResponseDto
    {
        public string UsthUsremail { get; set; }
        public string UsthUstname { get; set; }
        public string UsthHolidayDescription { get; set; }
        public string UsthHolidayDate { get; set; }
    }

    public class lstStoreHolidayResponseDto
    {
        public string usthUserStore { get; set; }
        public string UsthHolidayDescription { get; set; }
        public string UsthHolidayDate { get; set; }
        public bool ShowSuccessAlert { get; set; }
        public List<StoreHolidayResponseDto> lstStoreHoliday { get; set; }
        public StoreWeekOffHolidayRequestDto StoreWeekOffHolidayRequestDto { get; set; }
    }
    public class StoreHolidayRequestDto
    {
        public string UsthUsremail { get; set; }
        public string UsthUstname { get; set; }
        public string UsthHolidayDate { get; set; }
        public string UsthHolidayDescription { get; set; }
    }

    public class StoreWeekOffHolidayRequestDto
    {
        public string UsthUsremail { get; set; }
        public string UsthUstname { get; set; }
        public string UsthWeekOffHoliday { get; set; }
    }
    public class StoreHolidayWeekOffResponseDto
    {
        public string UsthUsremail { get; set; }
        public string UsthUstname { get; set; }
        public string UstWeekOffDay { get; set; }
    }
}

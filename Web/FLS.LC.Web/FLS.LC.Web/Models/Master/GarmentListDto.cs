﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class GarmentListDto
    {
        public string GlcUsremail { get; set; }
        public string GlcUsrstore { get; set; }
    }
}

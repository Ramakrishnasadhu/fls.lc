﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class GarmentspriceConfigurationReadDto
    {
        public long GpcId { get; set; }
        public string GpcUsremail { get; set; }
        public string GpcUstname { get; set; }
        public byte[] GpcImage { get; set; }
        public string GpcPriceListName { get; set; }
        public bool? GpcIsenable { get; set; }
        public string GpcGarmentCategory { get; set; }
        public string GpcGarmentName { get; set; }
        public string GpcGarmentCode { get; set; }
        public string GpcGarmentService { get; set; }
        public int? GpcGarmentServicePrice { get; set; }
        public string GpcGarmentMeasurementUnit { get; set; }
        public int? GpcNoOfGarments { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public string ImgDataURL { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class UserStoreDto
    {
        public string UsrEmail { get; set; }
        public string UstStoreName { get; set; }
    }
    public class viewUserStore
    {
        public UserStoreDetailDto UserStore { get; set; }
        public IReadOnlyCollection<UserStoreDetailDtotemp> UserStores { get; set; }
        public bool ShowSuccessAlert { get; set; }
        public string OperationFailureReason { get; set; }
        public string UserEmail { get; set; }

    }
}

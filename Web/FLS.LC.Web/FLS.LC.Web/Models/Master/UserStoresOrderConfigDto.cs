﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class UserStoresOrderConfigDto
    {
        public string UsrEmail { get; set; }
        public string UstStoreName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class GarmentReturnCauseResponseDto
    {
        public long GrecId { get; set; }
        public string GreReturnCauseDescription { get; set; }
    }

    public class lstGarmentReturnCauseResponseDto
    {
        public long GrecId { get; set; }
        public string GreReturnCauseDescription { get; set; }
        public bool ShowSuccessAlert { get; set; }
        public List<GarmentReturnCauseResponseDto> lstGarmentReturnCauseResponse { get; set; }
    }
}

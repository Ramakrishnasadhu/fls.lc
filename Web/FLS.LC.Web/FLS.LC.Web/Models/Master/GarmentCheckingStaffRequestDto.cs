﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class GarmentCheckingStaffRequestDto
    {
        public string GpcUsremail { get; set; }
        public string GpcUstname { get; set; }
        public string GcsEmployeeName { get; set; }
        public string GcsEmployeeAddress { get; set; }
        public long? GcsEmployeeMobileNo { get; set; }
        public string GcsEmployeeEmail { get; set; }
    }
}

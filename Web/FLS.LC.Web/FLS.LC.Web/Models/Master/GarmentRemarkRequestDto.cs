﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class GarmentRemarkRequestDto
    {
        public string GreRemarkDescription { get; set; }
        public int Gre_ID { get; set; }
    }

    public class GarmentRemarkPostRequestDto
    {
        public string garmentRemarkDescriptionOldValue { get; set; }
        public string garmentRemarkDescriptionNewValue { get; set; }
        public int garmentRemarkId { get; set; }
    }


    public class GarmentRemarkResponseDto
    {
       public List<GarmentRemarkRequestDto> lstGarmentRemarks { get; set; }
        public string GreRemarkDescription { get; set; }
        public bool ShowSuccessAlert { get; set; }
    }
}

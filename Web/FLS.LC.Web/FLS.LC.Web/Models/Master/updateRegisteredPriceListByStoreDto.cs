﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class updateRegisteredPriceListByStoreDto
    {
        public string usrEmail { get; set; }
        public string usrStoreName { get; set; }
        public bool isIncrementChecked { get; set; }
        public bool isDecrementChecked { get; set; }
        public string pricePercentage { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class StickerConfigurationDto
    {
        public long StcId { get; set; }

        public string StcUsremail { get; set; }
        public string StcUstname { get; set; }
        public string StcOption { get; set; }

        public bool StcPrintIsenable { get; set; }

        public string StcFontName { get; set; }

        public string StcTextAlign { get; set; }

        public int StcTextSize { get; set; }

        public string StcTextStyle { get; set; }
        public bool StcBoldStyle { get; set; }
        public bool StcItalicStyle { get; set; }
        public bool StcUnderlineStyle { get; set; }

        public DateTime? CreatedOn { get; set; }

        public long? CreatedBy { get; set; }

        public DateTime? DeletedOn { get; set; }

        public long? DeletedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public long? ModifiedBy { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class GarmentReturnCausePostDto
    {
        public string garmentReturnCauseOldValue { get; set; }
        public string garmentReturnCauseNewValue { get; set; }
        public int GrecId { get; set; }
    }
}

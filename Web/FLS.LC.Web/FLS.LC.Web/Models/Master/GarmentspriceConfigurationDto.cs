﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class GarmentspriceConfigurationDto
    {
        public string GpcUsremail { get; set; }

        public string GpcUstname { get; set; }

        public IFormFile GpcImprtPriceList { get; set; }

        public IReadOnlyCollection<UserStoreDetailDtotemp> UserStores { get; set; }
        public bool ShowSuccessAlert { get; set; }
        public string OperationFailureReason { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class GarmentslistConfigurationInput
    {
        public long GlcId { get; set; }
        public string GlcUsremail { get; set; }
        public string GlcUstname { get; set; }
        public string GlcGarmentName { get; set; }
        public string GlcGarmentShortName { get; set; }
        public string GlcGarmentServiceName { get; set; }
        public string GlcGarmentMeasurementUnit { get; set; }
        public string GlcGarmentCategory { get; set; }
        public long GlcGarmentNoOfGarments { get; set; }
        public string GlcGarmentImage { get; set; }
        public bool GlcGarmentIsSingle { get; set; }
        public bool GlcGarmentIsInuse { get; set; }
        public bool ShowSuccessAlert { get; set; }
        public string OperationFailureReason { get; set; }
        public string UserEmail { get; set; }
        public IReadOnlyCollection<RegistredGarmentslistDto> RegistredGarmentslistDto { get; set; }
    }

    public class RegistredGarmentslistDto
    {
        public long GpcId { get; set; }
        public string GpcUsremail { get; set; }
        public string GpcUstname { get; set; }
        public string GpcPriceListName { get; set; }
        public bool? GpcIsenable { get; set; }
        public int? GpcNoOfGarments { get; set; }
        public string GpcGarmentMeasurementUnit { get; set; }
        public string GpcGarmentCategory { get; set; }
        public string GpcGarmentName { get; set; }
        public string GpcGarmentService { get; set; }
        public Stream GlcGarmentImage { get; set; }
        public string GpcGarmentCode { get; set; }
        public int? GpcGarmentServicePrice { get; set; }
    }

    public class ListRegisteredGarmentsByStore
    {
        public string GlcUsremail { get; set; }
        public string GlcUstname { get; set; }
        public List<string> lstGarmentCategory { get; set; }
        public List<string> lstGarmentService { get; set; }
        public bool ShowSuccessAlert { get; set; }
        public List<RegisteredGarmentsDetailsByStore> lstRegisteredGarmentsDetailsByStore { get; set; }
    }
    public class RegisteredGarmentsDetailsByStore
    {
        public string GlcUsremail { get; set; }
        public string GlcUstname { get; set; }
        public string GarmentCategory { get; set; }
        public string GarmentName { get; set; }
        public int? GarmentServicePrice { get; set; }
        public bool ShowSuccessAlert { get; set; }
    }
    public class UpdateServicePriceByGarmentDto
    {
        public string UsrEmail { get; set; }
        public string UsrStoreName { get; set; }
        public int Updatedprice { get; set; }
        public string Garmentname { get; set; }
        public string Service { get; set; }
        public string Garment { get; set; }
    }
}

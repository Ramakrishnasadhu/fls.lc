﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class GarmentsCategoryDto
    {
        public int Operation { get; set; }
        public long GcId { get; set; }

        public string GcUsremail { get; set; }

        public string GcUstname { get; set; }

        public string GcGarmentCategory { get; set; }

        public DateTime? CreatedOn { get; set; }

        public long? CreatedBy { get; set; }

        public DateTime? DeletedOn { get; set; }

        public long? DeletedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public long? ModifiedBy { get; set; }
        public bool ShowSuccessAlert { get; set; }
        public string OperationFailureReason { get; set; }
        public string UserEmail { get; set; }

        public IReadOnlyCollection<RegisteredGarmentspriceConfigurationDto> RegistredGarmentsCategorylist { get; set; }
        public IReadOnlyCollection<string> RegistredGarmentsCategory { get; set; }
    }
}

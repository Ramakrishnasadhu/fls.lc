﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class OrderConfigDto
    {
        public OrderScreenConfigurationDto OrderScreenConfigurationDto { get; set; }
        public IReadOnlyCollection<OrderScreenConfigurationDto> OrderConfigurations { get; set; }
        public bool ShowSuccessAlert { get; set; }
        public string OperationFailureReason { get; set; }
        public string UserEmail { get; set; }
        public int Operation { get; set; }
    }
}

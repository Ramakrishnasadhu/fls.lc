﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class UserStoreDetailDto
    {
        public int Operation { get; set; }
        public string UstName { get; set; }
        [Display(Name = "Store Code")]
        public string UstCode { get; set; }
        public string UstBusinessName { get; set; }
        public string UstAddress { get; set; }
        public long? UstMobileNo { get; set; }
        public string UsrEmail { get; set; }
        public string UstEmail { get; set; }
        public string UstAreaLocation { get; set; }
        public string UstCity { get; set; }
        public string UstState { get; set; }
        public string UstCountry { get; set; }
        public string UstOpeningTime { get; set; }
        public string UstClosingTime { get; set; }
        public string UstBusinessSlogan { get; set; }
        public IFormFile UstLogo { get; set; }
        public bool? UstISdelete { get; set; }
        public string UstWebsiteUrl { get; set; }
        public bool? UstOperatingtime { get; set; }
        public string UstWeekOff { get;set; }

        
    }

    public class UserStoreDetailDtotemp
    {
        public int Operation { get; set; }
        public string UstName { get; set; }
        public string UstCode { get; set; }
        public string UstBusinessName { get; set; }
        public string UstAddress { get; set; }
        public long? UstMobileNo { get; set; }
        public string UsrEmail { get; set; }
        public string UstEmail { get; set; }
        public string UstAreaLocation { get; set; }
        public string UstCity { get; set; }
        public string UstState { get; set; }
        public string UstCountry { get; set; }
        public string UstOpeningTime { get; set; }
        public string UstClosingTime { get; set; }
        public string UstBusinessSlogan { get; set; }
        public byte[] UstLogo { get; set; }
        public bool? UstISdelete { get; set; }
        public string UstWebsiteUrl { get; set; }
        public bool? UstOperatingtime { get; set; }
        public string UstWeekOff { get; set; }
        public string ImageDataUrl { get; set; }
    }

}

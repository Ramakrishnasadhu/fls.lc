﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class OrderScreenConfigurationDto
    {
        public long OscId { get; set; }

        public string OscUsrEmail { get; set; }

        public long? UsrId { get; set; }

        public string OscStartOrderFromPrefix { get; set; }

        public string OscStartOrderFromPostfix { get; set; }

        public long? OscDeliveryDateOffset { get; set; }

        public int? OscDeliveryTimePrefix { get; set; }

        public string OscDeliveryTimePostfix { get; set; }

        public string OscOrderDefaultSearch { get; set; }

        public string OscUstName { get; set; }

        public string OscDiscountType { get; set; }

        public string OscOrderNetamountType { get; set; }

        public long? OscUrgentSameDayOrderRate { get; set; }

        public long? OscUrgentNextDayOrderRate { get; set; }

        public long? OscUrgentSameDayOrderDayOffset { get; set; }

        public long? OscUrgentNextDayOrderDayOffset { get; set; }

        public bool OscRule1 { get; set; }

        public bool OscRule2 { get; set; }

        public bool OscRule3 { get; set; }

        public bool OscRule4 { get; set; }

        public bool OscRule5 { get; set; }

        public bool OscRule6 { get; set; }

        public bool OscRule7 { get; set; }

        public bool OscRule8 { get; set; }

        public bool OscRule9 { get; set; }

        public bool OscRule10 { get; set; }

        public bool OscRule11 { get; set; }

        public bool OscRule12 { get; set; }

        public bool OscRule13 { get; set; }

        public DateTime? CreatedOn { get; set; }

        public long? CreatedBy { get; set; }

        public DateTime? DeletedOn { get; set; }

        public long? DeletedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public long? ModifiedBy { get; set; }
    }
}

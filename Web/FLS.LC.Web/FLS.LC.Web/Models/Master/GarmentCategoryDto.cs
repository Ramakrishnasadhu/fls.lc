﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class GarmentCategoryDto
    {
        public string GlcUsremail { get; set; }
        public string GlcUsrstore { get; set; }
    }

    public class RequiredGarmentDto
    {
        public string GlcUsremail { get; set; }
        public string GlcUsrstore { get; set; }
        public string GlcGarmentName { get; set; }
        public string GlcSelectedGarment { get; set; }
        public string GlcSelectedService { get; set; }
    }
}

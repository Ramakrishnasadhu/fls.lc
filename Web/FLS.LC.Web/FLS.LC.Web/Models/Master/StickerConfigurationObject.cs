﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models.Master
{
    public class StickerConfigurationObject
    {
        public string StcUsremail { get; set; }
        public string StcUstname { get; set; }
        public int Operation { get; set; }
        public bool ShowSuccessAlert { get; set; }
        public string OperationFailureReason { get; set; }
        public List<string> StConfigStores { get; set; }
        public List<StickerConfigurationDto> listStickerConfigurationDto { get; set; }
    }
}

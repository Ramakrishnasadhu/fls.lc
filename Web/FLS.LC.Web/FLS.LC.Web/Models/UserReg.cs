﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models
{

    /// <summary>
    /// UserReg
    /// </summary>
    public class UserReg
    {
        /// <summary>
        /// Gets or sets the UserName.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the Email.
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Gets or sets the Phone NO.
        /// </summary>
        public string PhoneNo { get; set; }

        /// <summary>
        /// Gets or sets the Gender.
        /// </summary>
        public string Gender { get; set; }
        public bool ShowSuccessAlert { get; set; }
        public string OperationFailureReason { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models
{
    public class MessageDto
    {
        public string StatusIconCssClassName { get; set; }
        public string CssClassName { get; set; }
        public string Title { get; set; }
        public string DisplayMessage { get; set; }
    }
}

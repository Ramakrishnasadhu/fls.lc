﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models
{
    /// <summary>
    /// Registered Users
    /// </summary>
    public class RegUsers
    {
        /// <summary>
        /// Gets or sets the UsrId.
        /// </summary>
        public long UsrId { get; set; }
        /// <summary>
        /// Gets or sets the UserName.
        /// </summary>
        [StringLength(maximumLength: int.MaxValue)]
        public string UsrUserName { get; set; }
        /// <summary>
        /// Gets or sets the UserEmail.
        /// </summary>
        [StringLength(maximumLength: int.MaxValue)]
        public string UsrUserEmail { get; set; }
        /// <summary>
        /// Gets or sets the UserPhone.
        /// </summary>
        [StringLength(maximumLength: int.MaxValue)]
        public string UsrUserPhone { get; set; }
        /// <summary>
        /// Gets or sets the UseActive.
        /// </summary>
        public bool UsrIsActive { get; set; }
        /// <summary>
        /// Gets or sets the Us.erIsLogin
        /// </summary>
        public bool UsrIsLogin { get; set; }
        /// <summary>
        /// Gets or sets the UserModules.
        /// </summary>
        [StringLength(maximumLength: int.MaxValue)]
        public string UsrModules { get; set; }
        /// <summary>
        /// Gets or sets the User isLocked.
        /// </summary>
        public bool UsrIsLocked { get; set; }
        /// <summary>
        /// Gets or sets the UsrIsSubscribed.
        /// </summary>
        public bool UsrIsSubscribed { get; set; }
        /// <summary>
        /// Gets or sets the UserRole.
        /// </summary>
        public bool UserRole { get; set; }
        /// <summary>
        /// Gets or sets the UserType.
        /// </summary>
        [StringLength(maximumLength: int.MaxValue)]
        public string UserType { get; set; }

        public List<RegUsers> RegUsersList { get; set; }
    }
}

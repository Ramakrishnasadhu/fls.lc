﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Models
{
    public class UserModel
    {
        ///<summary>
        /// Gets or sets Customers.
        ///</summary>
        public List<RegUsers> Users { get; set; }

        ///<summary>
        /// Gets or sets CurrentPageIndex.
        ///</summary>
        public int CurrentPageIndex { get; set; }

        ///<summary>
        /// Gets or sets PageCount.
        ///</summary>
        public int PageCount { get; set; }

        public string UserEmail { get; set; }
        public string Password { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public int Operation { get; set; }
        public bool ShowSuccessAlert { get; set; }
        public string OperationFailureReason { get; set; }

        public bool IsUserFirstLogin { get; set; }
    }
}

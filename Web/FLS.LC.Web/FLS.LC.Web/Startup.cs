using FLS.LC.Web.Services;
using FLS.LC.Web.Services.Interfaces;
using FLS.LC.Web.Services.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Extensions.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FLS.LC.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
           {
               // Set a short timeout for easy testing.
               options.IdleTimeout = TimeSpan.FromMinutes(60);
               options.Cookie.HttpOnly = true;
               // Make the session cookie essential
               options.Cookie.IsEssential = true;
           });
            services.AddMemoryCache();

            services.AddHttpClient<IAccountApiService, AccountApiService>(configureClient =>
          {
              configureClient.BaseAddress = new Uri(Configuration.GetSection("FLS.LC.Cloud.Account.Api:Url").Value);
          });
            services.AddHttpClient<IStoreApiService, StoreApiService>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(Configuration.GetSection("FLS.LC.Cloud.Master.Api:Url").Value);
            });
            services.AddHttpClient<IOrderConfig, OrderConfigApiService>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(Configuration.GetSection("FLS.LC.Cloud.Master.Api:Url").Value);
            });
            services.AddHttpClient<IStickerConfiguration, StickerConfigurationApiService>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(Configuration.GetSection("FLS.LC.Cloud.Master.Api:Url").Value);
            });
            services.AddHttpClient<IGarmentConfig, GarmentConfigApiService>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(Configuration.GetSection("FLS.LC.Cloud.Master.Api:Url").Value);
            });
            services.AddHttpClient<IGarmentCategoryConfig, GarmentCategoryConfigApiService>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(Configuration.GetSection("FLS.LC.Cloud.Master.Api:Url").Value);
            });
            services.AddHttpClient<IPriceListConfig, PriceListConfigApiService>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(Configuration.GetSection("FLS.LC.Cloud.Master.Api:Url").Value);
            });

            services.AddHttpClient<IGarmentRemark, GarmentRemarkService>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(Configuration.GetSection("FLS.LC.Cloud.Master.Api:Url").Value);
            });
            services.AddHttpClient<IGarmentCheckingStaffConfig, GarmentCheckingStaffService>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(Configuration.GetSection("FLS.LC.Cloud.Master.Api:Url").Value);
            });
            services.AddHttpClient<IGarmentReturnCause, GarmentReturnCauseService>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(Configuration.GetSection("FLS.LC.Cloud.Master.Api:Url").Value);
            });
            services.AddHttpClient<IStoreHoliday, StoreHolidayApiService>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(Configuration.GetSection("FLS.LC.Cloud.Master.Api:Url").Value);
            });
            
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
          //  app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseSession();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Login}/{action=Login}/{id?}");
            });
        }
    }
}

﻿using FLS.LC.Web.Models;
using FLS.LC.Web.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
namespace FLS.LC.Web.Controllers
{
    public class LoginController : Controller
    {
        IAccountApiService _AccountApiService;
        private IReadOnlyCollection<RegUsers> regUsers;
        public LoginController(IAccountApiService accountApiService)
        {
            _AccountApiService = accountApiService;
        }
        public IActionResult Login()
        {
            var model = new UserModel();
            //  regUsers = await _AccountApiService.RegisteredUsersAsync();
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Login(UserModel UserModel)
        {
            try
            {
                var operationUserFirstLoginResponse = await _AccountApiService.userFirstLoginAsync(UserModel.UserEmail);
                if (operationUserFirstLoginResponse.CompletedWithSuccess)
                {
                    UserModel.ShowSuccessAlert = true;
                    UserModel.IsUserFirstLogin = false;
                    return View(UserModel);
                }
                else
                {
                    var operationResponse = await _AccountApiService.userLoginAsync(UserModel.UserEmail, UserModel.Password);
                    if (operationResponse.CompletedWithSuccess)
                    {
                        UserModel.ShowSuccessAlert = true;
                        HttpContext.Session.SetString("UserEmail", UserModel.UserEmail);
                        return RedirectToAction("Home", "Home");

                        //var operationUserFirstLoginResponse = await _AccountApiService.userFirstLoginAsync(UserModel.UserEmail);
                        //if (operationUserFirstLoginResponse.CompletedWithSuccess)
                        //{
                        //    UserModel.ShowSuccessAlert = true;
                        //    HttpContext.Session.SetString("UserEmail", UserModel.UserEmail);
                        //    return RedirectToAction("Home", "Home");
                        //}
                        //else
                        //{
                        //    UserModel.ShowSuccessAlert = true;
                        //    UserModel.IsUserFirstLogin = false;
                        //    return View(UserModel);
                        //}
                    }
                    else
                    {
                        UserModel.ShowSuccessAlert = false;
                        TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = operationResponse.OperationError.Details });
                        UserModel.OperationFailureReason = operationResponse.OperationError.Details;
                        return View(UserModel);
                    }
                }
            }
            catch (Exception ex)
            {
                UserModel.ShowSuccessAlert = false;
                TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = ex.Message });
                UserModel.OperationFailureReason = ex.Message;
                return View(UserModel);
            }
        }

        public IActionResult ChangePassword()
        {
            var model = new UserModel();
            return View(model);
        }
        private bool ValidatePassword(string password, out string ErrorMessage)
        {
            var input = password;
            ErrorMessage = string.Empty;

            if (string.IsNullOrWhiteSpace(input))
            {
                throw new Exception("Password should not be empty");
            }

            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMiniMaxChars = new Regex(@".{8,8}");
            var hasLowerChar = new Regex(@"[a-z]+");
            var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");

            if (!hasLowerChar.IsMatch(input))
            {
                ErrorMessage = "Password should contain At least one lower case letter";
                return false;
            }
            else if (!hasUpperChar.IsMatch(input))
            {
                ErrorMessage = "Password should contain At least one upper case letter";
                return false;
            }
            else if (!hasMiniMaxChars.IsMatch(input))
            {
                ErrorMessage = "Password should not be less than or greater than 8 characters";
                return false;
            }
            else if (!hasNumber.IsMatch(input))
            {
                ErrorMessage = "Password should contain At least one numeric value";
                return false;
            }

            else if (!hasSymbols.IsMatch(input))
            {
                ErrorMessage = "Password should contain At least one special case characters";
                return false;
            }
            else
            {
                return true;
            }
        }
        [HttpPost]
        public async Task<string> ChangeUserPassword(string CurrentPassword, string NewPassword, string ConfirmPassword,string UserEmail)
        {
            HttpContext.Session.SetString("UserEmail", UserEmail);
            if (!ValidatePassword(NewPassword, out string ErrorMessage))
            {
                return ErrorMessage;
            }
            var operationResponse = await _AccountApiService.ChangePasswordAsync(UserEmail, CurrentPassword, NewPassword);

            if (operationResponse.CompletedWithSuccess)
            {
                    return "true";
            }
            else
            {
                return operationResponse.OperationError.Details;
            }
        }
        [HttpPost]
        public async Task<IActionResult> ChangePassword(UserModel UserModel)
        {
            try
            {
                string UserEmail = HttpContext.Session.GetString("UserEmail");
                if(UserModel.NewPassword!= UserModel.ConfirmPassword)
                {
                    UserModel.ShowSuccessAlert = false;
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = "Password Do not Match with New Password and Confirm Password." });
                    return View(UserModel);
                }
                if(!ValidatePassword(UserModel.NewPassword,out string ErrorMessage))
                {
                    UserModel.ShowSuccessAlert = false;
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = ErrorMessage });
                    return View(UserModel);
                }

                var operationResponse = await _AccountApiService.ChangePasswordAsync(UserEmail, UserModel.CurrentPassword,UserModel.NewPassword);
                if (operationResponse.CompletedWithSuccess)
                {
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { StatusIconCssClassName = "Success:", CssClassName = "alert-success", Title = "password Reset", DisplayMessage = "Your Password has been reset successfully.Click on Login" });
                    UserModel.ShowSuccessAlert = true;
                }
                else
                {
                    UserModel.ShowSuccessAlert = false;
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = operationResponse.OperationError.Details });
                    UserModel.OperationFailureReason = operationResponse.OperationError.Details;
                }
                return View(UserModel);
            }
            catch (Exception ex)
            {
                UserModel.ShowSuccessAlert = false;
                TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = ex.Message });
                UserModel.OperationFailureReason = ex.Message;
                return View(UserModel);
            }
        }

        public IActionResult Register()
        {
            var model = new UserReg();
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Register(UserReg UserReg)
        {
            try
            {
                var operationResponse = await _AccountApiService.UserRegisterAsync(UserReg);
                if (operationResponse.CompletedWithSuccess)
                {
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { StatusIconCssClassName = "Success:", CssClassName = "alert-success", Title = "User Registration Successful", DisplayMessage = "An Email Message has been sent containing OTP to reset the Password" });
                    UserReg.ShowSuccessAlert = true;
                }
                else
                {
                    UserReg.ShowSuccessAlert = false;
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = operationResponse.OperationError.Details });
                    UserReg.OperationFailureReason = operationResponse.OperationError.Details;
                }
                return View(UserReg);
            }
            catch (Exception ex)
            {
                UserReg.ShowSuccessAlert = false;
                TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = ex.Message });
                UserReg.OperationFailureReason = ex.Message;
                return View(UserReg);
            }
        }
        public IActionResult Forgot()
        {
            var model = new UserModel();
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Forgot(UserModel UserModel)
        {
            try
            {
                var operationResponse = await _AccountApiService.ForgotPasswordAsync(UserModel.UserEmail);
                if (operationResponse.CompletedWithSuccess)
                {
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { StatusIconCssClassName = "Success:", CssClassName = "alert-success", Title = "password Reset Successful", DisplayMessage = "An Email Message has been sent containing OTP to reset the Password" });
                    UserModel.ShowSuccessAlert = true;
                }
                else
                {
                    UserModel.ShowSuccessAlert = false;
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = operationResponse.OperationError.Details });
                    UserModel.OperationFailureReason = operationResponse.OperationError.Details;
                }
                return View(UserModel);
            }
            catch (Exception ex)
            {
                UserModel.ShowSuccessAlert = false;
                TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = ex.Message });
                UserModel.OperationFailureReason = ex.Message;
                return View(UserModel);
            }
        }
    }
}

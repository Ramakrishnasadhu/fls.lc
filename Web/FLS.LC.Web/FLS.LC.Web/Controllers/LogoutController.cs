﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Controllers
{
    public class LogoutController : Controller
    {
        public IActionResult Logout()
        {
            return View();
        }
    }
}

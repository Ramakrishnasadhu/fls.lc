﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using FLS.LC.Web.Services.Interfaces;
using System.Collections.Generic;
using FLS.LC.Web.Models;

namespace FLS.LC.Web.Controllers
{
    public class HomeController : Controller
    {
        IAccountApiService _AccountApiService;
        private IReadOnlyCollection<RegUsers> regUsers;
        public HomeController(IAccountApiService accountApiService)
        {
            _AccountApiService = accountApiService;
        }
        public async Task<IActionResult> Home()
        {
            //string UserEmail = HttpContext.Session.GetString("UserEmail");
            //var regUsers = await _AccountApiService.RegisteredUsersAsync(int pageIndex);
            //return View(regUsers);
            return View();
        }
    }
}

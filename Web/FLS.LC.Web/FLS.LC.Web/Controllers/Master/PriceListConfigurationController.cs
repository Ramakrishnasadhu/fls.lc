﻿using FLS.LC.Web.Models;
using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FLS.LC.Web.Controllers.Master
{
    public class PriceListConfigurationController : Controller
    {
        private readonly IMemoryCache _cache;
        private IWebHostEnvironment Environment;
        IStoreApiService _StoreApiService;
        IPriceListConfig _priceListConfig;
        public PriceListConfigurationController(IMemoryCache memoryCache, IPriceListConfig PriceListConfig, IStoreApiService storeApiService, IWebHostEnvironment _environment)
        {
            _StoreApiService = storeApiService;
            _priceListConfig = PriceListConfig;
            Environment = _environment;
            _cache = memoryCache;
        }
        private void AdduserStoresDataToCache(IReadOnlyCollection<UserStoreDetailDtotemp> userStores)
        {
            var memoryCacheEntryOptions = new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(1),
                Priority = CacheItemPriority.High,
                SlidingExpiration = TimeSpan.FromMinutes(1),
            };
            _cache.Set("userStoresCachedKey", userStores, memoryCacheEntryOptions);
        }
        public async Task<IActionResult> PriceListConfiguration()
        {
            var regStoresResult = new List<SelectListItem>();
            GarmentspriceConfigurationDto GarmentspriceConfigurationDto = new GarmentspriceConfigurationDto();
            string UserEmail = HttpContext.Session.GetString("UserEmail");
            GarmentspriceConfigurationDto.GpcUsremail = UserEmail;
            var existinguserStoresDataInCache = (IReadOnlyCollection<UserStoreDetailDtotemp>)_cache.Get("userStoresCachedKey");
            if (existinguserStoresDataInCache == null)
            {
                var regStores = await _StoreApiService.RegisteredStoresAsync(UserEmail);
                AdduserStoresDataToCache(regStores);
                if (regStores.Count == 0)
                {
                    ViewBag.RegisteredUserStoresListItems = regStores.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList();
                }
                else if (regStores.Count > 0)
                {
                    foreach (var store in regStores)
                    {
                        var storeItem = new SelectListItem();
                        storeItem.Value = store.UstName; //the property you want to display i.e. Title
                        storeItem.Text = store.UstName;
                        regStoresResult.Add(storeItem);
                    }
                    ViewBag.RegisteredUserStoresListItems = regStoresResult;
                    if (TempData.ContainsKey("selectedStore"))
                        GarmentspriceConfigurationDto.GpcUstname = TempData["selectedStore"].ToString();
                }
            }
            else
            {
                ViewBag.RegisteredUserStoresListItems = existinguserStoresDataInCache.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList(); ;

            }
                    
            return View(GarmentspriceConfigurationDto);
        }
        [HttpPost]
        public async Task<JsonResult> GetRegisteredPriceListByStore(string usrEmail, string usrStoreName)
        {
            var operationResponse = await _priceListConfig.GetRegisteredPriceListByStoreAsync(usrEmail, usrStoreName);
            if (operationResponse == null)
            {
            }
            else if (operationResponse != null)
            {
                return Json(operationResponse.FirstOrDefault());
            }
            return null;
        }
        [HttpPost]
        public async Task<JsonResult> updateRegisteredPriceListByStore(string usrEmail, string usrStoreName,bool isIncrementChecked,bool isDecrementChecked,string pricePercentage)
        {
            var operationResponse = await _priceListConfig.updateRegisteredPriceListByStore(usrEmail, usrStoreName, isIncrementChecked, isDecrementChecked, pricePercentage);
            if (operationResponse == null)
            {
                return Json(false);
            }
            else if (operationResponse != null)
            {
                return Json(true);
            }
            return null;
        }
        
            [HttpPost]
        public async Task<IActionResult> PriceListConfiguration(GarmentspriceConfigurationDto GarmentspriceConfigurationDto)
        {
            TempData["selectedStore"] = GarmentspriceConfigurationDto.GpcUstname;
            string UserEmail = HttpContext.Session.GetString("UserEmail");
            GarmentspriceConfigurationDto.GpcUsremail = UserEmail;
            var operationResponse = await _priceListConfig.priceListImportAsync(GarmentspriceConfigurationDto);
            if (operationResponse.CompletedWithSuccess)
            {
                TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { StatusIconCssClassName = "Success:", CssClassName = "alert-success", Title = "User Registration Successful", DisplayMessage = "An Email Message has been sent containing OTP to reset the Password" });
                GarmentspriceConfigurationDto.ShowSuccessAlert = true;
                return RedirectToAction("PriceListConfiguration");
            }
            else
            {
                GarmentspriceConfigurationDto.ShowSuccessAlert = false;
                TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = operationResponse.OperationError.Details });
                GarmentspriceConfigurationDto.OperationFailureReason = operationResponse.OperationError.Details;
            }
            return View(GarmentspriceConfigurationDto);

        }
        public FileResult downLoadExcelFile()
        {
            string fileName = "GarmentsList.xls";
            byte[] file = System.IO.File.ReadAllBytes(this.Environment.WebRootPath + @"\PriceList\" + fileName);
            return File(file, "application/octet-stream", "GarmentsList.xls");
            return null;
        }
    }
}

﻿using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Controllers.Master
{
    public class HolidaysController : Controller
    {
        IStoreHoliday _storeHoliday;
        IStoreApiService _StoreApiService;
        public HolidaysController(IStoreApiService storeApiService, IStoreHoliday storeHoliday)
        {
            _StoreApiService = storeApiService;
            _storeHoliday = storeHoliday;
        }

        [HttpPost]
        public async Task<IActionResult> PostStoreWeekOffHoliday(StoreWeekOffHolidayRequestDto StoreWeekOffHolidayRequestDto)
        {
            string UserEmail = HttpContext.Session.GetString("UserEmail");
            StoreWeekOffHolidayRequestDto.UsthUsremail = UserEmail;
            TempData["selectedStore"] = StoreWeekOffHolidayRequestDto.UsthUstname;
            try
            {
                var holidayResponse = await _storeHoliday.PostStoreWeekOffHolidayAsync(StoreWeekOffHolidayRequestDto);
                if (holidayResponse == true)
                    return Json("true");
                else
                    return Json("false");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public async Task<IActionResult> PostStoreHoliday(StoreHolidayRequestDto storeHolidayRequestDto)
        {
            string UserEmail = HttpContext.Session.GetString("UserEmail");
            storeHolidayRequestDto.UsthUsremail = UserEmail;
            try
            {
                var holidayResponse = await _storeHoliday.PostStoreHolidayAsync(storeHolidayRequestDto);
                if (holidayResponse == true)
                    return RedirectToAction("StoreHolidays");
                else
                    return RedirectToAction("StoreHolidays");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public IActionResult StoreHolidaysByStore(StoreHolidayRequestDto storeHolidayRequestDto)
        {
            TempData["selectedStore"] = storeHolidayRequestDto.UsthUstname;
            return RedirectToAction("StoreHolidays");
        }

        public async Task<IActionResult> StoreHolidays()
        {

            string UserEmail = HttpContext.Session.GetString("UserEmail");
            var regStores = await _StoreApiService.RegisteredStoresAsync(UserEmail);
            ViewBag.RegisteredUserStoresListItems = regStores.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList();
            lstStoreHolidayResponseDto lstStoreHolidayResponseDto = new lstStoreHolidayResponseDto();
            if (TempData["selectedStore"] == null)
            {
                lstStoreHolidayResponseDto.lstStoreHoliday = await _storeHoliday.GetRegisteredHolidayAsync(UserEmail);
                if (lstStoreHolidayResponseDto.lstStoreHoliday.Count == 0)
                    lstStoreHolidayResponseDto.ShowSuccessAlert = false;
                else
                lstStoreHolidayResponseDto.ShowSuccessAlert = true;
            }
            else if (TempData["selectedStore"] != null)
            {
                lstStoreHolidayResponseDto.lstStoreHoliday = await _storeHoliday.GetStoreRegisteredHolidayAsync(UserEmail, TempData["selectedStore"].ToString().Trim());
                if (lstStoreHolidayResponseDto.lstStoreHoliday.Count == 0)
                    lstStoreHolidayResponseDto.ShowSuccessAlert = false;
                else
                    lstStoreHolidayResponseDto.ShowSuccessAlert = true;
            }
            StoreHolidayWeekOffResponseDto StoreHolidayWeekOffResponseDto = await _storeHoliday.GetStoreWeekOffHolidayAsync(UserEmail, TempData["selectedStore"].ToString().Trim());

            if(StoreHolidayWeekOffResponseDto!=null)
            {

            }

            return View(lstStoreHolidayResponseDto);
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FLS.LC.Web.Controllers.Master
{
    public class GarmentCheckingStaffController : Controller
    {
        IGarmentCheckingStaffConfig _garmentcheckingstaff;
        IStoreApiService _StoreApiService;
        private readonly IMemoryCache _cache;
        public GarmentCheckingStaffController(IMemoryCache memoryCache, IStoreApiService storeApiService, IGarmentCheckingStaffConfig garmentcheckingstaff)
        {
            _garmentcheckingstaff = garmentcheckingstaff;
            _StoreApiService = storeApiService;
            _cache = memoryCache;
        }
        private void AdduserStoresDataToCache(IReadOnlyCollection<UserStoreDetailDtotemp> userStores)
        {
            var memoryCacheEntryOptions = new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(1),
                Priority = CacheItemPriority.High,
                SlidingExpiration = TimeSpan.FromMinutes(5),
            };
            _cache.Set("userStoresCachedKey", userStores, memoryCacheEntryOptions);
        }
        public async Task<IActionResult> GarmentCheckingStaff()
        {
            try
            {
                GarmentCheckingStaffResponseDto GarmentCheckingStaffResponseDto = new GarmentCheckingStaffResponseDto();
                string UserEmail = HttpContext.Session.GetString("UserEmail");
                GarmentCheckingStaffResponseDto.GpcUsremail = UserEmail;
                var existinguserStoresDataInCache = (IReadOnlyCollection<UserStoreDetailDtotemp>)_cache.Get("userStoresCachedKey");
                if (existinguserStoresDataInCache == null)
                {
                    var regStores = await _StoreApiService.RegisteredStoresAsync(UserEmail);
                    AdduserStoresDataToCache(regStores);
                    ViewBag.RegisteredUserStoresListItems = regStores.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList();
                }
                else
                {
                    ViewBag.RegisteredUserStoresListItems = existinguserStoresDataInCache.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList();
                }
                var operationResponse = await _garmentcheckingstaff.GetRegisteredGarmentRemarksAsync();
                if (operationResponse.Count > 0)
                {
                    GarmentCheckingStaffResponseDto.lstGarmentCheckingStaffResponseDto = operationResponse;
                    GarmentCheckingStaffResponseDto.ShowSuccessAlert = true;
                    return View(GarmentCheckingStaffResponseDto);
                }
                else
                {
                    return View(GarmentCheckingStaffResponseDto);
                }
            }
            catch (Exception ex)
            {
            }
            return View();
        }


        [HttpPost]
        public async Task<JsonResult> PostGarmentCheckingStaff(string useremail, string userstore, string employeeName, string employeeAddress, string employeemobileno
            , string employeeemail)
        {
            try
            {


                if (string.IsNullOrEmpty(useremail) || string.IsNullOrEmpty(userstore))
                { return Json(false); }
                var garmentCheckingStaffRequestDto = new GarmentCheckingStaffRequestDto()
                {
                    GpcUsremail = useremail,
                    GpcUstname = userstore,
                    GcsEmployeeName = employeeName,
                    GcsEmployeeAddress = employeeAddress,
                    GcsEmployeeMobileNo = Convert.ToInt64(employeemobileno),
                    GcsEmployeeEmail = employeeemail
                };
                var operationResponse = await _garmentcheckingstaff.RegisterGarmentCheckingStaffAsync(garmentCheckingStaffRequestDto);
                if (operationResponse == true)
                {
                    return Json("true");
                }
                return Json("false");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

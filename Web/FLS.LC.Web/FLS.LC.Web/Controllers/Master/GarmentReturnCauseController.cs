﻿using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Controllers.Master
{
    public class GarmentReturnCauseController : Controller
    {
        IGarmentReturnCause _garmentReturnCause;
        public GarmentReturnCauseController(IGarmentReturnCause garmentReturnCause)
        {
            _garmentReturnCause = garmentReturnCause;
        }
        public async Task<IActionResult> GarmentReturnCause()
        {
            try
            {
                lstGarmentReturnCauseResponseDto lstgarmentReturnCauseResponseDto = new lstGarmentReturnCauseResponseDto();
                var operationResponse = await _garmentReturnCause.GetRegisteredGarmentReturnCauseAsync();
                if (operationResponse.Count > 0)
                {
                    lstgarmentReturnCauseResponseDto.lstGarmentReturnCauseResponse = operationResponse;
                    lstgarmentReturnCauseResponseDto.ShowSuccessAlert = true;
                    return View(lstgarmentReturnCauseResponseDto);
                }
                else
                {
                    return View(lstgarmentReturnCauseResponseDto);
                }
            }
            catch (Exception ex)
            {
            }
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> PostGarmentsReturnCause(GarmentReturnCausePostDto garmentReturnCausePostDto)
        {
            var operationResponse = await _garmentReturnCause.RegisterGarmentReturnCauseAsync(garmentReturnCausePostDto);
            if (true)
            {
                return Json("true");
            }
        }
    }
}

﻿using FLS.LC.Web.Services.Master.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FLS.LC.Web.Models.Master;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FLS.LC.Web.Controllers.Master
{
    public class EditPriceListController : Controller
    {
        IGarmentCategoryConfig _GarmentCategoryConfig;
        IStoreApiService _StoreApiService;
        IGarmentConfig _IGarmentConfig;
        private readonly IMemoryCache _cache;
        public EditPriceListController(IMemoryCache memoryCache, IGarmentCategoryConfig garmentCategoryConfig, IStoreApiService storeApiService, IGarmentConfig garmentConfig)
        {
            _StoreApiService = storeApiService;
            _IGarmentConfig = garmentConfig;
            _GarmentCategoryConfig = garmentCategoryConfig;
            _cache = memoryCache;
        }
        private void AdduserStoresDataToCache(IReadOnlyCollection<UserStoreDetailDtotemp> userStores)
        {
            var memoryCacheEntryOptions = new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(1),
                Priority = CacheItemPriority.High,
                SlidingExpiration = TimeSpan.FromMinutes(5),
            };
            _cache.Set("userStoresCachedKey", userStores, memoryCacheEntryOptions);
        }
        [HttpPost]
        public async Task<JsonResult> GetSericePriceByGarment(string usrEmail, string usrStoreName, string service, string garment)
        {
            if (string.IsNullOrEmpty(service) || string.IsNullOrEmpty(garment))
                return Json(false);

            var registerGarmentsDto = new GarmentListDto()
            {
                GlcUsremail = usrEmail,
                GlcUsrstore = usrStoreName
            };
            var resultregGarmentsResponse = await _IGarmentConfig.RegisteredGarmentsByStoreAsync(registerGarmentsDto);
            var result = resultregGarmentsResponse.Where(x => x.GpcGarmentCategory == garment && x.GpcGarmentService == service).Distinct().OrderBy(x => x.GpcGarmentName).ToList();
            return Json(result);
        }
        [HttpPost]
        public async Task<JsonResult> UpdateServicePriceByGarment(string usrEmail, string usrStoreName, int updatedprice, string garmentname, string service, string garment)
        {
            if (string.IsNullOrEmpty(usrEmail) || string.IsNullOrEmpty(usrStoreName)
                || string.IsNullOrEmpty(garmentname)
                || updatedprice == 0
                || string.IsNullOrEmpty(service)
                || string.IsNullOrEmpty(garment))
                return Json(false);

            var updateServicePriceByGarmentDto = new UpdateServicePriceByGarmentDto()
            {
                UsrEmail = usrEmail.Trim(),
                UsrStoreName = usrStoreName.Trim(),
                Garment = garment.Trim(),
                Service = service.Trim(),
                Garmentname = garmentname.Trim(),
                Updatedprice = updatedprice
            };
            var result = await _IGarmentConfig.UpdateServicePriceByGarmentAsync(updateServicePriceByGarmentDto);
            return Json(result);
        }

        public async Task<IActionResult> EditPriceList()
        {
            ListRegisteredGarmentsByStore listRegisteredGarmentsByStore = new ListRegisteredGarmentsByStore();
            listRegisteredGarmentsByStore.ShowSuccessAlert = false;
            string UserEmail = HttpContext.Session.GetString("UserEmail");
            listRegisteredGarmentsByStore.GlcUsremail = UserEmail;
            string UstName = string.Empty;

            var existinguserStoresDataInCache = (IReadOnlyCollection<UserStoreDetailDtotemp>)_cache.Get("userStoresCachedKey");
            if (existinguserStoresDataInCache == null)
            {
                var regStores = await _StoreApiService.RegisteredStoresAsync(UserEmail);
                UstName = regStores.FirstOrDefault().UstName;
                AdduserStoresDataToCache(regStores);
                ViewBag.RegisteredUserStoresListItems = regStores.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList();
            }
            else
            {
                ViewBag.RegisteredUserStoresListItems = existinguserStoresDataInCache.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList();
            }
            return View(listRegisteredGarmentsByStore);
        }
        [HttpPost]
        public async Task<IActionResult> EditPriceList(ListRegisteredGarmentsByStore EditPriceListDto)
        {

            string UserEmail = HttpContext.Session.GetString("UserEmail");
            string UstName = string.Empty;
            UstName = EditPriceListDto.GlcUstname;
            var registerGarmentsDto = new GarmentListDto()
            {
                GlcUsremail = UserEmail,
                GlcUsrstore = UstName
            };
            var resultregStoresResponse = await _IGarmentConfig.RegisteredGarmentsByStoreAsync(registerGarmentsDto);

            List<string> CategoryList = resultregStoresResponse.Select(x => x.GpcGarmentCategory).Distinct().OrderBy(x => x).ToList();

            List<string> ServiceList = resultregStoresResponse.Select(x => x.GpcGarmentService).Distinct().OrderBy(x => x).ToList();

            ListRegisteredGarmentsByStore listRegisteredGarmentsByStore = new ListRegisteredGarmentsByStore();
            listRegisteredGarmentsByStore.GlcUsremail = UserEmail;
            listRegisteredGarmentsByStore.GlcUstname = UstName;
            listRegisteredGarmentsByStore.lstGarmentCategory = CategoryList;
            listRegisteredGarmentsByStore.lstGarmentService = ServiceList;
            listRegisteredGarmentsByStore.ShowSuccessAlert = true;
            var existinguserStoresDataInCache = (IReadOnlyCollection<UserStoreDetailDtotemp>)_cache.Get("userStoresCachedKey");
            if (existinguserStoresDataInCache == null)
            {
                var regStores = await _StoreApiService.RegisteredStoresAsync(UserEmail);
                UstName = regStores.FirstOrDefault().UstName;
                AdduserStoresDataToCache(regStores);
                ViewBag.RegisteredUserStoresListItems = regStores.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList();
            }
            else
            {
                ViewBag.RegisteredUserStoresListItems = existinguserStoresDataInCache.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList();
            }
            //start
            List<RegisteredGarmentsDetailsByStore> lstRegisteredGarmentsDetailsByStore = new List<RegisteredGarmentsDetailsByStore>();
            foreach (string garmentCategory in CategoryList)
            {
                var withduplicateresult = resultregStoresResponse.Where(x => x.GpcGarmentCategory == garmentCategory).Distinct().OrderBy(x => x.GpcGarmentName).ToList();
                var result = withduplicateresult.GroupBy(x => x.GpcGarmentName).Select(y => y.First()).ToList();
                foreach (RegistredGarmentslistDto registredGarmentslistDto in result)
                {
                    RegisteredGarmentsDetailsByStore registeredGarmentsDetailsByStore = new RegisteredGarmentsDetailsByStore();
                    registeredGarmentsDetailsByStore.GarmentCategory = registredGarmentslistDto.GpcGarmentCategory;
                    registeredGarmentsDetailsByStore.GarmentName = registredGarmentslistDto.GpcGarmentName;
                    registeredGarmentsDetailsByStore.GarmentServicePrice = registredGarmentslistDto.GpcGarmentServicePrice;
                    lstRegisteredGarmentsDetailsByStore.Add(registeredGarmentsDetailsByStore);
                }
            }
            listRegisteredGarmentsByStore.lstRegisteredGarmentsDetailsByStore = lstRegisteredGarmentsDetailsByStore;
            //end
            return View(listRegisteredGarmentsByStore);
        }
    }
}

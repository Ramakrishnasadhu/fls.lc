﻿using FLS.LC.Web.Models;
using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FLS.LC.Web.Controllers.Master
{
    public class GarmentsListConfigurationController : Controller
    {
        IGarmentCategoryConfig _GarmentCategoryConfig;
        IStoreApiService _StoreApiService;
        IGarmentConfig _IGarmentConfig;
        private readonly IMemoryCache _cache;
        public GarmentsListConfigurationController(IMemoryCache memoryCache, IGarmentCategoryConfig garmentCategoryConfig, IStoreApiService storeApiService, IGarmentConfig garmentConfig)
        {
            _StoreApiService = storeApiService;
            _IGarmentConfig = garmentConfig;
            _GarmentCategoryConfig = garmentCategoryConfig;
            _cache = memoryCache;
        }
        private void AddregisterGarmentsDataToCache(IReadOnlyCollection<RegistredGarmentslistDto> registerGarments)
        {
            var memoryCacheEntryOptions = new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(1),
                Priority = CacheItemPriority.High,
                SlidingExpiration = TimeSpan.FromMinutes(5),
            };
            _cache.Set("registerGarmentsCachedKey", registerGarments, memoryCacheEntryOptions);
        }
        private void AdduserStoresDataToCache(IReadOnlyCollection<UserStoreDetailDtotemp> userStores)
        {
            var memoryCacheEntryOptions = new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(1),
                Priority = CacheItemPriority.High,
                SlidingExpiration = TimeSpan.FromMinutes(5),
            };
            _cache.Set("userStoresCachedKey", userStores, memoryCacheEntryOptions);
        }
        private void AddGarmentCategoryListDataToCache(IReadOnlyCollection<string> GarmentCategoryDto)
        {
            var memoryCacheEntryOptions = new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(1),
                Priority = CacheItemPriority.High,
                SlidingExpiration = TimeSpan.FromMinutes(1),
            };
            _cache.Set("registeredGarmentCategoryCachedKey", GarmentCategoryDto, memoryCacheEntryOptions);
        }
        private void AddRequiredGarmentCategoryDataToCache(GarmentspriceConfigurationReadDto GarmentCategoryDto)
        {
            var memoryCacheEntryOptions = new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(1),
                Priority = CacheItemPriority.High,
                SlidingExpiration = TimeSpan.FromMinutes(1),
            };
            _cache.Set("requiredGarmentCategoryDataToCachedKey", GarmentCategoryDto, memoryCacheEntryOptions);
        }
        private void AddGarmentNamesDataToCache(IReadOnlyCollection<string> GarmentCategoryDto)
        {
            var memoryCacheEntryOptions = new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(1),
                Priority = CacheItemPriority.High,
                SlidingExpiration = TimeSpan.FromMinutes(1),
            };
            _cache.Set("registeredGarmentNamesCachedKey", GarmentCategoryDto, memoryCacheEntryOptions);
        }
        public async Task<IActionResult> GarmentsList()
        {
            GarmentslistConfigurationInput GarmentslistConfigurationInput = new GarmentslistConfigurationInput();
            GarmentslistConfigurationInput.ShowSuccessAlert = true;
            string UserEmail = HttpContext.Session.GetString("UserEmail");
            GarmentslistConfigurationInput.GlcUsremail = UserEmail;
            string UstName = string.Empty;

            var existinguserStoresDataInCache = (IReadOnlyCollection<UserStoreDetailDtotemp>)_cache.Get("userStoresCachedKey");
            if (existinguserStoresDataInCache == null)
            {
                var regStores = await _StoreApiService.RegisteredStoresAsync(UserEmail);
                UstName = regStores.FirstOrDefault().UstName;
                AdduserStoresDataToCache(regStores);
                ViewBag.RegisteredUserStoresListItems = regStores.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList();
            }
            else
            {
                ViewBag.RegisteredUserStoresListItems = existinguserStoresDataInCache.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList();
                UstName = existinguserStoresDataInCache.FirstOrDefault().UstName;
            }
            var garmentServiceDto = new GarmentCategoryDto()
            {
                GlcUsremail = UserEmail,
                GlcUsrstore = UstName
            };
            var existingGarmentCategoryDataInCache = (IReadOnlyCollection<string>)_cache.Get("registeredGarmentCategoryCachedKey");

            if (existingGarmentCategoryDataInCache == null)
            {
                var registerGarments = await _GarmentCategoryConfig.RegisteredGarmentsListCategoryAsync(garmentServiceDto);
                AddGarmentCategoryListDataToCache(registerGarments);

                var registerGarmentsServices = await _GarmentCategoryConfig.RegisteredGarmentServiceListAsync(garmentServiceDto);
                AddGarmentNamesDataToCache(registerGarmentsServices);

                List<SelectListItem> ObjregisterGarments = new List<SelectListItem>();
                foreach (string item in registerGarments)
                {
                    SelectListItem SelectListItem = new SelectListItem() { Text = item, Value = item };
                    ObjregisterGarments.Add(SelectListItem);
                }
                ViewBag.RegisteredGarmentsCategoryListItems = ObjregisterGarments;

                List<SelectListItem> ObjregisterServices = new List<SelectListItem>();
                foreach (string item in registerGarmentsServices)
                {
                    SelectListItem SelectListItem = new SelectListItem() { Text = item, Value = item };
                    ObjregisterServices.Add(SelectListItem);
                }
                ViewBag.RegisteredGarmentsServiceListItems = ObjregisterServices;

            }
            else
            {
                //ViewBag.RegisteredGarmentsCategoryListItems = existingGarmentCategoryDataInCache.GroupBy(x => x.GpcGarmentCategory).Select(g => g.First()).Select(i => new SelectListItem() { Text = i.GpcGarmentCategory, Value = i.GpcGarmentCategory.ToString() }).ToList();
                //ViewBag.RegisteredGarmentsServiceListItems = existingGarmentCategoryDataInCache.GroupBy(x => x.GpcGarmentService).Select(g => g.First()).ToList().Select(i => new SelectListItem() { Text = i.GpcGarmentService, Value = i.GpcGarmentService.ToString() }).ToList();
                UstName = existinguserStoresDataInCache.FirstOrDefault().UstName;
            }
            return View(GarmentslistConfigurationInput);
        }

        [HttpPost]
        public async Task<IActionResult> GarmentsList(GarmentslistConfigurationInput GarmentslistConfigurationInput)
        {
            var regStores = await _StoreApiService.RegisteredStoresAsync(GarmentslistConfigurationInput.GlcUsremail);
            ViewBag.RegisteredUserStoresListItems = regStores.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList();
            try
            {
                string UserEmail = HttpContext.Session.GetString("UserEmail");
                GarmentslistConfigurationInput.GlcUsremail = UserEmail;
                var operationResponse = await _IGarmentConfig.GarmentRegisterAsync(GarmentslistConfigurationInput);
                if (operationResponse.CompletedWithSuccess)
                {
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { StatusIconCssClassName = "Success:", CssClassName = "alert-success", Title = "User Registration Successful", DisplayMessage = "An Email Message has been sent containing OTP to reset the Password" });
                    GarmentslistConfigurationInput.ShowSuccessAlert = true;
                    return RedirectToAction("GarmentsList");
                }
                else
                {
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = operationResponse.OperationError.Details });
                    GarmentslistConfigurationInput.OperationFailureReason = operationResponse.OperationError.Details;
                    return RedirectToAction("GarmentsList");
                }
            }
            catch (Exception ex)
            {
                GarmentslistConfigurationInput.ShowSuccessAlert = false;
                TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = ex.Message });
                GarmentslistConfigurationInput.OperationFailureReason = ex.Message;
                return View(GarmentslistConfigurationInput);
            }
        }

        [HttpPost]
        public async Task<JsonResult> SearchGarmentsByStore(string usrEmail, string usrStoreName, string searchGarmentText)
        {
            if (string.IsNullOrEmpty(searchGarmentText))
                return Json(false);
            var GarmentCategoryDto = new GarmentCategoryDto()
            {
                GlcUsremail = usrEmail,
                GlcUsrstore = usrStoreName
            };
            var existingGarmentCategoryDataInCache = (IReadOnlyCollection<string>)_cache.Get("registeredGarmentNamesCachedKey");
            existingGarmentCategoryDataInCache = null;
            if (existingGarmentCategoryDataInCache == null)
            {
                var registerGarmentsNames = await _GarmentCategoryConfig.SearchRegisteredGarmentServiceListAsync(GarmentCategoryDto);
                AddGarmentNamesDataToCache(registerGarmentsNames);
                string[] machedGarments = GetPossibleWords(registerGarmentsNames, searchGarmentText);
                if (machedGarments.Count() == 0)
                    return Json(false);
                return Json(machedGarments);
            }
            else if (existingGarmentCategoryDataInCache != null)
            {
                string[] machedGarments = GetPossibleWords(existingGarmentCategoryDataInCache, searchGarmentText);
                if (machedGarments.Count() == 0)
                {
                    return Json(false);
                }
                return Json(machedGarments);
            }
            return Json(false);
        }

        public string[] GetPossibleWords(IReadOnlyCollection<string> listGarments, string @searchGarmentText)
        {
            System.Text.RegularExpressions.Regex searchTerm = new System.Text.RegularExpressions.Regex(@searchGarmentText);
            List<String> Matches = new List<String>();
            foreach (var garment in listGarments)
            {
                if (searchTerm.IsMatch(garment))
                    Matches.Add(garment);
            }
            return Matches.ToArray();
        }
        [HttpPost]
        public async Task<JsonResult> RetriveSelectedGarmentByStore(string usrEmail, string usrStoreName, string retriveGarment, string selectedGarment, string selectedService)
        {
            if (string.IsNullOrEmpty(retriveGarment) || string.IsNullOrEmpty(selectedGarment) || string.IsNullOrEmpty(selectedService))
                return Json(false);
            var requiredGarmentDto = new RequiredGarmentDto()
            {
                GlcUsremail = usrEmail,
                GlcUsrstore = usrStoreName,
                GlcGarmentName = retriveGarment,
                GlcSelectedGarment = selectedGarment,
                GlcSelectedService = selectedService
            };
            // var existingGarmentCategoryDataInCache = (GarmentspriceConfigurationReadDto)_cache.Get("requiredGarmentCategoryDataToCachedKey");
            var requiredGarment = await _GarmentCategoryConfig.RetriveSelectedGarmentCategoryAsync(requiredGarmentDto);
            if (requiredGarment == null)
                return Json(false);
            // AddRequiredGarmentCategoryDataToCache(requiredGarment);
            if (requiredGarment != null)
            {
                string imreBase64Data = Convert.ToBase64String(requiredGarment.GpcImage);
                string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                requiredGarment.ImgDataURL = imgDataURL;
            }
            return Json(requiredGarment);
        }
    }
}

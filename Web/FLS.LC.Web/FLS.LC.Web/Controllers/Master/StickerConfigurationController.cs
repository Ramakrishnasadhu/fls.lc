﻿using FLS.LC.Web.Models;
using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Controllers.Master
{
    public class StickerConfigurationController : Controller
    {
        IStoreApiService _StoreApiService;
        IStickerConfiguration _StickerConfiguration;

        public StickerConfigurationController(IStoreApiService storeApiService, IStickerConfiguration stickerConfiguration)
        {
            _StoreApiService = storeApiService;
            _StickerConfiguration = stickerConfiguration;
        }

        public async Task<IActionResult> StickerConfiguration()
        {
            List<StickerConfigurationDto> listStickerConfigurationDto = new List<StickerConfigurationDto>();
            var StickerConfigurationDto = new StickerConfigurationDto()
            {
                StcOption = "Barcode",
            };
            listStickerConfigurationDto.Add(StickerConfigurationDto);
            var StickerConfigurationDtoBookingNo = new StickerConfigurationDto()
            {
                StcOption = "BookingNo",
            };
            listStickerConfigurationDto.Add(StickerConfigurationDtoBookingNo);
            var StickerConfigurationDtoProcessProcess = new StickerConfigurationDto()
            {
                StcOption = "Process",
            };
            listStickerConfigurationDto.Add(StickerConfigurationDtoProcessProcess);
            var StickerConfigurationDtoProcessGarmentName = new StickerConfigurationDto()
            {
                StcOption = "GarmentName",
            };
            listStickerConfigurationDto.Add(StickerConfigurationDtoProcessGarmentName);
            var StickerConfigurationDtoProcessCustomerName = new StickerConfigurationDto()
            {
                StcOption = "CustomerName",
            };
            listStickerConfigurationDto.Add(StickerConfigurationDtoProcessCustomerName);
            var StickerConfigurationDtoProcessCustomerAddress = new StickerConfigurationDto()
            {
                StcOption = "CustomerAddress",
            };
            listStickerConfigurationDto.Add(StickerConfigurationDtoProcessCustomerAddress);
            var StickerConfigurationDtoProcessColor = new StickerConfigurationDto()
            {
                StcOption = "Color",
            };
            listStickerConfigurationDto.Add(StickerConfigurationDtoProcessColor);
            var StickerConfigurationDtoProcessRemark = new StickerConfigurationDto()
            {
                StcOption = "Remark",
            };
            listStickerConfigurationDto.Add(StickerConfigurationDtoProcessRemark);
            var StickerConfigurationDtoProcessDueDate = new StickerConfigurationDto()
            {
                StcOption = "DueDate",
            };
            listStickerConfigurationDto.Add(StickerConfigurationDtoProcessDueDate);
            var StickerConfigurationObject = new StickerConfigurationObject()
            {
                listStickerConfigurationDto = listStickerConfigurationDto,
            };
            string UserEmail = HttpContext.Session.GetString("UserEmail");
            var regStores = await _StoreApiService.RegisteredStoresAsync(UserEmail);
            ViewBag.RegisteredUserStoresListItems = regStores.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList();
            var StickerConfigDto = new StickerConfigDto()
            {
                 StcUsremail= UserEmail,
            };
            StickerConfigurationObject.ShowSuccessAlert = true;
            StickerConfigurationObject.StcUsremail = UserEmail;
             var StConfigStores = await _StickerConfiguration.GetAllStoreStickerConfiConfigAsync(StickerConfigDto);
            StickerConfigurationObject.StConfigStores = StConfigStores;
            return View(StickerConfigurationObject);
        }
        [HttpPost]
        public async Task<JsonResult> SelectedStoreStickerConfigDetails(string usrEmail, string usrStoreName)
        {
            string UserEmail = HttpContext.Session.GetString("UserEmail");
            var operationResponse = await _StickerConfiguration.SelectedStoreStickerConfigDetailsAsync(usrEmail, usrStoreName);
            if (operationResponse == null)
            {
            }
            else if (operationResponse != null)
            {
                return Json(operationResponse);
            }
            return null;
        }
        [HttpPost]
        public async Task<IActionResult> StickerConfiguration(StickerConfigurationObject StickerConfigurationObject)
        {

            try
            {
                string UserEmail = HttpContext.Session.GetString("UserEmail");
                StickerConfigurationObject.StcUsremail = UserEmail;
                if(StickerConfigurationObject.listStickerConfigurationDto[0].StcId>0)
                    StickerConfigurationObject.Operation = 2;
                else
                StickerConfigurationObject.Operation = 1;
                var regStores = await _StoreApiService.RegisteredStoresAsync(UserEmail);
                ViewBag.RegisteredUserStoresListItems = regStores.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList();
                if (StickerConfigurationObject.StcUstname==null)
                {
                    StickerConfigurationObject.ShowSuccessAlert = false;
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = "Please Select Store Name Atleat" });
                    StickerConfigurationObject.OperationFailureReason = "Please Select Store Name Atleat";
                    var StickerConfigDto = new StickerConfigDto()
                    {
                        StcUsremail = UserEmail,
                    };
                    var StConfigStores = await _StickerConfiguration.GetAllStoreStickerConfiConfigAsync(StickerConfigDto);
                    StickerConfigurationObject.StConfigStores = StConfigStores;
                    return View(StickerConfigurationObject);
                }
                var operationResponse = await _StickerConfiguration.StickerConfigurationAsync(StickerConfigurationObject);
                if (operationResponse.CompletedWithSuccess)
                {
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { StatusIconCssClassName = "Success:", CssClassName = "alert-success", Title = "User Registration Successful", DisplayMessage = "An Email Message has been sent containing OTP to reset the Password" });
                    StickerConfigurationObject.ShowSuccessAlert = true;
                    return RedirectToAction("StickerConfiguration");
                }
                else
                {
                    StickerConfigurationObject.ShowSuccessAlert = false;
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = operationResponse.OperationError.Details });
                    StickerConfigurationObject.OperationFailureReason = operationResponse.OperationError.Details;
                    var StickerConfigDto = new StickerConfigDto()
                    {
                        StcUsremail = UserEmail,
                    };
                    var StConfigStores = await _StickerConfiguration.GetAllStoreStickerConfiConfigAsync(StickerConfigDto);
                    StickerConfigurationObject.StConfigStores = StConfigStores;
                }
                
                return View(StickerConfigurationObject);
            }
            catch (Exception ex)
            {
                StickerConfigurationObject.ShowSuccessAlert = false;
                TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = ex.Message });
                StickerConfigurationObject.OperationFailureReason = ex.Message;
                return View(StickerConfigurationObject);
            }
        }
    }
}

﻿using FLS.LC.Web.Models;
using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Controllers.Master
{
    public class OrderConfigController : Controller
    {
        IStoreApiService _StoreApiService;
        IOrderConfig _OrderConfig;

        public OrderConfigController(IStoreApiService storeApiService, IOrderConfig orderConfig)
        {
            _StoreApiService = storeApiService;
            _OrderConfig = orderConfig;
        }
        public async Task<IActionResult> OrderConfig()
        {
            string UserEmail = HttpContext.Session.GetString("UserEmail");
            var regStores = await _StoreApiService.RegisteredStoresAsync(UserEmail);
            ViewBag.RegisteredUserStoresListItems = regStores.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList();
            var operationResponse = await _OrderConfig.GetAllStoreOrderConfigAsync(UserEmail);
            if (operationResponse.Count == 0)
            {
                OrderConfigDto OrderConfigDto = new OrderConfigDto();
                OrderConfigDto.OrderConfigurations = operationResponse;
                return View(OrderConfigDto);
            }
            else if (operationResponse.Count > 0)

            {
                OrderConfigDto OrderConfigDto = new OrderConfigDto();
                OrderConfigDto.UserEmail = UserEmail;
                OrderConfigDto.OrderConfigurations = operationResponse;
                return View(OrderConfigDto);
            }
            return View(operationResponse);
        }
        [HttpPost]
        public async Task<JsonResult> SelectedStoreConfigDetails(string usrEmail, string usrStoreName)
        {
            string UserEmail = HttpContext.Session.GetString("UserEmail");
            var operationResponse = await _OrderConfig.SelectedStoreConfigDetailsAsync(usrEmail, usrStoreName);
            if (operationResponse == null)
            {
            }
            else if (operationResponse != null)
            {
                return Json(operationResponse.FirstOrDefault());
            }
            return null;
        }

        [HttpPost]
        public async Task<IActionResult> OrderConfig(OrderConfigDto OrderConfigDto)
        {
            try
            {
                string UserEmail = HttpContext.Session.GetString("UserEmail");
                OrderConfigDto.UserEmail = UserEmail;
                if(OrderConfigDto.OrderScreenConfigurationDto.OscId>0)
                OrderConfigDto.Operation = 2;
                else if(OrderConfigDto.OrderScreenConfigurationDto.OscId==0)
                    OrderConfigDto.Operation = 1;
                var operationResponse = await _OrderConfig.PostOrderConfigAsync(OrderConfigDto);
                if (operationResponse.CompletedWithSuccess)
                {
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { StatusIconCssClassName = "Success:", CssClassName = "alert-success", Title = "User Registration Successful", DisplayMessage = "An Email Message has been sent containing OTP to reset the Password" });
                    OrderConfigDto.ShowSuccessAlert = true;
                    return RedirectToAction("OrderConfig");
                }
                else
                {
                    var regStores = await _StoreApiService.RegisteredStoresAsync(UserEmail);
                    ViewBag.RegisteredUserStoresListItems = regStores.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList();
                    OrderConfigDto.ShowSuccessAlert = false;
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = operationResponse.OperationError.Details });
                    OrderConfigDto.OperationFailureReason = operationResponse.OperationError.Details;
                    var responseItems = await _OrderConfig.GetAllStoreOrderConfigAsync(UserEmail);
                    OrderConfigDto.OrderConfigurations = responseItems;
                }
            }
            catch(Exception ex)
            {

            }
            return View(OrderConfigDto);
        }
    }
}

﻿using FLS.LC.Web.Models;
using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Controllers.Master
{
    public class GarmentsCategoryController : Controller
    {
        IStoreApiService _StoreApiService;
        IGarmentCategoryConfig _GarmentCategoryConfig;
        public GarmentsCategoryController(IGarmentCategoryConfig garmentCategoryConfig, IStoreApiService storeApiService)
        {
            _StoreApiService = storeApiService;
            _GarmentCategoryConfig = garmentCategoryConfig;
        }
        public async Task<IActionResult> GarmentsCategory()
        {
            GarmentsCategoryDto GarmentsCategoryDto = new GarmentsCategoryDto();
            string UserEmail = HttpContext.Session.GetString("UserEmail");
            var regStores = await _StoreApiService.RegisteredStoresAsync(UserEmail);
            ViewBag.RegisteredUserStoresListItems = regStores.Distinct().Select(i => new SelectListItem() { Text = i.UstName, Value = i.UstName.ToString() }).ToList();
            var garmentCategoryDto = new GarmentCategoryDto()
            {
                GlcUsremail = UserEmail,
                GlcUsrstore = regStores.FirstOrDefault().UstName
            };
            var registerGarmentsCategory = await _GarmentCategoryConfig.RegisteredGarmentsListCategoryAsync(garmentCategoryDto);
            GarmentsCategoryDto.RegistredGarmentsCategory = registerGarmentsCategory;
            return View(GarmentsCategoryDto);
        }
        [HttpPost]
        public async Task<IActionResult> GarmentsCategory(GarmentsCategoryDto GarmentsCategoryDto)
        {
            try
            {
                string UserEmail = HttpContext.Session.GetString("UserEmail");
                GarmentsCategoryDto.GcUsremail = UserEmail;
                GarmentsCategoryDto.Operation = 1;
                GarmentsCategoryDto.GcUstname = GarmentsCategoryDto.GcUstname;
                GarmentsCategoryDto.GcGarmentCategory = GarmentsCategoryDto.GcGarmentCategory;
                var operationResponse = await _GarmentCategoryConfig.GarmentCategoryRegisterAsync(GarmentsCategoryDto);
                if (operationResponse.CompletedWithSuccess)
                {
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { StatusIconCssClassName = "Success:", CssClassName = "alert-success", Title = "Garment Category Registration Successful", DisplayMessage = "An Email Message has been sent containing OTP to reset the Password" });
                    GarmentsCategoryDto.ShowSuccessAlert = true;
                    return RedirectToAction("GarmentsCategory");
                }
                else
                {
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = operationResponse.OperationError.Details });
                    GarmentsCategoryDto.OperationFailureReason = operationResponse.OperationError.Details;
                }
                return View(GarmentsCategoryDto);
            }
            catch (Exception ex)
            {
                GarmentsCategoryDto.ShowSuccessAlert = false;
                TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = ex.Message });
                GarmentsCategoryDto.OperationFailureReason = ex.Message;
                return View(GarmentsCategoryDto);
            }
        }
    }
}

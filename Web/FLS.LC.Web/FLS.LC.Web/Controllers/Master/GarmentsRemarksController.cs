﻿using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace FLS.LC.Web.Controllers.Master
{
    public class GarmentsRemarksController : Controller
    {
        IGarmentRemark _garmentRemark;
        public GarmentsRemarksController(IGarmentRemark garmentRemark)
        {
            _garmentRemark = garmentRemark;
        }
        public async Task<IActionResult> GarmentsRemarks()
        {
            try
            {
                GarmentRemarkResponseDto garmentRemarkResponseDto = new GarmentRemarkResponseDto();
                var operationResponse = await _garmentRemark.GetRegisteredGarmentRemarksAsync();
                if (operationResponse.Count > 0)
                {
                    garmentRemarkResponseDto.lstGarmentRemarks = operationResponse;
                    garmentRemarkResponseDto.ShowSuccessAlert = true;
                    return View(garmentRemarkResponseDto);
                }
                else
                {
                    return View(garmentRemarkResponseDto);
                }
            }
            catch (Exception ex)
            {
            }
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> PostGarmentsRemarks(GarmentRemarkPostRequestDto garmentRemarkPostRequestDto)
        {
                var operationResponse = await _garmentRemark.RegisterGarmentRemarkAsync(garmentRemarkPostRequestDto.garmentRemarkDescriptionOldValue, garmentRemarkPostRequestDto.garmentRemarkDescriptionNewValue, garmentRemarkPostRequestDto.garmentRemarkId);
                if (true)
                {
                    return Json("true");
                }
        }
    }
}

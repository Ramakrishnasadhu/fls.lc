﻿using FLS.LC.Web.Models;
using FLS.LC.Web.Models.Master;
using FLS.LC.Web.Services.Master.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Web.Controllers.Master
{
    public class StoreController : Controller
    {
        IStoreApiService _StoreApiService;
        private readonly IMemoryCache _cache;
        public StoreController(IMemoryCache memoryCache, IStoreApiService storeApiService)
        {
            _StoreApiService = storeApiService;
            _cache = memoryCache;
        }
        private void AdduserStoresDataToCache(IReadOnlyCollection<UserStoreDetailDtotemp> userStores)
        {
            var memoryCacheEntryOptions = new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(1),
                Priority = CacheItemPriority.High,
                SlidingExpiration = TimeSpan.FromMinutes(1),
            };
            _cache.Set("userStoresCachedKey", userStores, memoryCacheEntryOptions);
        }
        [HttpGet]
        public async Task<IActionResult> RegStores()
        {
            viewUserStore viewUserStore = new viewUserStore();
            string UserEmail = HttpContext.Session.GetString("UserEmail");
            viewUserStore.UserEmail = UserEmail;
            var existinguserStoresDataInCache =(IReadOnlyCollection <UserStoreDetailDtotemp>) _cache.Get("userStoresCachedKey");
            if (existinguserStoresDataInCache == null)
            {
                var regStores = await _StoreApiService.RegisteredStoresAsync(UserEmail);
                if(regStores!=null)
                AdduserStoresDataToCache(regStores);
                if (regStores.Count == 0)
                {
                    viewUserStore.UserStores = regStores;
                }
                else if (regStores.Count > 0)
                {
                    viewUserStore.UserStores = regStores;
                }
            }
            else
            {
                viewUserStore.UserStores = existinguserStoresDataInCache;
            }
            return View(viewUserStore);
        }
        [HttpPost]
        public async Task<JsonResult> InActivateSelectedStore(string usrEmail, string usrStoreName)
        {
            string UserEmail = HttpContext.Session.GetString("UserEmail");
            var operationResponse = await _StoreApiService.InActivateSelectedStore(usrEmail, usrStoreName);

            if (operationResponse.CompletedWithSuccess == true)
            {
                var regStores = await _StoreApiService.RegisteredStoresAsync(UserEmail);
                AdduserStoresDataToCache(regStores);
                return Json("true");
            }
            else
            {
                return Json("false");
            }
        }
        [HttpPost]
        public async Task<JsonResult> SelectedStoreDetails(string usrEmail, string usrStoreName)
        {
            string UserEmail = HttpContext.Session.GetString("UserEmail");
            var operationResponse = await _StoreApiService.SelectedStoreAsync(usrEmail, usrStoreName);
            if (operationResponse == null)
            {
            }
            else if (operationResponse != null)
            {
                string imreBase64Data = Convert.ToBase64String(operationResponse.FirstOrDefault().UstLogo);
                string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                operationResponse.FirstOrDefault().ImageDataUrl = imgDataURL;
                return Json(operationResponse.FirstOrDefault());
            }
            return null;
        }
        [HttpPost]
        public async Task<IActionResult> RegStores(viewUserStore viewUserStore)
        {
            try
            {
                string UserEmail = HttpContext.Session.GetString("UserEmail");
                viewUserStore.UserStore.UsrEmail = UserEmail;
                viewUserStore.UserStore.Operation = 1;

                var operationResponse = await _StoreApiService.StoreRegisterAsync(viewUserStore);
                if (operationResponse.CompletedWithSuccess)
                {
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { StatusIconCssClassName = "Success:", CssClassName = "alert-success", Title = "User Registration Successful", DisplayMessage = "An Email Message has been sent containing OTP to reset the Password" });
                    viewUserStore.ShowSuccessAlert = true;
                    _cache.Remove("userStoresCachedKey");
                    return RedirectToAction("RegStores");
                }
                else
                {
                    viewUserStore.ShowSuccessAlert = false;
                    TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = operationResponse.OperationError.Details });
                    viewUserStore.OperationFailureReason = operationResponse.OperationError.Details;
                }
                return View(viewUserStore);
            }
            catch (Exception ex)
            {
                viewUserStore.ShowSuccessAlert = false;
                TempData["AlertMessage"] = JsonConvert.SerializeObject(new MessageDto() { CssClassName = "alert-danger", Title = "Fail!", DisplayMessage = ex.Message });
                viewUserStore.OperationFailureReason = ex.Message;
                return View(viewUserStore);
            }

        }
    }
}

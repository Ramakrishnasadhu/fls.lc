﻿using FLS.LC.Web.Models;
using FLS.LC.Web.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using PagedList;
namespace FLS.LC.Web.Controllers.Admin
{
    public class RegUsersController : Controller
    {
        IAccountApiService _AccountApiService;
        public RegUsersController(IAccountApiService accountApiService)
        {
            _AccountApiService = accountApiService;
        }
        [HttpGet]
        public async Task<IActionResult> RegUsers()
        {
            string UserEmail = HttpContext.Session.GetString("UserEmail");
            var regUsers = await _AccountApiService.RegisteredUsersAsync(1);
            return View(regUsers);
        }
        [HttpPost]
        public async Task<IActionResult> RegUsers(int currentPageIndex)
        {
            var regUsers = await _AccountApiService.RegisteredUsersAsync(currentPageIndex);
            return View(regUsers);
        }
    }
}

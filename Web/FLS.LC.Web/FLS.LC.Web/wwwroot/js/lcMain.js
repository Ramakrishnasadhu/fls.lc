﻿$(document).ready(function () {
    $("#searchGarment").autocomplete({
        source: function (request, response) {
            var selectOptions = $('#selectedStore option:selected');
            if (selectOptions.length > 0) {
                var resultString = "";
                resultString = selectOptions.text();
                var usrEmail = $("#usrEmail").val();
            var model = {
                'usrEmail': usrEmail.trim(),
                'usrStoreName': resultString.trim(),
            }
            $.ajax({
                type: "GET",
                url: '@Url.Action("SearchGarment", "GarmentListListConfiguration")',
                data: model,
                success: function (data) {
                    response($.map(data, function (item) {
                        return item;
                    }))
                },
                error: function (xhr, textStatus, error) {
                    alert(xhr.statusText);
                    alert(textStatus);
                    alert(error);
                },
                failure: function (response) {
                    alert("failure " + response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#lblVendorName").text(i.item.value);
        },
        minLength: 3
    });
}
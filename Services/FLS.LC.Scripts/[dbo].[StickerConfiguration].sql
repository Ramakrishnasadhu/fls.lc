

USE [FLS.LC.Dev]
GO

/****** Object:  Table [dbo].[USER_STORE_DETAIL]    Script Date: 7/14/2021 12:56:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StickerConfiguration](
    [Stc_ID] [bigint] IDENTITY(1,1) NOT NULL,
    [StcUsremail] [nvarchar](250) NULL,
	[StcUstname] [nvarchar](250) NULL,
	[StcOption] [nvarchar](50) NULL,
	[StcPrintIsenable] [bit] NULL,
	[StcFontName] [nvarchar](50) NULL,
	[StcTExtAlign] [nvarchar](50) NULL,
	[StcTextSize] [int]	NULL,
	[StcTextStyle] [nvarchar](50) NULL,
	[CREATED_ON] [datetime] NULL,
	[CREATED_BY] [bigint] NULL,
	[DELETED_ON] [datetime] NULL,
	[DELETED_BY] [bigint] NULL,
	[MODIFIED_ON] [datetime] NULL,
	[MODIFIED_BY] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Stc_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



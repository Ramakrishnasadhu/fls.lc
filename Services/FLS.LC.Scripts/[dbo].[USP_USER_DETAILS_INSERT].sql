USE [FLS.LC.Dev]
GO

/****** Object:  StoredProcedure [dbo].[USP_USER_DETAILS_INSERT]    Script Date: 7/18/2021 3:46:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROC [dbo].[USP_USER_DETAILS_INSERT](
 @USR_USER_NAME NVARCHAR(MAX)
,@USR_PASSWORD VARCHAR(MAX)
,@USR_USER_EMAIL  NVARCHAR(MAX)
,@USR_USER_PHONE  VARCHAR(1000) = NULL
,@FIRST_NAME NVARCHAR(4000) = NULL
,@LAST_NAME NVARCHAR(4000)  = NULL
,@MIDDLE_NAME NVARCHAR(4000) = NULL
,@GENDER VARCHAR(10)
,@USER_ROLE VARCHAR(2000) = NULL
,@USER_TYPE TINYINT  = NULL
,@DOB DATETIME =NULL
,@ADDITIONAL_COMMENTS  NVARCHAR(MAX) = NULL
,@MARITAL_STATUS  VARCHAR(10) = NULL
)
AS

BEGIN
	

	DECLARE @USR_ID BIGINT;


	-----------------------  Inserting into users table ------------------------------------------------------------
	INSERT INTO dbo.USERS(
	[USR_USER_NAME], [USR_PASSWORD], [USR_USER_EMAIL], [USR_USER_PHONE],   [CREATED_ON], [USER_ROLE], [USER_TYPE] , USR_IS_ACTIVE , USR_WRONG_PASSWORD_ATTEMPT ,USR_IS_LOCKED, USR_IS_SUBSCRIBED
	)

	SELECT  @USR_USER_NAME , @USR_PASSWORD , @USR_USER_EMAIL , @USR_USER_PHONE , GETDATE() , @USER_ROLE , @USER_TYPE , 0 , 0 , 0 , 0

	SELECT @USR_ID = SCOPE_IDENTITY();

	-----------------------  Inserting into users table ------------------------------------------------------------


	----------------------------------- Inserting into user personal information --------------------------------------------------------------------------------
	INSERT INTO USER_PERSONAL_INFO (
	 [USR_ID], [FIRST_NAME], [LAST NAME], [MIDDLE_NAME], [EMAIL], [PHONE_NUMBER], [GENDER], [DOB], [ADDITIONAL_COMMENTS], [MARITAL_STATUS], [CREATED_ON]
	)

	SELECT @USR_ID , @FIRST_NAME , @LAST_NAME , @MIDDLE_NAME , @USR_USER_EMAIL , @USR_USER_PHONE ,@GENDER , @DOB , @ADDITIONAL_COMMENTS ,@MARITAL_STATUS , GETDATE()
	----------------------------------- Inserting into user personal information --------------------------------------------------------------------------------

	SELECT 'Success' as Result;

END
GO



USE [FLS.LC.Dev]
GO

/****** Object:  Table [dbo].[GarmentsCategory]    Script Date: 12/13/2021 3:50:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[USER_STORE_WEEK_OFF_HOLIDAY](
	[Ust_Week_Off_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Ust_Usremail] [nvarchar](250) NULL,
	[Ust_Ustname] [nvarchar](250) NULL,
	[Ust_Week_Off_Day] [nvarchar](250) NULL,
	[Ust_Week_Off_Day_No] [int],
	[CREATED_ON] [datetime] NULL,
	[CREATED_BY] [bigint] NULL,
	[DELETED_ON] [datetime] NULL,
	[DELETED_BY] [bigint] NULL,
	[MODIFIED_ON] [datetime] NULL,
	[MODIFIED_BY] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Ust_Week_Off_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO



USE [FLS.LC.Dev]
GO

SELECT [Gcs_ID]
      ,[GpcUsremail]
      ,[GpcUstname]
      ,[GcsEmployeeName]
      ,[GcsEmployeeAddress]
      ,[GcsEmployeeMobileNo]
      ,[GcsEmployeeEmail]
      ,[CREATED_ON]
      ,[CREATED_BY]
      ,[DELETED_ON]
      ,[DELETED_BY]
      ,[MODIFIED_ON]
      ,[MODIFIED_BY]
  FROM [dbo].[GarmentCheckingStaff]

GO


USE [FLS.LC.Dev]
GO

/****** Object:  Table [dbo].[GarmentsRemark]    Script Date: 12/13/2021 10:40:20 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GarmentReturnCause](
	[Grec_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GreReturnCauseDescription] [nvarchar](250) NULL,
	[GrecIsActive] [bit] NULL,
	[CREATED_ON] [datetime] NULL,
	[CREATED_BY] [bigint] NULL,
	[DELETED_ON] [datetime] NULL,
	[DELETED_BY] [bigint] NULL,
	[MODIFIED_ON] [datetime] NULL,
	[MODIFIED_BY] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Grec_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO



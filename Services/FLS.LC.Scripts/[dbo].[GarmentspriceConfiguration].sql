USE [FLS.LC.Dev]
GO

SELECT [Gpc_ID]
      ,[GpcUsremail]
      ,[GpcUstname]
      ,[GpcImage]
      ,[GpcPriceListName]
      ,[GpcIsenable]
      ,[GpcGarmentCategory]
      ,[GpcGarmentName]
      ,[GpcGarmentCode]
      ,[GpcGarmentService]
      ,[GpcGarmentServicePrice]
      ,[GpcGarmentMeasurementUnit]
      ,[GpcNoOfGarments]
      ,[CREATED_ON]
      ,[CREATED_BY]
      ,[DELETED_ON]
      ,[DELETED_BY]
      ,[MODIFIED_ON]
      ,[MODIFIED_BY]
  FROM [dbo].[GarmentspriceConfiguration]

GO



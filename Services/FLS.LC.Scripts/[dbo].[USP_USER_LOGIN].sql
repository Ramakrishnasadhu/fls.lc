USE [FLS.LC.Dev]
GO

/****** Object:  StoredProcedure [dbo].[USP_USER_LOGIN]    Script Date: 7/14/2021 12:54:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--exec USP_USER_LOGIN 'test','test',1
CREATE PROC [dbo].[USP_USER_LOGIN] (
 @USR_USER_NAME NVARCHAR(MAX) 
,@OPERATION INT = NULL
,@USR_PASSWORD NVARCHAR(MAX) = NULL
,@USR_IS_LOGIN BIT = NULL
,@USR_IS_ACTIVE BIT = NULL
)
AS
BEGIN

	DECLARE @USR_ID BIGINT;

	SELECT @USR_ID = USR_ID FROM dbo.USERS (NOLOCK) WHERE  [USR_USER_NAME] = @USR_USER_NAME AND USR_IS_ACTIVE = 1;

	--- CHECKING THE USER STATUS-----------------------------------------------------
	IF @OPERATION = 1 
	BEGIN

	IF ISNULL(@USR_USER_NAME,'') ! = '' AND ISNULL(@USR_ID,0) > 0
	BEGIN

		SELECT 1 AS RESULT , 'Valid User!' AS MESSAGE;
		  
		RETURN
	END
    ELSE
	BEGIN

		SELECT 0 AS RESULT , 'In-Valid User! or may be inactive' AS MESSAGE;
		RETURN
	END
	END

	------------- PASSWORD CHECKING  ----------------------------------------------------

	IF @OPERATION = 2 AND ISNULL(@USR_PASSWORD,'') ! = ''
	BEGIN

		IF EXISTS (SELECT 1 FROM dbo.USERS (NOLOCK) WHERE  CAST(USR_PASSWORD AS NVARCHAR(MAX)) = @USR_PASSWORD AND USR_ID = @USR_ID )
		BEGIN

			SELECT 1 AS RESULT , 'Valid Password!' AS MESSAGE;
		  
		    RETURN;

		END
		ELSE
		BEGIN
			
			SELECT 0 AS RESULT , 'In-Valid Password!' AS MESSAGE;

			UPDATE U SET USR_WRONG_PASSWORD_ATTEMPT  = ISNULL(USR_WRONG_PASSWORD_ATTEMPT,0)+1
			FROM dbo.USERS  U (NOLOCK) WHERE  USR_ID = @USR_ID

		    RETURN;

		END
	END

	/*--------------------------------------------------------------------------------------------------------------------------------
	                              Login audit details insesert starts
	----------------------------------------------------------------------------------------------------------------------------------*/
	
	IF @OPERATION = 3  --- User Login status updating
	BEGIN
			
		UPDATE  U SET USR_IS_LOGIN = 1 , USR_LAST_LOGIN_DATE = GETDATE() , USR_WRONG_PASSWORD_ATTEMPT = 0
	    FROM dbo.USERS  U (NOLOCK) WHERE  USR_ID = @USR_ID

	END
	IF @OPERATION = 4  -- USERS ACTIVATION.
	BEGIN
	
		
		UPDATE  U SET [USR_IS_ACTIVE] = @USR_IS_ACTIVE
	    FROM dbo.USERS  U (NOLOCK) WHERE  USR_ID = @USR_ID;

		SELECT 1 RESULT;

	END                  
END
GO



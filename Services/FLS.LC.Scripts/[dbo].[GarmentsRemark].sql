USE [FLS.LC.Dev]
GO

/****** Object:  Table [dbo].[GarmentsCategory]    Script Date: 12/2/2021 4:05:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GarmentCheckingStaff](
	[Gcs_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GpcUsremail] [nvarchar](250) NULL,
	[GpcUstname] [nvarchar](250) NULL,
	[GcsEmployeeName] [nvarchar](250) NULL,
	[GcsEmployeeAddress] [nvarchar](250) NULL,
	[GcsEmployeeMobileNo] [bigint],
	[GcsEmployeeEmail] [nvarchar](250) NULL,
	[CREATED_ON] [datetime] NULL,
	[CREATED_BY] [bigint] NULL,
	[DELETED_ON] [datetime] NULL,
	[DELETED_BY] [bigint] NULL,
	[MODIFIED_ON] [datetime] NULL,
	[MODIFIED_BY] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Gcs_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO



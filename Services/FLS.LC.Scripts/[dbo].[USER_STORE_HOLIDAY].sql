USE [FLS.LC.Dev]
GO

/****** Object:  Table [dbo].[USER_STORE_HOLIDAY]    Script Date: 12/17/2021 6:19:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[USER_STORE_HOLIDAY](
	[Usth_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Usth_Usremail] [nvarchar](250) NULL,
	[Usth_Ustname] [nvarchar](250) NULL,
	[Usth_Holiday_Description] [nvarchar](250) NULL,
	[Usth_Holiday_Date]  [nvarchar](250) NULL,
	[CREATED_ON] [datetime] NULL,
	[CREATED_BY] [bigint] NULL,
	[DELETED_ON] [datetime] NULL,
	[DELETED_BY] [bigint] NULL,
	[MODIFIED_ON] [datetime] NULL,
	[MODIFIED_BY] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Usth_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO



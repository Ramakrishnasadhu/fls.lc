﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FLS.LC.Cloud.Account.DTO.Common
{
    public static class OperationErrorDictionary
    {
        public static class User
        {
            public static OperationError UserLocked() =>
              new OperationError("Unfortunately the user specified is In-Valid User! or may be Locked.");

            public static OperationError UserInValid() =>
               new OperationError("Unfortunately the user specified is In-Valid User! or may be inactive or may be Locked.");

            public static OperationError UserDoesNotExist() =>
               new OperationError("Unfortunately the user specified in the system does not exist.");
            public static OperationError UserRegistrationStatus() =>
               new OperationError("Email Sent Successfully.");

            public static OperationError UserPasswordInvalid() =>
               new OperationError("In-Valid Password!");
            public static OperationError UserCurrentPasswordInvalid() =>
               new OperationError("Current Password in the System is In-Valid Password!");

            public static OperationError UserFirstLogin() =>
               new OperationError("User need to change Password!");
            public static OperationError UserAlreadyExist() =>
                new OperationError("Unfortunately the user specified in the system already exist.");

            public static OperationError UserRegistrationFails() =>
                new OperationError("Error in User Registration in the System.Please try again");


            


        }
    }
}

﻿using FLS.LC.Cloud.Account.DataModels.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FLS.LC.Cloud.Account.DTO.Account
{
    public class UsersModel
    {
        ///<summary>
        /// Gets or sets Customers.
        ///</summary>
        public List<RegUsers> Users { get; set; }

        ///<summary>
        /// Gets or sets CurrentPageIndex.
        ///</summary>
        public int CurrentPageIndex { get; set; }

        ///<summary>
        /// Gets or sets PageCount.
        ///</summary>
        public int PageCount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FLS.LC.Cloud.Account.DTO.Account
{
    /// <summary>
    /// Login User
    /// </summary>
    public class LoginUser
    {
        /// <summary>
        /// Gets or sets the UserName.
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Gets or sets the Password.
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Gets or sets the Email.
        /// </summary>
        public string UserEmail { get; set; }
        /// <summary>
        /// Gets or sets the Operation.
        /// </summary>
        public int Operation { get; set; }

        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }

    }
}

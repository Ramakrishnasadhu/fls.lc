using System;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using FLS.LC.Cloud.Account.DataModels.Entities;

namespace FLS.LC.Cloud.Account.DTO
{

    /// <summary>
    /// UserReg
    /// </summary>
    public class UserReg
    {
        /// <summary>
        /// Gets or sets the UserName.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the Email.
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Gets or sets the Phone NO.
        /// </summary>
        public string PhoneNo { get; set; }

        /// <summary>
        /// Gets or sets the Gender.
        /// </summary>
        public string Gender { get; set; }
    }
}
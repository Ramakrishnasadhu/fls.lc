using System;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using FLS.LC.Cloud.Account.DataModels.Entities;

namespace FLS.LC.Cloud.Account.DTO
{
    [DataContract]
    public class SampleDto
    {
        [DataMember]
        [JsonProperty("month")]
        public string Month { get; set; }

        [DataMember]
        [JsonProperty("day")]
        public string Day { get; set; }

        [DataMember]
        [JsonProperty("year")]
        public string Year { get; set; }

        [DataMember]
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FLS.LC.Cloud.Account.DTO
{
    /// <summary>
    /// ResponseMessage
    /// </summary>
    public class ResponseMessage
    {
        /// <summary>
        /// Message
        /// </summary>
        [StringLength(maximumLength: int.MaxValue)]
        public string Message { get; set; }
    }
}

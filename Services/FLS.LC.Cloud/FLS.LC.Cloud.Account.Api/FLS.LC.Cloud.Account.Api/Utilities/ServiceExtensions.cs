﻿using FLS.LC.Cloud.Account.BusinessService.Interfaces;
using FLS.LC.Cloud.Account.BusinessService.Services;
using FLS.LC.Cloud.Account.DataModels.DataContext;
using FLS.LC.Cloud.Account.Repository.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
namespace FLS.LC.Cloud.Account.Api.Utilities
{
    public static class ServiceExtensions
    {
        /// <summary>
        /// Register All the Components with this Extension Method, All the Business Services and Repositories used in this Project
        /// must be registered here for Enabling Dependency Injection
        /// </summary>
        /// <param name="services"></param>
        /// <returns>Service Collection</returns>
        public static IServiceCollection RegisterServices(
            this IServiceCollection services)
        {
            services.AddTransient<ITest, TestService>();
            services.AddTransient<IAccount, AccountService>();
            return services;
        }

        /// <summary>
        /// Register DB Context for the Project in this Extension Method.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="connectionString">Connection String for the Main Moduel DB</param>
        /// <returns></returns>
        public static IServiceCollection RegisterDatabaseContext(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<AccountContext>(options =>
            {
                options.UseSqlServer(connectionString);
            })
           .AddUnitOfWork<AccountContext>();
            return services;
        }
    }
}

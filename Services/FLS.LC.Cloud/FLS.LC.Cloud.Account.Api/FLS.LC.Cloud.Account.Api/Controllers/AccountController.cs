﻿using AutoMapper;
using FLS.LC.Cloud.Account.Api.Utilities;
using FLS.LC.Cloud.Account.BusinessService.Interfaces;
using FLS.LC.Cloud.Account.DTO;
using FLS.LC.Cloud.Account.DTO.Account;
using FLS.LC.Cloud.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Cloud.Account.Api.Controllers
{
    /// <summary>
    /// Account Controller
    /// </summary>
    [Route("Account")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private IAccount _Account;
        private IMapper _mapper;

        /// <summary>
        /// Constructor for Account Controller
        /// </summary>
        public AccountController(IAccount IAccount, IMapper mapper)
        {
            _Account = IAccount;
            _mapper = mapper;
        }
        #region C O N ST A N T S 
        private const string RegisterUser_LOG = "Account/RegisterUserAsync API called";
        private const string HEADER_CONSTANT_XCOUNT = "X-Total-Count";
        #endregion

        /// <summary>
        /// API to Register User service
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IActionResult))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("UserRegister")]
        public async Task<IActionResult> UserRegisterAsync(UserReg UserReg)
        {
            try
            {
                var operationResult = await _Account.UserRegisterAsync(UserReg);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
        /// <summary>
        /// API to Get Registered Users   
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<RegUsers>))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("RegisteredUser")]
        public async Task<ActionResult> RegisteredUsersAsync(UsersModel userModel)
        {
            try
            {
                var response = await _Account.RegisteredUsersAsync(userModel.CurrentPageIndex);
                this.Response.Headers.Add(HEADER_CONSTANT_XCOUNT, response.XTotalCount);
                return StatusCode((int)response.StatusCode, response.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
        /// <summary>
        /// API to Check Login User   
        /// </summary>
        /// <param name="userName">userName</param>
        /// <param name="passWord">passWord</param>
        /// <param name="operation">operation</param>
        /// <returns></returns>

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IActionResult))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("LoginUser")]
        public async Task<ActionResult> LoginUser(LoginUser loginUser)
        {
            try
            {
                var operationResult = await _Account.LoginUserAsync(loginUser);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
        /// <summary>
        /// API to Check User First Login   
        /// </summary>
        /// <param name="userName">userName</param>
        /// <param name="passWord">passWord</param>
        /// <param name="operation">operation</param>
        /// <returns></returns>

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IActionResult))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("FirstLogin")]
        public async Task<ActionResult> FirstLoginAsync(LoginUser loginUser)
        {
            try
            {
                var operationResult = await _Account.FirstLoginAsync(loginUser);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
        /// <summary>
        /// API to Forgot Password   
        /// </summary>
        /// <param name="userEmail">userEmail</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseMessage))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("ForgotPassword")]
        public async Task<IActionResult> ForgotPasswordAsync(LoginUser LoginUser)
        {
            try
            {
                var operationResult = await _Account.ForgotPasswordAsync(LoginUser.UserEmail);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }

        }
        /// <summary>
        /// API to Change Password   
        /// </summary>
        /// <param name="userEmail">userEmail</param>
        /// <param name="userEmail">currentPassword</param>
        /// <param name="userEmail">NewPassword</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseMessage))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("ChangePassword")]
        public async Task<IActionResult> ChangePasswordAsync(LoginUser LoginUser)
        {
            try
            {

                var operationResult = await _Account.ChangePasswordAsync(LoginUser.UserEmail, LoginUser.CurrentPassword, LoginUser.NewPassword);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }

        }
    }
}


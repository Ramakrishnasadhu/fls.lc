﻿using AutoMapper;
using FLS.LC.Cloud.Account.BusinessService.Interfaces;
using FLS.LC.Cloud.Account.DTO;
using FLS.LC.Cloud.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Cloud.Account.Api.Controllers
{
    [Route("Test")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private ITest _Test;
        private IMapper _mapper;

        /// <summary>
        /// Constructor for Test Controller
        /// </summary>
        public TestController(ITest ITest, IMapper mapper)
        {
            _Test = ITest;
            _mapper = mapper;
        }


        #region C O N ST A N T S 
        private const string Test_LOG = "Test/TestServiceAsync API called";
        private const string GET_ALL_LOG = "Test/AllAsync API called";
        private const string HEADER_CONSTANT_XCOUNT = "X-Total-Count";
        #endregion

        /// <summary>
        /// API to test service
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpGet(nameof(TestServicesync))]
        public string TestServicesync()
        {
            return "I am Live";
        }
        /// <summary>
        /// API to Get All service
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<SampleDto>))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpGet, Route("AllAsync")]
        public async Task<ActionResult> AllAsync()
        {
            try
            {
                var result = await _Test.All();
                this.Response.Headers.Add(HEADER_CONSTANT_XCOUNT, result.XTotalCount);
                return StatusCode((int)result.StatusCode, result.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
    }
}

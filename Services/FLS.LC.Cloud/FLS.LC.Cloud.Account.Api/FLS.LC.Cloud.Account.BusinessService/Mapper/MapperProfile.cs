﻿using AutoMapper;
using FLS.LC.Cloud.Account.DataModels.Entities;
using FLS.LC.Cloud.Account.DTO;
using FLS.LC.Cloud.Account.DTO.Account;
using System;
using System.Collections.Generic;
using System.Text;

namespace FLS.LC.Cloud.Account.BusinessService.Mapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            AllowNullDestinationValues = true;

            CreateMap<Users, RegUsers>().ReverseMap();
            //CreateMap<Users, UserReg>().IgnoreAllPropertiesWithAnInaccessibleSetter()
            //    .ForMember(ev => ev.UserName, m => m.MapFrom(a => a.UsrUserName))
            //    .ForMember(ev => ev.Password, m => m.MapFrom(a => a.UsrPassword))
            //    .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.UsrUserEmail))
            //     .ForMember(des => des.PhoneNo, option => option.MapFrom(src => src.UsrUserPhone))
            //     .ForMember(des => des.IsActive, option => option.MapFrom(src => src.UsrIsActive));

            //   CreateMap<Sample, SampleDto>().ReverseMap();
            //CreateMap<PayorEntity, PayorEntityDto>().ReverseMap();

        }
    }
}

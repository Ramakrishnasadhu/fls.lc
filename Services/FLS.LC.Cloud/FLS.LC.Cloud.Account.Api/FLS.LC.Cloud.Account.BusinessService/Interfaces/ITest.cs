﻿using FLS.LC.Cloud.Account.DTO;
using FLS.LC.Cloud.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LC.Cloud.Account.BusinessService.Interfaces
{
    public interface ITest
    {
        Task<ActionReturnType> All();
    }
}

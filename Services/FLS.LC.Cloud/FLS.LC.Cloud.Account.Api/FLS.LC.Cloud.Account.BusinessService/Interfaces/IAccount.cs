﻿using FLS.LC.Cloud.Account.DataModels.Entities;
using FLS.LC.Cloud.Account.DTO;
using FLS.LC.Cloud.Account.DTO.Account;
using FLS.LC.Cloud.Account.DTO.Common;
using FLS.LC.Cloud.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LC.Cloud.Account.BusinessService.Interfaces
{
    public interface IAccount
    {
        Task<OperationResponse<Users>> UserRegisterAsync(UserReg UserReg);
        Task<ActionReturnType> RegisteredUsersAsync(int currentPage);
        Task<OperationResponse<Users>> LoginUserAsync(LoginUser LoginUser);
        Task<OperationResponse<Users>> FirstLoginAsync(LoginUser LoginUser);
        Task<OperationResponse<Users>> ForgotPasswordAsync(string userEmail);
        Task<OperationResponse<Users>> ChangePasswordAsync(string userEmail, string currentPassword, string NewPassword);
    }
   
}

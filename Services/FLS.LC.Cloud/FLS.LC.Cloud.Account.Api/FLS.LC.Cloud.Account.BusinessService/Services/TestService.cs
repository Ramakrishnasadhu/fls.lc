﻿using AutoMapper;
using FLS.LC.Cloud.Account.BusinessService.Interfaces;
using FLS.LC.Cloud.Account.DataModels.Entities;
using FLS.LC.Cloud.Account.DTO;
using FLS.LC.Cloud.Account.Repository.UnitOfWork;
using FLS.LC.Cloud.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LC.Cloud.Account.BusinessService.Services
{
    public class TestService : ITest
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly string _timeZone;
        public TestService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ActionReturnType> All()
        {
            try
            {
                //var result = await (from p in _unitOfWork.GetRepository<Sample>().Get()
                //                      select p).ToListAsync();
                //var resultResponse = _mapper.Map<List<SampleDto>>(result);
                //         return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, resultResponse, resultResponse.Count);
                return null;
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }
    }
}

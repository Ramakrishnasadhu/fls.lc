﻿using AutoMapper;
using FLS.LC.Cloud.Account.BusinessService.Interfaces;
using FLS.LC.Cloud.Account.DataModels.Entities;
using FLS.LC.Cloud.Account.DataModels.ReturnEntities;
using FLS.LC.Cloud.Account.DTO;
using FLS.LC.Cloud.Account.DTO.Account;
using FLS.LC.Cloud.Account.DTO.Common;
using FLS.LC.Cloud.Account.Repository.UnitOfWork;
using FLS.LC.Cloud.Security;
using FLS.LC.Cloud.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LC.Cloud.Account.BusinessService.Services
{
    public class AccountService : IAccount
    {
        #region C O N S T A N T S
        private const string POST_ACCOUNT_USER_REGISTRATION = "EXEC USP_USER_DETAILS_INSERT";
        #endregion
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly string _timeZone;
        private readonly string _sharedSecretkey;
        public AccountService(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _configuration = configuration;
            _timeZone = _configuration.GetValue<string>("TimeZone:DatabaseTimeZone");
            _sharedSecretkey = _configuration.GetValue<string>("SharedKey:SharedSecretkey");
        }

        public async Task<OperationResponse<Users>> ChangePasswordAsync(string userEmail, string currentPassword, string NewPassword)
        {
            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == userEmail).SingleOrDefaultAsync());
            Users userResponseItem = _mapper.Map<Users>(userDetails);
            if (userDetails.IsNull())
            {
                return new OperationResponse<Users>()
                                        .SetAsFailureResponse(OperationErrorDictionary.User.UserDoesNotExist());
            }
            else
            {
                if (userDetails.UsrIsFirstLogin == true && userDetails.UsrPassword != currentPassword)
                    return new OperationResponse<Users>()
                                       .SetAsFailureResponse(OperationErrorDictionary.User.UserCurrentPasswordInvalid());
                else if (userDetails.UsrIsFirstLogin == true)
                {
                    userResponseItem.UsrPassword = EncryptDecryptUtil.Encrypt(_sharedSecretkey, NewPassword);
                    userResponseItem.UsrIsFirstLogin = false;
                    userDetails.UsrWrongPasswordAttempt = 0;
                    userResponseItem.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                    _unitOfWork.GetRepository<Users>().Update(userResponseItem);
                    await _unitOfWork.SaveChangesAsync();
                    return new OperationResponse<Users>(userResponseItem);
                }
                else if (userDetails.UsrIsFirstLogin == false && EncryptDecryptUtil.Decrypt(_sharedSecretkey, userDetails.UsrPassword) != currentPassword)
                    return new OperationResponse<Users>()
                                       .SetAsFailureResponse(OperationErrorDictionary.User.UserCurrentPasswordInvalid());
                else if (userDetails.UsrIsFirstLogin == false && EncryptDecryptUtil.Decrypt(_sharedSecretkey, userDetails.UsrPassword) == currentPassword)
                {
                    userResponseItem.UsrPassword = EncryptDecryptUtil.Encrypt(_sharedSecretkey, NewPassword);
                    userResponseItem.UsrIsFirstLogin = false;
                    userDetails.UsrWrongPasswordAttempt = 0;
                    userResponseItem.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                    _unitOfWork.GetRepository<Users>().Update(userResponseItem);
                    await _unitOfWork.SaveChangesAsync();
                    return new OperationResponse<Users>(userResponseItem);
                }
            }
            return new OperationResponse<Users>(userResponseItem);
        }
        public async Task<OperationResponse<Users>> ForgotPasswordAsync(string userEmail)
        {
            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == userEmail).SingleOrDefaultAsync());
            if (userDetails.IsNull())
            {
                return new OperationResponse<Users>()
                                       .SetAsFailureResponse(OperationErrorDictionary.User.UserDoesNotExist());
            }
            else
            {
                if (userDetails.UsrIsActive == false || userDetails.UsrIsLocked == true)
                    return new OperationResponse<Users>()
                                       .SetAsFailureResponse(OperationErrorDictionary.User.UserInValid());
                Users userResponseItem = _mapper.Map<Users>(userDetails);
                string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
                string sRandomOTP = GeneratePassword.GenerateRandomOTP(8, saAllowedCharacters);
                userResponseItem.UsrPassword = sRandomOTP;
               // userResponseItem.UsrPassword = EncryptDecryptUtil.Encrypt(_sharedSecretkey, sRandomOTP);
                userResponseItem.UsrIsFirstLogin = true;
                userResponseItem.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                _unitOfWork.GetRepository<Users>().Update(userResponseItem);
                await _unitOfWork.SaveChangesAsync();
                return new OperationResponse<Users>(userResponseItem);
            }
        }

        public async Task<OperationResponse<Users>> FirstLoginAsync(LoginUser LoginUser)
        {
            
                var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == LoginUser.UserEmail
                && x.UsrIsActive == true || x.UsrIsLocked == false).SingleOrDefaultAsync());

                if (userDetails == null)
                    return new OperationResponse<Users>()
                                           .SetAsFailureResponse(OperationErrorDictionary.User.UserInValid());
                else if (userDetails != null)
                {
                    if (userDetails.UsrIsFirstLogin == true)
                        return new OperationResponse<Users>(userDetails);
                    else if (userDetails.UsrIsFirstLogin == false)
                    {
                        return new OperationResponse<Users>()
                                           .SetAsFailureResponse(OperationErrorDictionary.User.UserFirstLogin());
                    }
                }
                return new OperationResponse<Users>(userDetails);
        }

        public async Task<OperationResponse<Users>> LoginUserAsync(LoginUser LoginUser)
        {
            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == LoginUser.UserEmail
            && x.UsrIsActive == true && x.UsrIsLocked == false).SingleOrDefaultAsync());

            if (userDetails == null)
                return new OperationResponse<Users>()
                                       .SetAsFailureResponse(OperationErrorDictionary.User.UserInValid());
            else if (userDetails != null)
            {
                if (EncryptDecryptUtil.Decrypt(_sharedSecretkey, userDetails.UsrPassword) == LoginUser.Password)
                {
                    userDetails.UsrWrongPasswordAttempt = 0;
                    userDetails.UsrLastLoginDate = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                    userDetails.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                    _unitOfWork.GetRepository<Users>().Update(userDetails);
                    await _unitOfWork.SaveChangesAsync();
                    return new OperationResponse<Users>(userDetails);
                }
                else
                {
                    userDetails.UsrWrongPasswordAttempt += 1;
                    if (userDetails.UsrWrongPasswordAttempt == 3)
                    {
                        userDetails.UsrIsActive = false;
                        userDetails.UsrIsLocked = true;
                        userDetails.UsrIsLogin = false;
                    }
                    userDetails.UsrLastLoginDate = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                    userDetails.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                    _unitOfWork.GetRepository<Users>().Update(userDetails);
                    await _unitOfWork.SaveChangesAsync();
                    return new OperationResponse<Users>()
                                       .SetAsFailureResponse(OperationErrorDictionary.User.UserPasswordInvalid());
                }
            }
            return new OperationResponse<Users>(userDetails);
        }

        public async Task<ActionReturnType> RegisteredUsersAsync(int currentPage)
        {
            int maxRows = 1;
            UsersModel UsersModel = new UsersModel();
            var result=await (_unitOfWork.GetRepository<Users>().Get().ToListAsync());
            List<RegUsers> resCount = _mapper.Map<List<RegUsers>>(result);
            var resultRegUsers = await (_unitOfWork.GetRepository<Users>().Get().Skip((currentPage-1)* maxRows).Take(maxRows).ToListAsync());
            if (resultRegUsers.IsListNullOrEmpty())
            {
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
            }
            else
            {
                List<RegUsers> response = _mapper.Map<List<RegUsers>>(resultRegUsers);
                UsersModel.Users = response;
                double pageCount = (double)((decimal)resCount.Count / Convert.ToDecimal(maxRows));
                UsersModel.PageCount = (int)Math.Ceiling(pageCount);
                UsersModel.CurrentPageIndex = currentPage;
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, UsersModel, UsersModel.Users.Count());
            }

        }
        public async Task<OperationResponse<Users>> UserRegisterAsync(UserReg UserReg)
        {
            try
            {
                var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == UserReg.Email).SingleOrDefaultAsync());
                if (userDetails != null)
                {
                    return new OperationResponse<Users>()
                                           .SetAsFailureResponse(OperationErrorDictionary.User.UserAlreadyExist());
                }
                else
                {
                    bool flag = false;
                    var message = "";
                    var sqlQuery = new StringBuilder();
                    var parameters = new List<SqlParameter>();
                    DataHelper.AddParams("USR_USER_NAME", UserReg.UserName, ref sqlQuery, ref parameters);
                    string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
                    string sRandomOTP = GeneratePassword.GenerateRandomOTP(8, saAllowedCharacters);
                    DataHelper.AddParams("USR_PASSWORD", EncryptDecryptUtil.Encrypt(_sharedSecretkey, sRandomOTP), ref sqlQuery, ref parameters);
                    DataHelper.AddParams("USR_USER_EMAIL", UserReg.Email, ref sqlQuery, ref parameters);
                    DataHelper.AddParams("USR_USER_PHONE", UserReg.PhoneNo, ref sqlQuery, ref parameters);
                    DataHelper.AddParams("GENDER", UserReg.Gender, ref sqlQuery, ref parameters);
                    var resultUserReg = await _unitOfWork.GetRepository<UserReg_RE>().FromSql($"{POST_ACCOUNT_USER_REGISTRATION} {sqlQuery.ToString() }", parameters.ToArray()).FirstOrDefaultAsync();
                    if (resultUserReg.Result.ToUpper() == "SUCCESS")
                    {

                        return new OperationResponse<Users>(userDetails);
                    }
                    else
                    {
                        return new OperationResponse<Users>()
                                           .SetAsFailureResponse(OperationErrorDictionary.User.UserRegistrationFails());
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            }
    }
}

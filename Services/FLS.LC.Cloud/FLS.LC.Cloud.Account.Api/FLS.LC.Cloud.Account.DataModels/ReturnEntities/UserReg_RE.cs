﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FLS.LC.Cloud.Account.DataModels.ReturnEntities
{
    public class UserReg_RE
    {
        [Key]
        [StringLength(maximumLength: int.MaxValue)]
        [Column("Result")]
        public string Result { get; set; }
    }
}

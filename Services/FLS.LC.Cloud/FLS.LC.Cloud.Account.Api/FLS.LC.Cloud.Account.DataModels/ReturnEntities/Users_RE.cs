﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FLS.LC.Cloud.Account.DataModels.ReturnEntities
{
    public class Users_RE
    {
        #region CONSTANT
        private const int MAX_LENGTH_60 = 60;
        private const int MAX_LENGTH_500 = 500;
        private const int MAX_LENGTH_1064 = 1064;
        private const int MAX_LENGTH_1000 = 1000;
        private const int MAX_LENGTH_1050 = 1050;
        private const int MAX_LENGTH_200 = 200;
        #endregion

        [Column("USR_ID")]
        public long UsrId { get; set; }
        [StringLength(maximumLength: MAX_LENGTH_60)]
        [Column("USR_USER_NAME")]
        public string UsrUserName { get; set; }
        [StringLength(maximumLength: MAX_LENGTH_60)]
        [Column("USR_USER_EMAIL")]
        public string UsrUserEmail { get; set; }
        [StringLength(maximumLength: MAX_LENGTH_60)]
        [Column("USR_USER_PHONE")]
        public string UsrUserPhone { get; set; }
        [Column("USR_IS_ACTIVE")]
        public bool UsrIsActive { get; set; }
        [Column("USR_IS_LOGIN")]
        public bool UsrIsLogin { get; set; }
        [StringLength(maximumLength: MAX_LENGTH_60)]
        [Column("USR_MODULES")]
        public string UsrModules { get; set; }
        [Column("USR_IS_LOCKED")]
        public bool UsrIsLocked { get; set; }
        [Column("USER_ROLE")]
        public bool UserRole { get; set; }
        [StringLength(maximumLength: MAX_LENGTH_60)]
        [Column("USER_TYPE")]
        public string UserType { get; set; }

    }
}

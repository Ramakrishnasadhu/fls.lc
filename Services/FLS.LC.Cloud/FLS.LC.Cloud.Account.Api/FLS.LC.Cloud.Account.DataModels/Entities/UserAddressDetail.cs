﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

#nullable disable

namespace FLS.LC.Cloud.Account.DataModels.Entities
{
    public partial class UserAddressDetail
    {
        public long UadId { get; set; }
        public long? AdUsrId { get; set; }
        public byte? AdAddressType { get; set; }
        public string AdFlotDetail { get; set; }
        public string AdAreaDetail { get; set; }
        public string AdLandMark { get; set; }
        public string AdCity { get; set; }
        public string AdState { get; set; }
        public string AdPincode { get; set; }
        public long? AdStId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}
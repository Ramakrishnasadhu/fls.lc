﻿using Microsoft.AspNetCore.Http;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Caching;
using System.Text;

namespace FLS.LC.Cloud.Utilities
{
    public class ActionReturnType
    {
        public HttpStatusCode StatusCode { get; set; }
        public string XTotalCount { get; set; }
        public object ResultSet { get; set; }
        public object ContextData { get; set; }
    }
    public class ActionResultType<T>
    {
        public HttpStatusCode StatusCode { get; set; }
        public int? XTotalCount { get; set; }
        public T ResultSet { get; set; }
        public List<Error> Errors { get; set; }
      //  public ContextLocation location { get; set; }
    }

    public interface IResult
    {
        List<Error> Error { get; set; }
    }
    public class Result<T> : IResult
    {
        private readonly Error _error;
        public Result(Exception ex)
        {
            var errMsg = ex.InnerException != null ? ex.InnerException.InnerException != null ? ex.InnerException.InnerException.Message : ex.InnerException.Message : ex.Message;
            _error = new Error(ValidationCategory.ERROR, ValidationCode.EXCEPTION, errMsg, "N/A");
            this.Error = new List<Error>() { _error };
        }
        public Result()
        {
            _error = new Error(ValidationCategory.NOERROR, ValidationCode.NO_ERROR, "No Error !", "N/A");
            this.Error = new List<Error> { _error };
        }

        public List<Error> Error { get; set; }
    }

    public class Error
    {
        public Error(ValidationCategory category, ValidationCode code, string message, string field)
        {
            this.ValidationCategory = category.ToString();
            this.ValidationCode = code.ToString();
            this.Message = message;
            this.Field = field;
        }
        /// <summary>
        /// Type of Validation
        /// <example>
        /// example : VALIDATION, ERROR
        /// </example>
        /// </summary>
        [JsonProperty("category")]
        public string ValidationCategory { get; set; }
        /// <summary>
        /// Validation Code
        /// <example>
        /// example : INVALID_TYPE, INVALID_ID, MISSING_TYPE, NOT_FOUND, IS_REQUIRED, SYSTEM_VALIDATION, MAX_LENGTH, MIN_LENGTH, EXCEPTION, INCORRECT_TYPE 
        /// </example>
        /// </summary>
        [JsonProperty("code")]
        public string ValidationCode { get; set; }
        /// <summary>
        /// Description of Validation
        /// </summary>
        [JsonProperty("detail")]
        public string Message { get; set; }
        /// <summary>
        /// Validation field
        /// </summary>
        [JsonProperty("field")]
        public string Field { get; set; }

    }

    public enum ValidationCategory
    {
        NOERROR = 0,
        VALIDATION = 1,
        ERROR = 2
    }
    public enum ValidationCode
    {
        NO_ERROR = 0,
        INVALID_TYPE = 1,
        INVALID_ID = 2,
        MISSING_TYPE = 3,
        NOT_FOUND = 4,
        IS_REQUIRED = 5,
        SYSTEM_VALIDATION = 6,
        MAX_LENGTH = 7,
        EXCEPTION = 8,
        MIN_LENGTH = 9,
        INCORRECT_TYPE = 10,
        UN_AUTHORIZED = 11,
        INVALID_DATA = 12,
        DATABASE = 13,
        VALIDATION_SUMMARY = 14,
        IN_CORRECT = 15
    }
    public enum ValidationType
    {
        DATA_TYPE = 1,
        NOT_FOUND = 2,
        REQUIRED_FIELD = 3,
        FIELD = 4,
        MIN_LENGTH = 5,
        MAX_LENGTH = 6,
        REQUIRED_INPUT = 7,
        NO_DATA_FOUND = 8,
        SYSTEM = 9,
        IN_CORRECT = 10,
        INVALID_DATA = 11,
        CUSTOM_VALIDATION = 12,
        DATABASE = 13,
        CUSTOM_REQUIRED
    }

    public enum APIModule
    {
        Account = 1,
        Activity,
        Alert,
        Appointment,
        Assessment,
        Audit,
        Authorization,
        CarePlan,
        CareTeam,
        ConsentForm,
        Document,
        DocumentIO,
        Encounter,
        Eligibility,
        Letter,
        Master,
        Medication,
        Member,
        Payor,
        Profile,
        Provider,
        Risk,
        ServicePlan,
        IntegrationAPI,
        SDOHAPI,
        SmartComm
    }

    public enum UrlPathName
    {
        AuthorizationDetail,
        AuthorizationDocumentFile,
        ServicePlan,
        AuthorizationNote,
        AuthorizationDocument,
        AuthorizationActivity,
        AuthorizationGuideLineDocument,
        AuthDocFromDocio,
        AssessmentDetail
    }

    public enum SDOHType
    {
        Healthify = 0,
        AuntBertha = 1
    }

    public static class ActionSet
    {
        public static ActionReturnType ActionReturnType(HttpStatusCode statusCode, object resultSet = null, int xTotalCount = 0, object contextData = null, string transactionId = "")
        {
            var actionResult = new ActionReturnType
            {
                StatusCode = statusCode,
                ResultSet = resultSet,
                XTotalCount = Convert.ToString(xTotalCount),
                ContextData = contextData
            };
            return actionResult;
        }

        public static ActionResultType<T> ActionResultType<T>(HttpStatusCode statusCode, T resultSet = default(T), int xTotalCount = 0, ContextLocation location = null)
        {
            var actionResult = new ActionResultType<T>
            {
                StatusCode = statusCode,
                ResultSet = resultSet,
                XTotalCount = (xTotalCount == 0) ? null : (int?)xTotalCount,
               // location = location
            };
            return actionResult;
        }
        public static ActionResultType<T> ActionResultType<T>(HttpStatusCode statusCode, List<Error> errors)
        {
            var actionResult = new ActionResultType<T>
            {
                StatusCode = statusCode,
                Errors = errors,
                ResultSet = default(T)
            };
            return actionResult;
        }
    }

    public static class ResourceFileInformation
    {
        public static string GetUrlPaths(string key)
        {
            //  return UrlPaths.ResourceManager.GetString(key);
            return string.Empty;
        }
    }
    public class APIManagerKey
    {
        public string Name { get; set; }
        public string Version { get; set; }
    }

    public class UrlBuilder
    {
        public string GetUrl(HttpRequest request, string baseUrl)
        {
            StringBuilder url = new StringBuilder();
            //Need to check for some alternate till then commiting this code
            //var claimDetails = (ClaimsIdentity)request.HttpContext.User.Identity;
            //var contextPath = claimDetails.Claims.Where(i => i.Type == ClaimTypes.System).Select(i => i.Value).FirstOrDefault();
            url.Append(baseUrl);
            //if (contextPath != null)
            //{
            //    url.Append(contextPath);
            //}
            return url.ToString();
        }

        public static Dictionary<string, APIManagerKey> GetAPIManagerKeys(string versionMappingFile)
        {
            var apiManagerKeys = new Dictionary<string, APIManagerKey>();
            apiManagerKeys = JsonConvert.DeserializeObject<Dictionary<string, APIManagerKey>>(FileIoAccess.ReadAllText(versionMappingFile));
            return apiManagerKeys;
        }
        public static string GetModuleUrl(string versionMappingFile, APIModule module, bool isSSO)
        {
            try
            {
                ObjectCache cache = System.Runtime.Caching.MemoryCache.Default;
                Dictionary<string, APIManagerKey> versionMappings = cache["VersionMapping"] as Dictionary<string, APIManagerKey>;
                if (versionMappings.IsListNullOrEmpty())
                {
                    versionMappings = GetAPIManagerKeys(versionMappingFile);
                    CacheItemPolicy policy = new CacheItemPolicy();
                    List<string> filePaths = new List<string>
                {
                    versionMappingFile
                };
                    policy.ChangeMonitors.Add(new HostFileChangeMonitor(filePaths));
                    cache.Set("VersionMapping", versionMappings, policy);
                }

#if DEBUG
                return "";
#else
            if (isSSO)
            {
                return string.Concat("/",versionMappings[module.ToString()].Name,"/", versionMappings[module.ToString()].Version);
            }
            return string.Concat("/",versionMappings[module.ToString()].Name);
#endif
            }
            catch
            {
                return "";
            }
        }

    }

    #region Encode Password
    public static class GeneratePassword
    {
        public static string encode(string email)
        {
            char strChar;
            int bcode = 212;
            string endecode = string.Empty;

            while (email.Length > 0)
            {
                strChar = email[0];
                email = email.Substring(1, email.Length - 1);
                strChar = Strings.ChrW(bcode ^ Strings.AscW(strChar));

                endecode += strChar;
            }
            return endecode;
        }

        public static string GenerateRandomOTP(int iOTPLength, string[] saAllowedCharacters)
        {
            string sOTP = String.Empty;
            string sTempChars = String.Empty;
            Random rand = new Random();
            for (int i = 0; i < iOTPLength; i++)
            {
                int p = rand.Next(0, saAllowedCharacters.Length);
                sTempChars = saAllowedCharacters[rand.Next(0, saAllowedCharacters.Length)];
                sOTP += sTempChars;
            }
            return sOTP;
        }
    }
    
    #endregion
}

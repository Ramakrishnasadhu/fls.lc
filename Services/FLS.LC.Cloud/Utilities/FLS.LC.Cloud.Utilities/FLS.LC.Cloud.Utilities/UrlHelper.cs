﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Web;

namespace FLS.LC.Cloud.Utilities
{
    public enum ModeOperation
    {
        Modify,
        New,
        Remove
    }

    public class Link
    {
        /// <summary>
        /// Sub link Url
        /// </summary>
        [StringLength(maximumLength: int.MaxValue)]
        public string Href { get; set; }
        /// <summary>
        /// Reference
        /// </summary>
        [StringLength(maximumLength: int.MaxValue)]
        public string Rel { get; set; }
        /// <summary>
        /// Type of method. GET, POST
        /// </summary>
        [StringLength(maximumLength: int.MaxValue)]
        public string Method { get; set; }
    }

    public static class UrlHelper
    {
        public static string ModifyQueryString(this HttpRequest request, string url, Dictionary<string, string> keyValues, ModeOperation operation = ModeOperation.Modify)
        {
            var nameValues = HttpUtility.ParseQueryString(request.QueryString.ToString());
            foreach (var k in keyValues)
            {
                nameValues.Set(k.Key, k.Value);
            }
            return string.Format("{0}{1}?{2}", url, request.Path, nameValues);
        }
        public static string ModifyQueryStringByQueryHelper(this HttpRequest request, string url, Dictionary<string, string> keyValues, ModeOperation operation = ModeOperation.Modify)
        {
            var dictStringValues = QueryHelpers.ParseQuery(request.QueryString.ToString());

            foreach (var k in keyValues)
            {
                dictStringValues[k.Key] = k.Value;
            }

            var queryString = QueryString.Create(dictStringValues).ToString();//Url Encoding the whole thing; this always return with "?".

            return string.Format("{0}{1}{2}", url, request.Path, queryString);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace FLS.LC.Cloud.Utilities
{
    public static class GCSecurity
    {
        public enum AhsEncoder
        {
            HtmlEncode,
            UrlEncode,
            UrlPathEncode
        }

        //private static readonly string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        /// <summary>
        /// Generates randomId to maintain unique Id's in the application
        /// This will give fix for "Insufficient Entropy (CWE ID 331)"
        /// </summary>
        public static string AhsRandom(string pattern, int length, bool isPassword = false)
        {
            return GenerateDynamicCryptoString(pattern, length, isPassword);
        }
        public static long AhsRandomNumber(int length)
        {
            return GenerateDynamicCryptoNumber(length);
        }
        public static long GenerateDynamicCryptoNumber(int length)
        {
            if (length != 0)
            {
                RNGCryptoServiceProvider provider = CreateRNGCryptoServiceProvider();
                var byteArray = new byte[length > 3 ? length : 4];
                provider.GetBytes(byteArray);

                var randomNum = BitConverter.ToInt32(byteArray, 0).ToString();
                randomNum = randomNum.Length > length ? randomNum.Substring(0, length) : randomNum;
                return Convert.ToInt64(randomNum);
            }
            return 0;
        }
        public static string GenerateDynamicCryptoString(string pattern, int length, bool isPassword = false)
        {
            if (length != 0)
            {
                RNGCryptoServiceProvider provider = CreateRNGCryptoServiceProvider();
                StringBuilder sb;
                do
                {
                    sb = new StringBuilder();
                    var byteArray = new byte[length > 3 ? length : 4];
                    provider.GetBytes(byteArray);

                    int patternLength = pattern.Length;

                    //Gets character on index based from the pattern passed
                    for (var i = 0; i < byteArray.Length; i++)
                    {
                        byte x = byteArray[i];
                        while (x >= patternLength)
                            x = Convert.ToByte(x % patternLength);
                        sb.Append(pattern[x]);
                    }

                    //Below condition checks if atleast one special character is avaible in generated string.
                    isPassword = HasSpecialCharacters(sb, pattern, isPassword);
                } while (isPassword);

                return sb.ToString();
            }

            return string.Empty;
        }

        private static bool HasSpecialCharacters(StringBuilder sb, string pattern, bool isPassword)
        {
            //Below condition checks if atleast one special character is avaible in generated string.
            if (isPassword)
            {
                var regex = new Regex("[a-zA-Z0-9]*");
                var specialChars = regex.Replace(pattern, "");
                if (!string.IsNullOrEmpty(specialChars))
                {
                    var specialCharsRegex = new Regex("[" + specialChars + "]");
                    isPassword = !specialCharsRegex.IsMatch(sb.ToString());
                }
                else
                    isPassword = false;
            }
            return isPassword;
        }


        private static RNGCryptoServiceProvider CreateRNGCryptoServiceProvider()
        {
            RNGCryptoServiceProvider rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            return rngCryptoServiceProvider;
        }

        /// <summary>
        /// Encode the input text by using HttpUtility library if data does not bother about html characters
        /// Encode only script tag when data contains html content
        /// This will give fix for "IMPROPER NEUTRALIZATION OF SCRIPT-RELATED HTML TAGS IN A WEB PAGE (BASIC XSS) (CWE ID 80)"
        /// HtmlEncode : Encodes the specified string for use as text in HTML markup
        /// UrlEncode : Encodes the specified string for use in a URL
        /// UrlPathEncode : This method encodes only the path of a URL.If there is no scheme or authority in the string, the string is assumed to be a relative path, and the path is encoded.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="useHtmlEncode"></param>
        /// <param name="encoderType"></param>
        /// <returns></returns>

        public static string AhsEncode(string data, bool useHtmlEncode = true, AhsEncoder encoderType = AhsEncoder.HtmlEncode)
        {
            string outputString = data;

            if (outputString == null || outputString.Length == 0)
                return outputString;
            if (useHtmlEncode)
            {
                switch (encoderType)
                {
                    case AhsEncoder.HtmlEncode:
                        outputString = System.Web.HttpUtility.HtmlEncode(data);
                        break;
                    case AhsEncoder.UrlEncode:
                        outputString = System.Web.HttpUtility.UrlEncode(data);
                        break;
                    case AhsEncoder.UrlPathEncode:
                        outputString = System.Web.HttpUtility.UrlPathEncode(data);
                        break;
                }
            }
            return outputString;
        }

        /// <summary>
        /// This will give fix for  "IMPROPER NEUTRALIZATION OF SCRIPT-RELATED HTML TAGS IN A WEB PAGE (BASIC XSS) (CWE ID 80)"
        /// This  will resend the same stream data and this is for File Stream and Stream objects
        /// This is helpful when any stream object is writing to response.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static dynamic AhsEncodeStream(dynamic stream)
        {
            return stream;
        }

    }
}

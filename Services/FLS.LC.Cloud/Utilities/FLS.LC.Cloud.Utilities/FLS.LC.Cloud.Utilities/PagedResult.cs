﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace FLS.LC.Cloud.Utilities
{

    public class PagedResult<T> : EmbeddedModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public object ContextData { get; set; }
        public PagingInfo Page { get; set; }
        public IEnumerable<T> Results { get; set; }
        [JsonIgnore]
        public string SortBy { get; set; }
        public PagedResult()
        {

        }

        public PagedResult(object items, HttpRequest urlHelper, string url, int pageNo, int pageSize, int totalRecordCount, object contextData = null, bool applyQueryHelper = false, string sortBy = null)
        {
            Results = items as IEnumerable<T>;
            PagingInfo pagingInfo = new PagingInfo
            {
                PageNo = pageNo,
                PageSize = pageSize,
                PageCount = totalRecordCount > 0 ? (int)Math.Ceiling(totalRecordCount / (double)pageSize) : 0,
                TotalRecordCount = totalRecordCount
            };
            Page = pagingInfo;
            if (!sortBy.IsStringNullOrEmpty()) { SortBy = sortBy; }

            if (applyQueryHelper)
                Page.Links = GeneratePagingLinksByQueryHealpers(urlHelper, url);
            else
                Page.Links = GeneratePagingLinks(urlHelper, url);

            ContextData = contextData;
        }

        private Links GeneratePagingLinks(HttpRequest request, string url)
        {
            Dictionary<string, string> prev = new Dictionary<string, string>();
            prev.Add("pageSize", Convert.ToString(Page.PageSize));
            prev.Add("pageNo", Convert.ToString(Page.PageNo - 1));
            if (!SortBy.IsStringNullOrEmpty()) { prev.Add("sortBy", SortBy); }

            Dictionary<string, string> self = new Dictionary<string, string>();
            self.Add("pageSize", Convert.ToString(Page.PageSize));
            self.Add("pageNo", Convert.ToString(Page.PageNo));
            if (!SortBy.IsStringNullOrEmpty()) { self.Add("sortBy", SortBy); }

            Dictionary<string, string> next = new Dictionary<string, string>();
            next.Add("pageSize", Convert.ToString(Page.PageSize));
            next.Add("pageNo", Convert.ToString(Page.PageNo + 1));
            if (!SortBy.IsStringNullOrEmpty()) { next.Add("sortBy", SortBy); }

            return new Links()
            {
                Prev = new Prev()
                {
                    Href = (Page.PageNo <= 1) ? null : HttpUtility.UrlDecode(request.ModifyQueryString(url, prev))
                },
                Self = new Self()
                {
                    Href = HttpUtility.UrlDecode(request.ModifyQueryString(url, self)),
                },
                Next = new Next()
                {
                    Href = (Page.PageNo < Page.PageCount) ? HttpUtility.UrlDecode(request.ModifyQueryString(url, next)) : null
                }
            };
        }

        private Links GeneratePagingLinksByQueryHealpers(HttpRequest request, string url)
        {

            Dictionary<string, string> prev = new Dictionary<string, string>();
            prev.Add("pageSize", Convert.ToString(Page.PageSize));
            prev.Add("pageNo", Convert.ToString(Page.PageNo - 1));

            Dictionary<string, string> self = new Dictionary<string, string>();
            self.Add("pageSize", Convert.ToString(Page.PageSize));
            self.Add("pageNo", Convert.ToString(Page.PageNo));

            Dictionary<string, string> next = new Dictionary<string, string>();
            next.Add("pageSize", Convert.ToString(Page.PageSize));
            next.Add("pageNo", Convert.ToString(Page.PageNo + 1));

            return new Links()
            {
                Prev = new Prev()
                {
                    Href = (Page.PageNo <= 1) ? null : HttpUtility.UrlDecode(request.ModifyQueryStringByQueryHelper(url, prev))
                },
                Self = new Self()
                {
                    Href = HttpUtility.UrlDecode(request.ModifyQueryStringByQueryHelper(url, self)),
                },
                Next = new Next()
                {
                    Href = (Page.PageNo < Page.PageCount) ? HttpUtility.UrlDecode(request.ModifyQueryStringByQueryHelper(url, next)) : null
                }
            };
        }


        public class PagingInfo
        {
            public int PageSize { get; set; }
            public int PageNo { get; set; }
            public int PageCount { get; internal set; }
            public int TotalRecordCount { get; set; }
            public Links Links { get; set; }
        }

        public class Prev
        {
            public string Href { get; set; }
        }

        public class Self
        {
            public string Href { get; set; }
        }

        public class Next
        {
            public string Href { get; set; }
        }

        public class Links
        {
            public Prev Prev { get; set; }
            public Self Self { get; set; }
            public Next Next { get; set; }
        }

    }

    public sealed class InputPage
    {
        public int pageNo { get; set; }

        public int pageSize { get; set; }

    }

    public class ContextLocation
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
    public class EmbeddedModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ContextLocation location { get; set; }
    }
}

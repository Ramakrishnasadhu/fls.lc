﻿using System;
using System.Collections.Generic;
using System.Text;
using static FLS.LC.Cloud.Utilities.EnumHelper;

namespace FLS.LC.Cloud.Utilities
{
    public static class ClientTimeZone
    {
        private const string MST = "Mountain Standard Time";
        private const string PST = "Pacific Standard Time";
        private const string EST = "Eastern Standard Time";
        private const string HST = "Hawaiian Standard Time";
        private const string AKDT = "Alaskan Standard Time";         //UTC - 9             
        private const string CST = "Central Standard Time";         //UTC - 6
        private const string UMST = "US Mountain Standard Time";//(UTC-07:00) Arizona

        private const string IST = "India Standard Time";
        private const string CDT = "Central Daylight Time";         //UTC - 5
        private const string EDT = "Eastern Daylight Time";         //UTC - 4        
        private const string PSTM = "Pacific Standard Time (Mexico)";//UTC - 8 ,Baja California 
        private const string SPST = "SA Pacific Standard Time"; //(UTC-05:00) Bogota, Lima, Quito
        private const string PSST = "Pacific SA Standard Time";  //(UTC-04:00) Santiago
        private const string WPST = "West Pacific Standard Time";//UTC+10:00) Guam, Port Moresby
        private const string CPST = "Central Pacific Standard Time";//(UTC+11:00) Solomon Is., New Caledonia
        private const string PDT = "Pacific Daylight Time";         //UTC - 7
        private const string AST = "Atlantic Standard Time";        //UTC - 4  
        private const string SST = "Samoa Standard Time";           //UTC - 11
        private const string UTC = "UTC";           //UTC

        private static TimeZoneInfo _timeZoneInfo = null;

        private static System.DateTime ConvertDateTime(string timeZone, DateTime? universalTime = null)
        {
            _timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
            return TimeZoneInfo.ConvertTime(universalTime ?? DateTime.Now, _timeZoneInfo);
        }

        private static System.DateTime IndiaStandardTime
        {
            get
            {
                return ConvertDateTime(IST);
            }
        }

        private static System.DateTime PacificStandardTime
        {
            get
            {
                return ConvertDateTime(PST);
            }
        }

        private static System.DateTime EasternStandardTime
        {
            get
            {
                return ConvertDateTime(EST);
            }
        }

        private static System.DateTime HawaiianStandardTime
        {
            get
            {
                return ConvertDateTime(HST);
            }
        }

        private static System.DateTime MountainStandardTime
        {
            get
            {
                return ConvertDateTime(MST);
            }
        }

        private static System.DateTime CentralStandardTime
        {
            get
            {
                return ConvertDateTime(CST);
            }
        }

        private static System.DateTime AlaskanStandardTime
        {
            get
            {
                return ConvertDateTime(AKDT);
            }
        }

        private static System.DateTime USMountainStandardTime
        {
            get
            {
                return ConvertDateTime(UMST);
            }
        }


        public static System.DateTime GCStandardTime
        {
            get
            {

                string clientTimeZone = "";// AppKeyHelper.ClientTimeZone;
                DateTime destinationTimeZone = IndiaStandardTime;
                switch (clientTimeZone)
                {

                    case "MST":
                        destinationTimeZone = MountainStandardTime;//.isDayLightSaving() ? MountainStandardTime.AddHours(-1) : MountainStandardTime;
                        break;
                    case "PST":
                        destinationTimeZone = PacificStandardTime;//.isDayLightSaving() ? PacificStandardTime.AddHours(-1) : PacificStandardTime;
                        break;
                    case "EST":
                        destinationTimeZone = EasternStandardTime;//.isDayLightSaving() ? EasternStandardTime.AddHours(-1) : EasternStandardTime;
                        break;
                    case "HST":
                        destinationTimeZone = HawaiianStandardTime;//.isDayLightSaving() ? HawaiianStandardTime.AddHours(-1) : HawaiianStandardTime;
                        break;
                    case "CST":
                        destinationTimeZone = CentralStandardTime;//.isDayLightSaving() ? CentralStandardTime.AddHours(-1) : CentralStandardTime;
                        break;
                    case "AKDT":
                        destinationTimeZone = AlaskanStandardTime;//.isDayLightSaving() ? AlaskanStandardTime.AddHours(-1) : AlaskanStandardTime;
                        break;
                    case "UMST":
                        destinationTimeZone = USMountainStandardTime;//.isDayLightSaving() ? USMountainStandardTime.AddHours(-1) : USMountainStandardTime;
                        break;
                    case "IST":
                    default:
                        destinationTimeZone = IndiaStandardTime;
                        break;
                }


                return destinationTimeZone;
            }
        }


        private static string GetTimeZoneByName(string clientTimeZone)
        {
            var locTimeZone = "";
            switch (clientTimeZone)
            {

                case "MST": locTimeZone = MST; break;
                case "PST": locTimeZone = PST; break;
                case "EST": locTimeZone = EST; break;
                case "HST": locTimeZone = HST; break;
                case "IST": locTimeZone = IST; break;
                case "WPST": locTimeZone = WPST; break;
                case "AST": locTimeZone = AST; break;
                case "EDT": locTimeZone = EDT; break;
                case "CST": locTimeZone = CST; break;
                case "UMST": locTimeZone = UMST; break;
                case "CDT": locTimeZone = CDT; break;
                case "PSTM": locTimeZone = PSTM; break;
                case "SPST": locTimeZone = SPST; break;
                case "PSST": locTimeZone = PSST; break;
                case "CPST": locTimeZone = CPST; break;
                case "PDT": locTimeZone = PDT; break;
                case "AKDT": locTimeZone = AKDT; break;
                case "SST": locTimeZone = SST; break;
                case "UTC": locTimeZone = UTC; break;
                default: locTimeZone = EST; break;
            }

            return locTimeZone;
        }

        public static DateTime ToUserModifiedTime(DateTime providedTime, string sourceTimeZone, string clientTimeZone, string destinationTimeZone = "", TimeZoneType timeType = TimeZoneType.ToUser)
        {
            try
            {
                var sourceTimeZoneValue = GetTimeZoneByName(sourceTimeZone);
                var destinationTimeZoneValue = destinationTimeZone.IsStringNullOrEmpty() ? GetTimeZoneByName(clientTimeZone) : destinationTimeZone;

                switch (timeType)
                {
                    case TimeZoneType.ToUser:
                        destinationTimeZoneValue = GetTimeZoneByName(sourceTimeZone);
                        sourceTimeZoneValue = destinationTimeZone.IsStringNullOrEmpty() ? GetTimeZoneByName(clientTimeZone) : GetTimeZoneByName(destinationTimeZone);
                        break;
                    case TimeZoneType.ToSystem:
                        sourceTimeZoneValue = GetTimeZoneByName(sourceTimeZone);
                        destinationTimeZoneValue = destinationTimeZone.IsStringNullOrEmpty() ? GetTimeZoneByName(clientTimeZone) : GetTimeZoneByName(destinationTimeZone);
                        break;
                    case TimeZoneType.AsIS:

                        sourceTimeZoneValue = GetTimeZoneByName(clientTimeZone);
                        destinationTimeZoneValue = sourceTimeZoneValue;
                        break;
                    case TimeZoneType.None:
                        break;
                    default:
                        break;
                }

                var sourceTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(sourceTimeZoneValue);
                var destinationTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(destinationTimeZoneValue);

                DateTime now = DateTime.SpecifyKind(providedTime, DateTimeKind.Unspecified);

                var utcToNow = TimeZoneInfo.ConvertTime(now, sourceTimeZoneInfo, destinationTimeZoneInfo);

                //if (utcToNow.IsDaylightSavingTime())
                //    return utcToNow.AddHours(-1);
                //else if (utcToNow.isDayLightSaving())
                //    return utcToNow.AddHours(-1);
                //else
                //return utcToNow.isDayLightSaving() ? utcToNow.AddHours(-1) : utcToNow;
                return utcToNow;
            }
            catch (Exception)
            {

            }

            return providedTime;
        }

        public static Dictionary<TimeZoneType, string> TimeDictionary { get; set; }

        public class TimeZoneFormat : Attribute
        {
            public TimeZoneType Description { get; set; }
        }

        public static DateTime ToUserTimeZone(DateTime userDateTime, string TimeZoneFormat)
        {

            string locTimeZone = "";

            switch (TimeZoneFormat)
            {
                case "MST": locTimeZone = MST; break;
                case "PST": locTimeZone = PST; break;
                case "EST": locTimeZone = EST; break;
                case "HST": locTimeZone = HST; break;
                case "IST": locTimeZone = IST; break;
                case "WPST": locTimeZone = WPST; break;
                case "AST": locTimeZone = AST; break;
                case "EDT": locTimeZone = EDT; break;
                case "CST": locTimeZone = CST; break;
                case "CDT": locTimeZone = CDT; break;
                case "PSTM": locTimeZone = PSTM; break;
                case "SPST": locTimeZone = SPST; break;
                case "PSST": locTimeZone = PSST; break;
                case "CPST": locTimeZone = CPST; break;
                case "PDT": locTimeZone = PDT; break;
                case "AKDT": locTimeZone = AKDT; break;
                case "UMST": locTimeZone = UMST; break;
                case "SST": locTimeZone = SST; break;

                default:
                    break;
            }


            return ConvertDateTime(locTimeZone, userDateTime);
        }
        public static string GetTimeZoneDifference(string TimeZoneVal)
        {
            var sourceTimeZone = GetTimeZoneByName("");
            var destinationTimeZone = GetTimeZoneByName(TimeZoneVal);
            var source = TimeZoneInfo.FindSystemTimeZoneById(sourceTimeZone);
            var destination = TimeZoneInfo.FindSystemTimeZoneById(destinationTimeZone);
            TimeSpan sourceOffset = source.GetUtcOffset(DateTime.Now);
            TimeSpan destinationOffset = destination.GetUtcOffset(DateTime.Now);
            TimeSpan difference = destinationOffset - sourceOffset;
            double time = (difference.TotalMinutes) / 60;
            return time.ToString();
        }
        public static string GetTimeZoneDifferenceGMT(string TimeZoneVal)
        {
            var destinationTimeZone = GetTimeZoneByName(TimeZoneVal);
            var destination = TimeZoneInfo.FindSystemTimeZoneById(destinationTimeZone);
            TimeSpan destinationOffset = destination.GetUtcOffset(DateTime.Now);
            TimeSpan difference = destinationOffset;
            double time = difference.TotalMinutes;
            return time.ToString();
        }
        public static DateTime ToUTC(this DateTime dateTime, string fromTimeZone)
        {
            if (dateTime.Kind == DateTimeKind.Utc)
                return dateTime;
            else if (dateTime.Kind == DateTimeKind.Local)
                return dateTime.ToUniversalTime();

            string zoneName = GetTimeZoneByName(fromTimeZone);
            TimeZoneInfo zone = TimeZoneInfo.FindSystemTimeZoneById(zoneName);
            dateTime = DateTimeWithZoneByInfoToUTC(dateTime, zone);
            return dateTime;
        }

        public static DateTime? ToUTC(this DateTime? dateTime, string fromTimeZone)
        {
            if (!dateTime.HasValue) return null;

            dateTime = ToUTC(dateTime.Value, fromTimeZone);
            return dateTime;
        }
        public static DateTime? ToUTC(this string dateTime, string fromTimeZone)
        {
            if (dateTime.IsStringNullOrEmpty() || dateTime.Equals("N/A", StringComparison.OrdinalIgnoreCase)) return null;
            DateTime a;
            bool isDateValid = DateTime.TryParse(dateTime, out a);
            return isDateValid ? a.ToUTC(fromTimeZone) : (DateTime?)null;
        }
        private static DateTime DateTimeWithZoneByInfoToUTC(DateTime dateTime, TimeZoneInfo timeZone)
        {
            var dateTimeUnspec = DateTime.SpecifyKind(dateTime, DateTimeKind.Unspecified);
            var utcDateTime = TimeZoneInfo.ConvertTimeToUtc(dateTimeUnspec, timeZone);
            return utcDateTime;
        }

        public static DateTime ToSystemTimeFromUTC(this System.DateTime clientDateTime, string clientTimeZone, TimeZoneType UsertimeType = TimeZoneType.ToSystem)
        {
            var timeZone = ClientTimeZone.UTC;
            return ClientTimeZone.ToUserModifiedTime(clientDateTime, sourceTimeZone: timeZone, clientTimeZone: clientTimeZone, timeType: UsertimeType);
        }

        public static DateTime? ToSystemTimeFromUTC(this System.DateTime? clientDateTime, string clientTimeZone, TimeZoneType UsertimeType = TimeZoneType.ToSystem)
        {
            if (!clientDateTime.HasValue)
                return null;
            return clientDateTime.Value.ToSystemTimeFromUTC(clientTimeZone, UsertimeType);
        }
    }
}

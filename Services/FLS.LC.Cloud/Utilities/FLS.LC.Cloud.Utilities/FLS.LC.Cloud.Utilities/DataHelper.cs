﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace FLS.LC.Cloud.Utilities
{
    public static class DataHelper
    {

        public static void AddParams(string parameterName, object value, ref StringBuilder sqlQuery, ref List<SqlParameter> parameters)
        {
            if (sqlQuery == null || parameters == null || string.IsNullOrWhiteSpace(parameterName))
                return;
            parameters.Add(PrepareParameter(parameterName, value));

            if (parameters.Count > 1)
                sqlQuery.Append(string.Concat(",@", parameterName, "=@", parameterName));
            else
                sqlQuery.Append(string.Concat("@", parameterName, "=@", parameterName));
        }

        public static void AddParams(string parameterName, object value, StringBuilder sqlQuery, List<SqlParameter> parameters)
        {
            if (sqlQuery == null || parameters == null || string.IsNullOrWhiteSpace(parameterName))
                return;
            parameters.Add(PrepareParameter(parameterName, value));

            if (parameters.Count > 1)
                sqlQuery.Append(string.Concat(",@", parameterName, "=@", parameterName));
            else
                sqlQuery.Append(string.Concat("@", parameterName, "=@", parameterName));
        }

        private static SqlParameter PrepareParameter(string parameterName, object value)
        {
            return new SqlParameter { ParameterName = string.Concat("@", parameterName), Value = value };
        }


        /// <summary>
        /// Which converts model property into sql input parameter.
        /// </summary>
        /// <param name="field"></param>
        /// <param name="optionalColumn"></param>
        /// <returns></returns>
        public static string ToParameterInput(this string field, string optionalColumn = "")
        {
            try
            {
                if (!field.IsStringNullOrEmpty())
                {
                    if (optionalColumn.IsStringNullOrEmpty())
                    {
                        return string.Concat(field.Select(a => Char.IsUpper(a) ? "_" + a : a.ToString())).ToUpper().Substring(1);
                    }
                    else
                    {
                        return optionalColumn;
                    }
                }
                else if (!optionalColumn.IsStringNullOrEmpty())
                    return field = optionalColumn;
                else
                    return field;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  //Declaring a generic method which will actually return property Name and its value for any given class.
        /// </summary>
        /// <param name="classObj"></param>
        /// <returns></returns>
        public static void ToParameters(this object classObj, ref StringBuilder sqlQuery, ref List<SqlParameter> parameters)
        {
            foreach (var prop in classObj.GetType().GetProperties())
            {
                string propName = prop.Name.ToParameterInput();
                var propValue = prop.GetValue(classObj, null);
                string propertyValue = Convert.ToString(propValue);
                if (!propertyValue.Equals(Convert.ToString(default(DateTime)), StringComparison.OrdinalIgnoreCase))
                {
                    if (propertyValue != "")
                    {
                        AddParams(propName, propValue, ref sqlQuery, ref parameters);
                    }
                }

            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;

namespace FLS.LC.Cloud.Utilities
{
    public class EnumHelper
    {
        public enum AuthStatusCode
        {
            Close = 1,
            Open = 2,
            Cancelled = 3,
            ReOpen = 4,
            ReferralOnly = 5,
            ReOpenandClose = 6,
            ClosedandAdjusted = 7,
            ClosedandCancelled = 8,
            ClosedReportingonly = 9
        }
        public enum TimeZoneType
        {
            ToUser = 1,
            ToSystem = 2,
            AsIS = 3,
            None = 4

        }
        public enum CommunicationType
        {
            Encode = 1,
            Decode = 0
        }

        public enum GcModule
        {
            CM = 1,
            UM = 3,
            AG = 5,
            MS = 7,
            MTM = 9,
            PP = 37
        }
        public enum LetterSaveType
        {
            None = 0,
            PrintQueue = 1,
            SavePrintQueue = 2,
            SaveAsDraft = 3,
            CompleteReview = 4,
            SaveConsentForm = 5,
            SaveCaseConference = 6
        }
        public enum Component
        {
            DevExpress = 1,
            SmartComm = 2
        }
        public enum LogType
        {
            Request = 1,
            Response = 2
        }
        public enum GuideLineSource
        {
            MCG = 1,
            INTERQUAL = 2
        }

        public enum ConfidentialTypeData
        {
            [Description("All Users")]
            AllUsers = 1,
            [Description("Internal Users Only")]
            InternalUsersOnly = 2
        }

        public enum UmNoteFrom
        {
            [Description("Auth Notes")]
            AuthNotes = 1,
            [Description("Member Notes")]
            MemberNotes = 2,
            [Description("Provider Notes")]
            ProviderNotes = 3,
            [Description("Physician Notes")]
            PhysicianNotes = 4,
            [Description("Transfer Note")]
            TransferNote = 6,
            [Description("Activity")]
            Activity = 7,
            [Description("Decision Notes")]
            DecisionNotes = 8
        }

        public enum UserType
        {
            [Description("API_USER")]
            ApiUser = 1
        }

        public static string GetDescription(Enum EnumCode)
        {
            try
            {
                FieldInfo fi = EnumCode.GetType().GetField(EnumCode.ToString());
                DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes.Length > 0)
                {
                    return attributes[0].Description;
                }
                else
                {
                    return EnumCode.ToString();
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public enum ActivityType
        {
            Scheduled = 1,
            Completed = 2,
            CaseConference = 3,
            MemberServices = 4
        }
        public enum HealthNoteType
        {
            [Description("Activity")]
            Activity = 1,
            [Description("Referral")]
            Referral = 2,
            [Description("Activity Outcome")]
            ActivityOutcome = 3
        }
    }

    public enum ProviderEnum
    {
        Admitting = 1,
        ReferredBy = 2,
        ReferredTo = 3,
        Facility = 4
    }

    public enum DocumentIOStoreType
    {
        Mongo,
        AzureBlob,
        AwsBlob
    }

    public enum TelehealthIOMeetingType
    {
        Teams,
        Webex,
        Zoom
    }
    public enum DocumentIOType
    {
        Authorization
    }
    public enum MetaDataParameters
    {
        AuthorizationId,
        AuthorizationNo,
        ClientPatientId,
        ComplaintNo,
        ComplaintId,
        PatientId
    }
    public enum FileExtensionType
    {
        None,
        Txt,
        Pdf
    }
    public enum DecisionStatusCode
    {
        Pending = 1,
        Denied = 2,
        Approved = 3,
        Adjusted = 4,
        [Description("Appeal Overturned")]
        AppealOverturned = 5,
        [Description("Appeal Upheld")]
        AppealUpheld = 6,
        [Description("Appeal Partially Approved")]
        AppealPartiallyApproved = 7,
        Void = 8,
        Modified = 9,
        [Description("Partially Approved")]
        PartiallyApproved = 10,
        [Description("Invalid Request")]
        InvalidRequest = 11,
        COB = 12,
        [Description("End of Service")]
        EndofService = 13,
        [Description("Appeal & Fair Hearing")]
        AppealAndFairHearing = 14,
        Accepted = 15,
        Rejected = 16,
        [Description("Partial Approval")]
        PartialApproval = 17
    }
    public enum ProviderType
    {
        Admitting = 1,
        [Description("Referred By")]
        ReferredBy = 2,
        [Description("Referred To")]
        ReferredTo = 3,
        Facility = 4,
        Service = 5
    }

    public enum AuthCodeType
    {
        [Description("Procedure Code")]
        ProcedureCode = 1,
        [Description("Admitting or Principle Diagnosis Code")]
        AdmittingDiagnosisCode = 2,
        [Description("Discharge Diagnosis Code")]
        DischargeDiagnosisCode = 3,
        [Description("Medication Code")]
        MedicationCode = 4,
        [Description("Service Category Code")]
        ServiceCategoryCode = 5
    }

    public enum AccordianCode
    {
        ProviderDetails = 22,
        AuthBasicDetails = 23,
        DiagnosisAndProcedureCodes = 24,
        FinancialDetails = 25,
        AdditionalDetails = 26
    }

    public enum UmProviderType
    {
        [Description("External Source Submitting")]
        ExternalSourceSubmitting = 0,
        Admitting = 1,
        [Description("Referred By")]
        ReferredBy = 2,
        [Description("Referred To")]
        ReferredTo = 3,
        Facility = 4,
    }
}

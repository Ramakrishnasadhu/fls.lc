﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json.Linq;
using static FLS.LC.Cloud.Utilities.EnumHelper;

namespace FLS.LC.Cloud.Utilities
{
    public static class Extension
    {
        public static bool IsStringNullOrEmpty(this string stringObj)
        {
            return string.IsNullOrEmpty(stringObj);
        }
        public static bool IsStringTrimNullOrEmpty(this string stringObj)
        {
            return string.IsNullOrEmpty(stringObj) || string.IsNullOrEmpty(stringObj.Trim());
        }
        public static string TrimText(this string Text)
        {
            return Text.IsStringNullOrEmpty() ? "" : Text.Trim().TrimStart().TrimEnd();
        }

        /// <summary>
        /// To Trim and Convert the string into UpperCase
        /// </summary>
        /// <param name="ObjString"></param>
        /// <returns></returns>
        public static string TrimNUpper(this string ObjString)
        {
            try
            {
                if (!string.IsNullOrEmpty(ObjString))
                {
                    return ObjString.Trim().ToUpper();
                }
                return ObjString;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public static string ToString(this object str, object IsEmptyOrNull = null)
        {
            if (IsEmptyOrNull != null && str == null)
            {
                return Convert.ToString(IsEmptyOrNull);
            }
            return Convert.ToString(str);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortfield">Sort field</param>
        /// <param name="defaultSortColumn">Default sort column, which is assigned if sortField is Empty.</param>
        /// <returns></returns>
        /// /// <example>
        /// <code>
        /// sortField = sortField.ToKendoSortField("DEFAULT_COLUMN_VALUE");
        /// </code>
        /// </example>
        public static string ToKendoSortField(this string sortfield, string defaultSortColumn = "")
        {
            try
            {
                if (!sortfield.IsStringNullOrEmpty())
                {
                    var returnSortField = string.Concat(sortfield.Select(a => Char.IsUpper(a) ? "_" + a : a.ToString())).ToUpper();
                    returnSortField = returnSortField.Trim(new Char[] { '_' });
                    return returnSortField;
                }
                else if (!defaultSortColumn.IsStringNullOrEmpty())
                {
                    return sortfield = defaultSortColumn;
                }
                else
                    return sortfield;
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return sortfield;
        }
        /// <summary>
        /// Use for Search Functionality to search for wild char's.
        /// </summary>
        /// <param name="SearchText">The search text.</param>
        /// <param name="IsExactSearch">In exact search case single quote need to be handled while passing to data base</param>
        /// <returns>System.String.</returns>
        public static string ExplicitUseforSearch(this string SearchText, bool IsExactSearch = false)
        {
            try
            {
                StringBuilder sb = new StringBuilder(SearchText);
                sb = sb.Replace("[", "[[]");
                sb = sb.Replace("]", "[]]");
                sb = sb.Replace("_", "[_]");
                sb = sb.Replace("%", "[%]");
                sb = IsExactSearch ? sb.Replace("'", "['']") : sb.Replace("'", "['''']");
                sb = sb.Replace("[[[]]", "[[]");
                sb = sb.Replace("[[]]]", "[]]");
                sb = sb.Replace("[[][]]", "[[]]");
                sb = sb.Replace("[]]", "]");
                return sb.ToString().Trim();
            }
            catch (Exception)
            {
                return SearchText;
            }

        }

        /// <summary>
        /// To the DDMMYYYY format.
        /// </summary>
        /// <param name="time">The time.</param>
        /// <returns>System.String.</returns>
        public static string ToMMDDYYYYFormat(this string time)
        {
            try
            {
                if (!string.IsNullOrEmpty(time))
                {
                    return string.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(time));
                }
            }
            catch (Exception)
            {
                return "";
            }
            return "";
        }

        /// <summary>
        /// To the DDMMYYYY format.
        /// </summary>
        /// <param name="time">The time.</param>
        /// <returns>System.String.</returns>
        /// <SUMMARY>
        /// GET THE CLIENT DATE TIME INSTEAD OF AH.DATETIME.CLIENTNOW ();
        ///   </SUMMARY>
        ///   <PARAM NAME="TIME">THE TIME.</PARAM>
        ///   <RETURNS></RETURNS>
        public static string ToDDMMYYYYFormat(this DateTime? time)
        {
            try
            {
                if (time != null)
                {
                    return string.Format("{0:MM/dd/yyyy}", time);
                }

            }
            catch (Exception)
            {
                return "";
            }
            return "";
        }


        public static string ToDDMMYYYYFormat(this DateTime time)
        {
            try
            {
                if (time != null)
                {
                    return string.Format("{0:MM/dd/yyyy}", time);
                }

            }
            catch (Exception)
            {
                return "";
            }
            return "";
        }


        /// <summary>
        /// To the DDMMYYYY format
        /// </summary>
        /// <param name="time">The time.</param>
        /// <returns>System.String.</returns>
        public static string ToSQLDate(this string time)
        {
            try
            {
                if (!string.IsNullOrEmpty(time))
                {
                    return string.Format("{0:yyyyMMdd}", Convert.ToDateTime(time));
                }
            }
            catch (Exception)
            {
                return null;
            }
            return null;
        }

        /// <summary>
        /// To the yyyyMMDD format
        /// </summary>
        /// <param name="time">The time.</param>
        /// <returns>System.String.</returns>
        public static string ToSQLDate(this DateTime? date)
        {
            try
            {
                if (!date.IsNull())
                {
                    return string.Format("{0:yyyyMMdd}", date.Value);
                }
            }
            catch (Exception)
            {
                return null;
            }
            return null;
        }

        /// <summary>
        /// Checks if input has valid datetime
        /// </summary>
        /// <param name="ObjString"></param>
        /// <returns></returns>
        public static bool HasDate(this DateTime ObjString)
        {
            try
            {
                if (!ObjString.ToShortDateString().Equals(default(DateTime).ToShortDateString()) && ObjString.ToShortDateString() != "12/31/0001")
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {

            }
            return false;
        }
        /// <summary>
        /// Clients the time.
        /// </summary>
        /// <param name="clientDateTime">The client date time.</param>
        /// <returns>System.DateTime.</returns>
        /// <SUMMARY>
        /// CONVERTS ANY DATE TIME INFO TO RESPECTIVE CLIENT TIME ZONE.
        ///   </SUMMARY>
        ///   <PARAM NAME="CLIENTDATETIME">THE CLIENT DATE TIME.</PARAM>
        ///   <RETURNS></RETURNS>
        public static System.DateTime ClientTime(this System.DateTime clientDateTime)
        {
            return ClientTimeZone.GCStandardTime;
        }
        public static bool IsNull(this object ObjectNullable)
        {
            return (ObjectNullable == null);
        }
        public static bool IsListNullOrEmpty<T>(this IEnumerable<T> listObj)
        {
            if (listObj == null) { return true; }
            return !listObj.Any();
        }
        public static T CustomDeserializeObject<T>(string data, bool useNewtonSoft = true)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }
        public static string ToStringCommunication(this string LetterContent, CommunicationType encodeType)
        {
            try
            {
                if (!LetterContent.IsStringNullOrEmpty())
                    return encodeType == CommunicationType.Encode ? HttpUtility.HtmlEncode(LetterContent) : HttpUtility.HtmlDecode(LetterContent);
                else
                    return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Returns indented Json Result 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToJson<T>(this T obj)
        {
            try
            {
                return JsonConvert.SerializeObject(obj, Formatting.Indented);
            }
            catch
            {
                return string.Empty;
            }
        }

        public static bool IsValidJson(this string src)
        {
            try
            {
                var validJson = JObject.Parse(src);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static string ToJsonIgnoreNull<T>(this T obj)
        {
            try
            {
                return JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
            }
            catch
            {
                return string.Empty;
            }
        }

        public static Dictionary<TimeZoneType, string> DeserializeTimeZoneObject(string TimeDictionaryobj)
        {
            try
            {
                if (!string.IsNullOrEmpty(TimeDictionaryobj) && TimeDictionaryobj.Length > 0)
                {
                    var timeZoneDecodeString = Extension.CustomDeserializeObject<string>(TimeDictionaryobj);
                    timeZoneDecodeString = timeZoneDecodeString.ToStringCommunication(CommunicationType.Decode);
                    var timeZoneDictObj = JsonConvert.DeserializeObject<Dictionary<TimeZoneType, string>>(timeZoneDecodeString);
                    if (timeZoneDictObj != null)
                    {
                        return timeZoneDictObj;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return new Dictionary<TimeZoneType, string>();
        }

        public static string GetChar(int CI)
        {
            return char.ConvertFromUtf32(CI);
        }

        /// <summary>
        /// To the bool.
        /// </summary>
        /// <param name="ObjString">The obj string.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
        public static bool ToBool(this string ObjString)
        {
            try
            {
                if (ObjString.IsStringNullOrEmpty()) { return false; }
                if (ObjString.Trim().ToLower() == "true" || ObjString.Trim().ToLower() == "1")
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool ToBool(this int ObjInt)
        {
            try
            {
                if (ObjInt > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static int ToInt32(this object value)
        {
            try
            {
                if (value != null && !Convert.ToString(value).IsStringNullOrEmpty())
                {
                    return ((IConvertible)value).ToInt32(null);
                }
            }
            catch (Exception)
            {
                return (int)0;
            }
            return (int)0;
        }

        /// <summary>
        /// Replace illegal characters with some special characters
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ReplaceIllegalChars(this string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : value.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;");
        }

        public static string RandomID()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var result = GCSecurity.AhsRandom(chars, 8);
            return result;
        }


        public static string StatusToString(this bool status)
        {
            return status ? "Active" : "InActive";
        }

        public static string StatusToString(this bool? status)
        {
            if (status.IsNull())
            {
                return "InActive";
            }
            return status.Value.StatusToString();
        }

        public static string FullName(this string firstName, string middleName, string lastName)
        {
            firstName = firstName.TrimText();
            middleName = middleName.TrimText();
            lastName = lastName.TrimText();

            if (middleName.IsStringNullOrEmpty())
            {
                return string.Concat(firstName, " ", lastName).TrimText();
            }
            return string.Concat(firstName, " ", middleName, " ", lastName).TrimText();
        }

        public static string ToGenderFullForm(this string genderAbbreviation)
        {
            string genderFullForm = null;
            switch (genderAbbreviation)
            {
                case "M":
                case "m":
                    genderFullForm = "Male";
                    break;
                case "F":
                case "f":
                    genderFullForm = "Female";
                    break;
                case "U":
                case "u":
                    genderFullForm = "Unspecified";
                    break;
            }
            return genderFullForm;
        }


        /// <summary>
        /// Will transform the Phone Number to ###-###-####
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public static string ToPhoneNumber(this string phoneNumber)
        {
            string regexPattern = @"^\d{10}$";
            return Regex.IsMatch(phoneNumber, regexPattern) ? string.Format("{0:###-###-####}", Convert.ToInt64(phoneNumber)) : phoneNumber;
        }
        public static int ToProperPageSize(this int pageSize)
        {
            return (pageSize < 2 || pageSize > 100) ? 20 : pageSize;
        }

        public static int ToProperPageSize(this int? pageSize)
        {
            if (pageSize.IsNull())
            {
                return 20;
            }
            return pageSize.Value.ToProperPageSize();
        }
        public static int ToProperPageNo(this int pageNo)
        {
            return pageNo < 1 ? 1 : pageNo;
        }
        public static int ToProperPageNo(this int? pageNo)
        {
            if (pageNo.IsNull())
            {
                return 1;
            }
            return pageNo.Value.ToProperPageNo();
        }
        #region RegexUtilities


        public static class MIMEAssistant
        {
            private static readonly Dictionary<string, string> MIMETypesDictionary = new Dictionary<string, string>
  {
    {"ai", "application/postscript"},
    {"aif", "audio/x-aiff"},
    {"aifc", "audio/x-aiff"},
    {"aiff", "audio/x-aiff"},
    {"asc", "text/plain"},
    {"atom", "application/atom+xml"},
    {"au", "audio/basic"},
    {"avi", "video/x-msvideo"},
    {"bcpio", "application/x-bcpio"},
    {"bin", "application/octet-stream"},
    {"bmp", "image/bmp"},
    {"cdf", "application/x-netcdf"},
    {"cgm", "image/cgm"},
    {"class", "application/octet-stream"},
    {"cpio", "application/x-cpio"},
    {"cpt", "application/mac-compactpro"},
    {"csh", "application/x-csh"},
    {"css", "text/css"},
    {"dcr", "application/x-director"},
    {"dif", "video/x-dv"},
    {"dir", "application/x-director"},
    {"djv", "image/vnd.djvu"},
    {"djvu", "image/vnd.djvu"},
    {"dll", "application/octet-stream"},
    {"dmg", "application/octet-stream"},
    {"dms", "application/octet-stream"},
    {"doc", "application/msword"},
    {"docx","application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
    {"dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template"},
    {"docm","application/vnd.ms-word.document.macroEnabled.12"},
    {"dotm","application/vnd.ms-word.template.macroEnabled.12"},
    {"dtd", "application/xml-dtd"},
    {"dv", "video/x-dv"},
    {"dvi", "application/x-dvi"},
    {"dxr", "application/x-director"},
    {"eps", "application/postscript"},
    {"etx", "text/x-setext"},
    {"exe", "application/octet-stream"},
    {"ez", "application/andrew-inset"},
    {"gif", "image/gif"},
    {"gram", "application/srgs"},
    {"grxml", "application/srgs+xml"},
    {"gtar", "application/x-gtar"},
    {"hdf", "application/x-hdf"},
    {"hqx", "application/mac-binhex40"},
    {"htm", "text/html"},
    {"html", "text/html"},
    {"ice", "x-conference/x-cooltalk"},
    {"ico", "image/x-icon"},
    {"ics", "text/calendar"},
    {"ief", "image/ief"},
    {"ifb", "text/calendar"},
    {"iges", "model/iges"},
    {"igs", "model/iges"},
    {"jnlp", "application/x-java-jnlp-file"},
    {"jp2", "image/jp2"},
    {"jpe", "image/jpeg"},
    {"jpeg", "image/jpeg"},
    {"jpg", "image/jpeg"},
    {"js", "application/x-javascript"},
    {"kar", "audio/midi"},
    {"latex", "application/x-latex"},
    {"lha", "application/octet-stream"},
    {"lzh", "application/octet-stream"},
    {"m3u", "audio/x-mpegurl"},
    {"m4a", "audio/mp4a-latm"},
    {"m4b", "audio/mp4a-latm"},
    {"m4p", "audio/mp4a-latm"},
    {"m4u", "video/vnd.mpegurl"},
    {"m4v", "video/x-m4v"},
    {"mac", "image/x-macpaint"},
    {"man", "application/x-troff-man"},
    {"mathml", "application/mathml+xml"},
    {"me", "application/x-troff-me"},
    {"mesh", "model/mesh"},
    {"mid", "audio/midi"},
    {"midi", "audio/midi"},
    {"mif", "application/vnd.mif"},
    {"mov", "video/quicktime"},
    {"movie", "video/x-sgi-movie"},
    {"mp2", "audio/mpeg"},
    {"mp3", "audio/mpeg"},
    {"mp4", "video/mp4"},
    {"mpe", "video/mpeg"},
    {"mpeg", "video/mpeg"},
    {"mpg", "video/mpeg"},
    {"mpga", "audio/mpeg"},
    {"ms", "application/x-troff-ms"},
    {"msh", "model/mesh"},
    {"mxu", "video/vnd.mpegurl"},
    {"nc", "application/x-netcdf"},
    {"oda", "application/oda"},
    {"ogg", "application/ogg"},
    {"pbm", "image/x-portable-bitmap"},
    {"pct", "image/pict"},
    {"pdb", "chemical/x-pdb"},
    {"pdf", "application/pdf"},
    {"pgm", "image/x-portable-graymap"},
    {"pgn", "application/x-chess-pgn"},
    {"pic", "image/pict"},
    {"pict", "image/pict"},
    {"png", "image/png"},
    {"pnm", "image/x-portable-anymap"},
    {"pnt", "image/x-macpaint"},
    {"pntg", "image/x-macpaint"},
    {"ppm", "image/x-portable-pixmap"},
    {"ppt", "application/vnd.ms-powerpoint"},
    {"pptx","application/vnd.openxmlformats-officedocument.presentationml.presentation"},
    {"potx","application/vnd.openxmlformats-officedocument.presentationml.template"},
    {"ppsx","application/vnd.openxmlformats-officedocument.presentationml.slideshow"},
    {"ppam","application/vnd.ms-powerpoint.addin.macroEnabled.12"},
    {"pptm","application/vnd.ms-powerpoint.presentation.macroEnabled.12"},
    {"potm","application/vnd.ms-powerpoint.template.macroEnabled.12"},
    {"ppsm","application/vnd.ms-powerpoint.slideshow.macroEnabled.12"},
    {"ps", "application/postscript"},
    {"qt", "video/quicktime"},
    {"qti", "image/x-quicktime"},
    {"qtif", "image/x-quicktime"},
    {"ra", "audio/x-pn-realaudio"},
    {"ram", "audio/x-pn-realaudio"},
    {"ras", "image/x-cmu-raster"},
    {"rdf", "application/rdf+xml"},
    {"rgb", "image/x-rgb"},
    {"rm", "application/vnd.rn-realmedia"},
    {"roff", "application/x-troff"},
    {"rtf", "text/rtf"},
    {"rtx", "text/richtext"},
    {"sgm", "text/sgml"},
    {"sgml", "text/sgml"},
    {"sh", "application/x-sh"},
    {"shar", "application/x-shar"},
    {"silo", "model/mesh"},
    {"sit", "application/x-stuffit"},
    {"skd", "application/x-koan"},
    {"skm", "application/x-koan"},
    {"skp", "application/x-koan"},
    {"skt", "application/x-koan"},
    {"smi", "application/smil"},
    {"smil", "application/smil"},
    {"snd", "audio/basic"},
    {"so", "application/octet-stream"},
    {"spl", "application/x-futuresplash"},
    {"src", "application/x-wais-source"},
    {"sv4cpio", "application/x-sv4cpio"},
    {"sv4crc", "application/x-sv4crc"},
    {"svg", "image/svg+xml"},
    {"swf", "application/x-shockwave-flash"},
    {"t", "application/x-troff"},
    {"tar", "application/x-tar"},
    {"tcl", "application/x-tcl"},
    {"tex", "application/x-tex"},
    {"texi", "application/x-texinfo"},
    {"texinfo", "application/x-texinfo"},
    {"tif", "image/tiff"},
    {"tiff", "image/tiff"},
    {"tr", "application/x-troff"},
    {"tsv", "text/tab-separated-values"},
    {"txt", "text/plain"},
    {"ustar", "application/x-ustar"},
    {"vcd", "application/x-cdlink"},
    {"vrml", "model/vrml"},
    {"vxml", "application/voicexml+xml"},
    {"wav", "audio/x-wav"},
    {"wbmp", "image/vnd.wap.wbmp"},
    {"wbmxl", "application/vnd.wap.wbxml"},
    {"wml", "text/vnd.wap.wml"},
    {"wmlc", "application/vnd.wap.wmlc"},
    {"wmls", "text/vnd.wap.wmlscript"},
    {"wmlsc", "application/vnd.wap.wmlscriptc"},
    {"wrl", "model/vrml"},
    {"xbm", "image/x-xbitmap"},
    {"xht", "application/xhtml+xml"},
    {"xhtml", "application/xhtml+xml"},
    {"xls", "application/vnd.ms-excel"},
    {"xml", "application/xml"},
    {"xpm", "image/x-xpixmap"},
    {"xsl", "application/xml"},
    {"xlsx","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
    {"xltx","application/vnd.openxmlformats-officedocument.spreadsheetml.template"},
    {"xlsm","application/vnd.ms-excel.sheet.macroEnabled.12"},
    {"xltm","application/vnd.ms-excel.template.macroEnabled.12"},
    {"xlam","application/vnd.ms-excel.addin.macroEnabled.12"},
    {"xlsb","application/vnd.ms-excel.sheet.binary.macroEnabled.12"},
    {"xslt", "application/xslt+xml"},
    {"xul", "application/vnd.mozilla.xul+xml"},
    {"xwd", "image/x-xwindowdump"},
    {"xyz", "chemical/x-xyz"},
    {"zip", "application/zip"}
  };

            public static string GetMIMEType(string fileName)
            {
                //get file extension
                string extension = Path.GetExtension(fileName).ToLowerInvariant();

                if (extension.Length > 0 &&
                    MIMETypesDictionary.ContainsKey(extension.Remove(0, 1)))
                {
                    return MIMETypesDictionary[extension.Remove(0, 1)];
                }
                return "unknown/unknown";
            }



        }

        public static string GetValidFileName(this string fileName)
        {
            try
            {
                return Regex.Replace(fileName.Trim(), "[^A-Za-z0-9_. ]+", "");
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return "";
        }
        public static bool IsValidEmail(this string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    var domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException e)
            {
                return false;
            }
            catch (ArgumentException e)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public static bool IsValidSizeInMb(this long bytes, double noOfMB)
        {
            var oneMB = 1024 * 1024;//1048576.0
            double divided = (bytes / (double)oneMB);
            if (divided > noOfMB)
            {
                // image is too large
                return false;
            }
            else return true;
        }

        #endregion

        public static string ToStingFromException(this Exception ex)
        {
            try
            {
                string msg = string.Empty;

                if (ex == null) return msg;

                if (ex.GetBaseException() != null)
                    msg = ex.GetBaseException().Message + " >> \n";

                msg = msg + ex.StackTrace + " >> \n" + ex.ToString();

                return msg;
            }
            catch (Exception)
            {
                if (ex != null) return ex.ToString();
                return string.Empty;
            }
        }

        public static void AddIfNotNull<T>(this List<T> obj, T value)
        {
            if (!value.IsNull())
            {
                obj.Add(value);
            }
        }

        public static string ToCamelCase(this string value)
        {
            if (value.IsStringNullOrEmpty())
            {
                return value;
            }
            return char.ToLowerInvariant(value[0]) + value.Substring(1);
        }


        #region Generate Dynamic File
        private static string GenerateStringSize(FileExtensionType extension, long numOfChars)
        {
            StringBuilder generatedText = new StringBuilder();
            if (extension == FileExtensionType.Pdf)
                generatedText.Append("%PDF-");
            Random random = new Random();
            string allows = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            int maxIndex = allows.Length - 1;
            //As per Ticket-PROD-99422 related to veracode CWE ID-331
            //for (int i = 0; i < numOfChars; i++)
            //{
            //    int index = random.Next(maxIndex);
            //    char currentChar = allows[index];
            //    generatedText.Append(currentChar);
            //}
            generatedText.Append(GCSecurity.AhsRandom(allows, numOfChars.ToInt32()));

            return generatedText.ToString();
        }

        public static MemoryStream GenerateDummyFileStreamBySize(this FileExtensionType extension, long sizeInMb)
        {
            string text = GenerateStringSize(extension, sizeInMb * 1024 * 1024);
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            MemoryStream stream = new MemoryStream(bytes);
            return stream;
        }
        #endregion
        #region code to check file is PDF format/not
        public static bool CheckUploadedFileIsGenuinePDFOrNot(this IFormFile attachement)
        {
            var pdfString = "%PDF-";
            var pdfBytes = Encoding.ASCII.GetBytes(pdfString);
            var bytesLength = pdfBytes.Length;
            var byteAry = new byte[bytesLength];
            var remaining = bytesLength;
            var position = 0;
            using (var fileStream = attachement.OpenReadStream())
            {
                while (remaining > 0)
                {
                    var amtRead = fileStream.Read(byteAry, position, remaining);
                    if (amtRead == 0) return false;
                    remaining -= amtRead;
                    position += amtRead;
                }
            }
            return pdfBytes.SequenceEqual(byteAry);
        }
        #endregion

        public static int CompareDate(this DateTime firstDate, DateTime secondDate)
        {
            firstDate = firstDate.AddSeconds(-firstDate.Second).AddMilliseconds(-firstDate.Millisecond);
            secondDate = secondDate.AddSeconds(-secondDate.Second).AddMilliseconds(-secondDate.Millisecond);
            if (firstDate < secondDate)
            {
                return -1;
            }
            else if (firstDate > secondDate)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public static string ToSentenceCase(this string value)
        {
            if (value.IsStringNullOrEmpty())
            {
                return value;
            }
            return char.ToUpperInvariant(value[0]) + value.Substring(1);
        }


        public static DateTime? StringToSystemTimeFromUTC(this string date, string timeZone)
        {
            try
            {
                if (!date.IsStringTrimNullOrEmpty())
                {
                    return DateTimeOffset.Parse(date.TrimText()).DateTime.ToSystemTimeFromUTC(timeZone);
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public static DateTime? StringToDate(this string date)
        {
            try
            {
                if (!date.IsStringTrimNullOrEmpty())
                {
                    return Convert.ToDateTime(date).Date;
                }
            }
            catch (Exception)
            {
                return null;
            }
            return null;
        }

        public static DateTime? StringToDateTime(this string date)
        {
            try
            {
                if (!date.IsStringTrimNullOrEmpty())
                {
                    return DateTimeOffset.Parse(date.TrimText()).DateTime;
                }
            }
            catch (Exception)
            {
                return null;
            }
            return null;
        }

    }
}

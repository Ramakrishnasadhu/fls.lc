﻿using FLS.LC.Cloud.Security;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace FLS.LC.Cloud.Utilities
{
    public static class FileIoAccess
    {
        static readonly string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        public static string ReadAllText(string path)
        {
            string srcrandomId = GenerateRandomID();
            LCSecurityFileIO file = new LCSecurityFileIO(srcrandomId);
            file.AddFileData(srcrandomId, path);
            return file.ReadAllText();
        }

        private static string GenerateRandomID()
        {
            string srcrandomId = GCSecurity.AhsRandom(chars, 8);
            return srcrandomId;
        }
        public static byte[] ReadAllBytes(string path)
        {
            string srcrandomId = GenerateRandomID();
            LCSecurityFileIO file = new LCSecurityFileIO(srcrandomId);
            file.AddFileData(srcrandomId, path);
            return file.ReadAllBytes();
        }
        public static DirectoryInfo CreateDirectory(string path)
        {
            string srcrandomId = GenerateRandomID();
            LCSecurityFileIO file = new LCSecurityFileIO(srcrandomId);
            file.AddFileData(srcrandomId, path);
            return file.CreateDirectory();
        }

        public static bool ExistsDirectory(string path)
        {
            return Directory.Exists(path);
        }

        public static string CreateFolder(string docRootPath, string folderName, string patientId)
        {
            //EXAMPLE: D:\\2013
            string FolderRoot = string.Format("{0}\\{1}", docRootPath, DateTime.Now.Year);
            //EXAMPLE: D:\\2013\\AUG
            string SubFolderPath = string.Format("{0}\\{1}", FolderRoot, DateTime.Now.ToString("MMM", CultureInfo.InvariantCulture));
            //EXAMPLE: D:\\2013\\AUG\\24
            string LeafFolderPath = string.Format("{0}\\Day{1}", SubFolderPath, DateTime.Now.Day);
            //EXAMPLE: D:\\2013\\AUG\\24\\445350\\
            string PateintFolderPathe = string.Format("{0}\\{1}", LeafFolderPath, patientId);
            //EXAMPLE: D:\\2013\\AUG\\24\\445350\\CARE_PLAN
            string DestinationPath = string.Format("{0}\\{1}", PateintFolderPathe, folderName.Replace(" ", "_"));
            if (!ExistsDirectory(DestinationPath))
            {
                if (!ExistsDirectory(FolderRoot))
                    CreateDirectory(FolderRoot);

                if (!ExistsDirectory(SubFolderPath))
                    CreateDirectory(SubFolderPath);

                if (!ExistsDirectory(LeafFolderPath))
                    CreateDirectory(LeafFolderPath);

                if (!ExistsDirectory(PateintFolderPathe))
                    CreateDirectory(PateintFolderPathe);

                if (!ExistsDirectory(DestinationPath))
                    CreateDirectory(DestinationPath);
            }
            return DestinationPath + "\\";
        }

        public static void AppendAllText(string path, string contents)
        {
            string randomId = GenerateRandomID();
            LCSecurityFileIO file = new LCSecurityFileIO(randomId);
            file.AddFileData(randomId, path);
            file.AppendAllText(contents);
        }

        public static FileStream Create(string path)
        {
            string srcrandomId = GenerateRandomID();
            LCSecurityFileIO file = new LCSecurityFileIO(srcrandomId);
            file.AddFileData(srcrandomId, path);
            return file.Create();
        }
        public static bool Exists(string path)
        {
            return File.Exists(path);
        }

        public static FileStream FileStream(string path, FileMode mode)
        {
            string srcrandomId = GenerateRandomID();
            LCSecurityFileIO file = new LCSecurityFileIO(srcrandomId);
            file.AddFileData(srcrandomId, path);
            return file.FileStream(mode);
        }
    }
}

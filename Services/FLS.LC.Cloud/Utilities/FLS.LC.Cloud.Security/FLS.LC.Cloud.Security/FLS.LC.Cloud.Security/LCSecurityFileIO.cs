﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FLS.LC.Cloud.Security
{
    /// <summary>
    ///Wrapper to FileIoAccess to give fix for "External Control of File Name or Path (CWE ID 73) (Directory Traversal)"
    ///Maintain data in dictionary with key.
    ///Get the key and assign it local private variables inside the constructor
    ///Read the respective value from dictionary by using key to perform file operations.
    /// </summary>
    public class LCSecurityFileIO
    {
        #region Private Global Variables
        private readonly string _source;
        private readonly string _destination;
        private ConcurrentDictionary<string, string> FData { get; set; }
        #endregion



        #region Constructors
        public LCSecurityFileIO(string source)
        {
            _source = source;
            _destination = string.Empty;
            FData = new ConcurrentDictionary<string, string>();
        }

        public LCSecurityFileIO(string source, string destination)
        {
            _source = source;
            _destination = destination;
            FData = new ConcurrentDictionary<string, string>();
        }
        #endregion

        #region  File Methods
        public void Copy()
        {
            string srcGenFileName = string.Empty;
            string dstGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            RemoveFileData(_destination, out dstGenFileName);

            var fileName = Path.GetFileName(srcGenFileName);
            var filePath = Path.GetDirectoryName(srcGenFileName);

            var dfileName = Path.GetFileName(dstGenFileName);
            var dfilePath = Path.GetDirectoryName(dstGenFileName);

            if (LCSecurity.AhsIsValidFilePathAndFileName(fileName, filePath) && LCSecurity.AhsIsValidFilePathAndFileName(dfileName, dfilePath))
                File.Copy(srcGenFileName, dstGenFileName);
        }
        public void Copy(bool overwrite)
        {
            string srcGenFileName = string.Empty;
            string dstGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            RemoveFileData(_destination, out dstGenFileName);

            var fileName = Path.GetFileName(srcGenFileName);
            var filePath = Path.GetDirectoryName(srcGenFileName);

            var dfileName = Path.GetFileName(dstGenFileName);
            var dfilePath = Path.GetDirectoryName(dstGenFileName);

            if (LCSecurity.AhsIsValidFilePathAndFileName(fileName, filePath) && LCSecurity.AhsIsValidFilePathAndFileName(dfileName, dfilePath))
                File.Copy(srcGenFileName, dstGenFileName, overwrite);
        }
        public void AppendAllText(string contents)
        {
            string srcGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            File.AppendAllText(srcGenFileName, contents);
        }
        public StreamWriter AppendText()
        {
            string srcGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            return File.AppendText(srcGenFileName);
        }
        public FileStream Create()
        {
            string srcGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            return File.Create(srcGenFileName);
        }
        public FileStream Create(int bufferSize)
        {
            string srcGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            return File.Create(srcGenFileName, bufferSize);
        }
        public FileStream Create(int bufferSize, FileOptions options)
        {
            string srcGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            return File.Create(srcGenFileName, bufferSize, options);
        }

        public FileStream FileStream(FileMode mode)
        {
            string srcGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);

            var fileName = Path.GetFileName(srcGenFileName);
            var filePath = Path.GetDirectoryName(srcGenFileName);

            if (LCSecurity.AhsIsValidFilePathAndFileName(fileName, filePath))
            {
                FileStream fileStream = new FileStream(new FileInfo(srcGenFileName).FullName, mode);
                return fileStream;
            }
            return null;
        }

        public void Delete()
        {
            string srcGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            var fileName = Path.GetFileName(srcGenFileName);
            var filePath = Path.GetDirectoryName(srcGenFileName);
            if (LCSecurity.AhsIsValidFilePathAndFileName(fileName, filePath))
            {
                File.Delete(srcGenFileName);
            }
        }
        public void Move()
        {
            string srcGenFileName = string.Empty;
            string dstGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            RemoveFileData(_destination, out dstGenFileName);
            var fileName = Path.GetFileName(srcGenFileName);
            var filePath = Path.GetDirectoryName(srcGenFileName);
            var dfileName = Path.GetFileName(dstGenFileName);
            var dfilePath = Path.GetDirectoryName(dstGenFileName);
            if (LCSecurity.AhsIsValidFilePathAndFileName(fileName, filePath) && LCSecurity.AhsIsValidFilePathAndFileName(dfileName, dfilePath))
            {
                File.Move(srcGenFileName, dstGenFileName);
            }
        }

        public FileStream OpenRead()
        {
            string srcGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            var fileName = Path.GetFileName(srcGenFileName);
            var filePath = Path.GetDirectoryName(srcGenFileName);
            if (LCSecurity.AhsIsValidFilePathAndFileName(fileName, filePath))
            {
                return File.OpenRead(srcGenFileName);
            }
            return null;
        }

        public FileStream OpenWrite()
        {
            string srcGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            var fileName = Path.GetFileName(srcGenFileName);
            var filePath = Path.GetDirectoryName(srcGenFileName);
            if (LCSecurity.AhsIsValidFilePathAndFileName(fileName, filePath))
            {
                return File.OpenWrite(srcGenFileName);
            }
            return null;
        }

        public byte[] ReadAllBytes()
        {
            string srcGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            var fileName = Path.GetFileName(srcGenFileName);
            var filePath = Path.GetDirectoryName(srcGenFileName);
            if (LCSecurity.AhsIsValidFilePathAndFileName(fileName, filePath))
            {
                return File.ReadAllBytes(srcGenFileName);
            }
            return null;
        }

        public string ReadAllText()
        {
            string srcGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            var fileName = Path.GetFileName(srcGenFileName);
            var filePath = Path.GetDirectoryName(srcGenFileName);
            if (LCSecurity.AhsIsValidFilePathAndFileName(fileName, filePath))
            {
                return File.ReadAllText(srcGenFileName);
            }
            return null;
        }

        public string ReadAllText(Encoding encoding)
        {
            string srcGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            return File.ReadAllText(srcGenFileName, encoding);
        }

        public void WriteAllText(string contents)
        {
            string srcGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            var fileName = Path.GetFileName(srcGenFileName);
            var filePath = Path.GetDirectoryName(srcGenFileName);
            if (LCSecurity.AhsIsValidFilePathAndFileName(fileName, filePath))
            {
                File.WriteAllText(srcGenFileName, contents);
            }
        }

        public void WriteAllText(string contents, Encoding encoding)
        {
            string srcGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            File.WriteAllText(srcGenFileName, contents, encoding);
        }

        public void WriteAllBytes(byte[] bytes)
        {
            string srcGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            var fileName = Path.GetFileName(srcGenFileName);
            var filePath = Path.GetDirectoryName(srcGenFileName);
            if (LCSecurity.AhsIsValidFilePathAndFileName(fileName, filePath))
            {
                File.WriteAllBytes(srcGenFileName, bytes);
            }
        }

        #endregion

        #region Directory Methods
        public DirectoryInfo CreateDirectory()
        {
            string srcGenFileName = string.Empty;
            RemoveFileData(_source, out srcGenFileName);
            var fileName = Path.GetFileName(srcGenFileName);
            var filePath = Path.GetDirectoryName(srcGenFileName);
            if (LCSecurity.AhsIsValidFilePathAndFileName(fileName, filePath))
            {
                return Directory.CreateDirectory(srcGenFileName);
            }
            return null;
        }
        #endregion

        #region Public Implemenatations
        public void AddFileData(string key, string value)
        {
            FData.TryAdd(key, value);
        }
        public void RemoveFileData(string key, out string value)
        {
            FData.TryRemove(key, out value);
        }
        #endregion

        #region private Implementations
        private void GetFileData(string key, out string value)
        {
            FData.TryGetValue(key, out value);
        }
        #endregion

    }
}

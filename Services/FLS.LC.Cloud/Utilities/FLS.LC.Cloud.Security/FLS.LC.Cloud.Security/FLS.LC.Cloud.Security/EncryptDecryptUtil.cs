﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
namespace FLS.LC.Cloud.Security
{
    public static class EncryptDecryptUtil
    {
        private const string InitializationVector = "k17YznTQ#k%xU!r4";
        public static string Encrypt(string key, string textToEncrypt)
        {
            if (string.IsNullOrWhiteSpace(key))
                return textToEncrypt;

            byte[] array;
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = Encoding.UTF8.GetBytes(InitializationVector);
                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(textToEncrypt);
                        }
                        array = memoryStream.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(array);
        }

        public static string Decrypt(string key, string encryptedText)
        {
            if (string.IsNullOrWhiteSpace(key) || !IsBase64Encode(encryptedText))
                return encryptedText;

            byte[] buffer = Convert.FromBase64String(encryptedText);

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = Encoding.UTF8.GetBytes(InitializationVector);
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }

        public static bool IsBase64Encode(string inputString)
        {
            if (string.IsNullOrWhiteSpace(inputString) || (inputString.Length % 4) != 0)
            {
                return false;
            }

            try
            {
                Convert.FromBase64String(inputString);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace FLS.LC.Cloud.Security
{
    public static class LCSecurity
    {
        #region Privalte Global variables

        const string InitializationVector = "6b38217970034272";

        private static readonly char[] InvalidFilenameChars = Path.GetInvalidFileNameChars();
        private static readonly char[] inValidPathChars = Path.GetInvalidPathChars();

        private static readonly List<string> FileExtensionsBlackList = new List<string>
        {
            ".exe",
            ".jsp",
            ".php5",
            ".pht",
            ".phtml",
            ".shtml",
            ".asa",
            ".cer",
            ".asax",
            ".swf",
            ".xap",
            ".asp",
            ".py",
            ".lua",
            ".ps1"
        };

        #endregion

        private static string GetAbsoluteUrl(string inputUrl, HttpClient client)
        {
            Uri inputUri = new Uri(inputUrl, UriKind.RelativeOrAbsolute);
            if (inputUri.IsAbsoluteUri)
            {
                return inputUrl;
            }
            else
            {
                return !client.BaseAddress.ToString().EndsWith("/") ? client.BaseAddress.ToString() + "/" + inputUrl : client.BaseAddress.ToString() + inputUrl;
            }
        }
        public static bool ValidateURL(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public static async Task<HttpResponseMessage> AhsSecureGetAsync(this HttpClient httpClient, string localUrl)
        {
            if (ValidateURL(GetAbsoluteUrl(localUrl, httpClient)))
            {
                HttpResponseMessage response = await httpClient.GetAsync(localUrl);
                return response;
            }
            else
            {
                var gcSecurityException = new LCSecurityException(string.Format("The input URL is not a trusted one. The URL is: {0}", localUrl));
                throw gcSecurityException;
            }
        }
        public static async Task<HttpResponseMessage> AhsSecurePostAsync(this HttpClient httpClient, string localUrl, HttpContent httpContent)
        {
            if (ValidateURL(GetAbsoluteUrl(localUrl, httpClient)))
            {
                HttpResponseMessage response = await httpClient.PostAsync(localUrl, httpContent);
                return response;
            }
            else
            {
                var gcSecurityException = new LCSecurityException(string.Format("The input URL is not a trusted one. The URL is: {0}", localUrl));
                throw gcSecurityException;
            }
        }
        public static async Task<HttpResponseMessage> AhsSecurePutAsync(this HttpClient httpClient, string localUrl, HttpContent httpContent)
        {
            if (ValidateURL(GetAbsoluteUrl(localUrl, httpClient)))
            {
                HttpResponseMessage response = await httpClient.PutAsync(localUrl, httpContent);
                return response;
            }
            else
            {
                var gcSecurityException = new LCSecurityException(string.Format("The input URL is not a trusted one. The URL is: {0}", localUrl));
                throw gcSecurityException;
            }
        }
        public static async Task<HttpResponseMessage> AhsSecureSendAsync(this HttpClient httpClient, HttpRequestMessage request)
        {
            HttpResponseMessage response = await httpClient.SendAsync(request);
            return response;
        }
        /// <summary>
        /// Generates randomId to maintain unique Id's in the application
        /// This will give fix for "Insufficient Entropy (CWE ID 331)"
        /// </summary>
        public static string AhsRandom(string pattern, int length, bool isPassword = false)
        {
            return GenerateDynamicCryptoString(pattern, length, isPassword);
        }
        public static string GenerateDynamicCryptoString(string pattern, int length, bool isPassword = false)
        {
            if (length != 0)
            {
                RNGCryptoServiceProvider provider = CreateRNGCryptoServiceProvider();
                StringBuilder sb;
                do
                {
                    sb = new StringBuilder();
                    var byteArray = new byte[length > 3 ? length : 4];
                    provider.GetBytes(byteArray);

                    int patternLength = pattern.Length;

                    //Gets character on index based from the pattern passed
                    for (var i = 0; i < byteArray.Length; i++)
                    {
                        byte x = byteArray[i];
                        while (x >= patternLength)
                            x = Convert.ToByte(x % patternLength);
                        sb.Append(pattern[x]);
                    }

                    //Below condition checks if atleast one special character is avaible in generated string.
                    isPassword = HasSpecialCharacters(sb, pattern, isPassword);
                } while (isPassword);
                return sb.ToString();
            }
            return string.Empty;
        }
        /// <summary>
        /// To validate the filepath and filename of a given input path.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool AhsIsValidFilePathAndFileName(string fileName, string filePath)
        {
            if (!IsValidFileName(fileName))
            {
                return false;
            }
            string decodedFilePath = GetDecodedFilePath(filePath);
            return IsValidFilePath(decodedFilePath);
        }
        /// <summary>
        /// validate the file name by verifying invalid characters in input string
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool IsValidFileName(string fileName)
        {
            //Checking for invalid file extensions
            if (FileExtensionsBlackList.Any(e => fileName.Contains(e)))
            {
                return false;
            }
            //Checking for any invalid characters suggested by DotNet Framework in filename
            if (fileName.IndexOfAny(InvalidFilenameChars) >= 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// validate the file path by verifying invalid characters in input string
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool IsValidFilePath(string path)
        {
            //Checking for any invalid characters given by DotNet Framework in path
            if (path.IndexOfAny(inValidPathChars) >= 0)
            {
                return false;
            }
            //Check for the characters in the path by which we may get "directory traversal"
            if (path.Contains(".."))
            {
                return false;
            }
            //Check whether input path is a known path or not
            //if (!IsKnownPath(path))
            //{
            //    return false;
            //}
            return true;
        }
        private static string GetDecodedFilePath(string filePath)
        {
            string decodedFilePath = filePath;
            string pathForComparison = decodedFilePath;
            do
            {
                pathForComparison = decodedFilePath;
                decodedFilePath = HttpUtility.UrlDecode(decodedFilePath);
            }
            while (!decodedFilePath.Equals(pathForComparison));
            return decodedFilePath;
        }

        //private static bool IsKnownPath(string path)
        //{
        //    UpdateKnownPathsWhiteListIfRequired();
        //    return KnownFilePathsWhiteList.Any(p =>
        //    {
        //        var updatedWhiteListPath = p.Replace("\\\\", "\\");
        //        var updatedInputPath = path.Replace("\\\\", "\\");
        //        return string.Concat(updatedInputPath, "\\").StartsWith(updatedWhiteListPath) || updatedInputPath.StartsWith(updatedWhiteListPath);
        //    });
        //}


        private static RNGCryptoServiceProvider CreateRNGCryptoServiceProvider()
        {
            RNGCryptoServiceProvider rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            return rngCryptoServiceProvider;
        }
        private static bool HasSpecialCharacters(StringBuilder sb, string pattern, bool isPassword)
        {
            //Below condition checks if atleast one special character is avaible in generated string.
            if (isPassword)
            {
                var regex = new Regex("[a-zA-Z0-9]*");
                var specialChars = regex.Replace(pattern, "");
                if (!string.IsNullOrEmpty(specialChars))
                {
                    var specialCharsRegex = new Regex("[" + specialChars + "]");
                    isPassword = !specialCharsRegex.IsMatch(sb.ToString());
                }
                else
                    isPassword = false;
            }
            return isPassword;
        }

        public static string AhsHttpHeaderEncode(string data, bool isEncode = true)
        {
            //if (!IsGCSecurityEnabled)
            //{
            //    return data;
            //}
            if (isEncode)
            {
                return ReplaceHttpResponseSplittingChar(data);
            }
            return data;
        }
        private static string ReplaceHttpResponseSplittingChar(string data)
        {
            string resultData = ReplaceSlashR(data);
            resultData = ReplaceSlashN(resultData);
            resultData = ReplacePercent0D(resultData);
            resultData = ReplacePercent0A(resultData);

            return resultData;
        }
        /// <summary>
        /// Replace CR ("\r") with empty string
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static string ReplaceSlashR(string data)
        {
            string resultData = Regex.Replace(data, "\r", string.Empty, RegexOptions.IgnoreCase);
            return resultData;
        }
        /// <summary>
        /// Replace LF ("\n") with empty string
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static string ReplaceSlashN(string data)
        {
            string resultData = Regex.Replace(data, "\n", string.Empty, RegexOptions.IgnoreCase);
            return resultData;
        }
        /// <summary>
        /// Replace CR ("%0d") with empty string
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static string ReplacePercent0D(string data)
        {
            string resultData = Regex.Replace(data, "%0d", string.Empty, RegexOptions.IgnoreCase);
            return resultData;
        }
        /// <summary>
        /// Replace LF ("%0a") with empty string
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static string ReplacePercent0A(string data)
        {
            string resultData = Regex.Replace(data, "%0a", string.Empty, RegexOptions.IgnoreCase);
            return resultData;
        }
    }

}

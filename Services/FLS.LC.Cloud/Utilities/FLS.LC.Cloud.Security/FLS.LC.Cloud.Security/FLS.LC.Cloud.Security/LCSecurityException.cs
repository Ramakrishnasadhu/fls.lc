﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FLS.LC.Cloud.Security
{
    [Serializable]
    public class LCSecurityException : Exception
    {
        public LCSecurityException(string errorMessage) : base(errorMessage)
        {
        }
        public LCSecurityException(string errorMessage, Exception innerException) : base(errorMessage, innerException)
        {
        }
    }
}

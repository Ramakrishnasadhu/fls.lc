﻿using FLS.LC.Cloud.Master.DTO;
using FLS.LC.Cloud.Master.DTO.Common;
using FLS.LC.Cloud.Master.DTO.GarmentConfig;
using FLS.LC.Cloud.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LS.Cloud.Master.BusinessService.Interfaces
{
    public interface IGarmentConfig
    {
        Task<OperationResponse<GarmentslistConfigurationDto>> UpdateGarmentConfigAsync(GarmentslistConfigurationDto GarmentslistConfigurationDto);
        Task<OperationResponse<GarmentspriceConfigurationDto>> ImportGarmentsPriceListAsync(GarmentspriceConfigurationDto GarmentspriceConfigurationDto);
        Task<ActionReturnType> GetAllRegisteredGarmentsListAsync(GarmentListDto GarmentListDto);
        Task<ActionReturnType> GetRegisteredPriceListByStoreAsync(GarmentListDto GarmentListDto);
        Task<ActionReturnType> updateRegisteredPriceListByStoreAsync(updateRegisteredPriceListByStoreDto updateRegisteredPriceListByStoreDto);
        Task<ActionReturnType> UpdateServicePriceByGarmentAsync(UpdateServicePriceByGarmentDto updateServicePriceByGarmentDto);
    }
}

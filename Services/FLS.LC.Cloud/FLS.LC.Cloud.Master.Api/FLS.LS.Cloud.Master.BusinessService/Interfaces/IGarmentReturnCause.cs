﻿using FLS.LC.Cloud.Master.DataModels.Entities;
using FLS.LC.Cloud.Master.DTO.Common;
using FLS.LC.Cloud.Master.DTO.GarmentReturnCause;
using FLS.LC.Cloud.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LS.Cloud.Master.BusinessService.Interfaces
{
    public interface IGarmentReturnCause
    {
        Task<ActionReturnType> GetRegisteredGarmentReturnCauseAsync();
        Task<OperationResponse<GarmentReturnCause>> AddGarmentReturnCauseAsync(GarmentReturnCausePostDto garmentReturnCausePostDto);
    }
}

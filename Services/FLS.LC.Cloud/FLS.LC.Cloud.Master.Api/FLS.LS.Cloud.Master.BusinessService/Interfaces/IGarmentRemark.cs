﻿using FLS.LC.Cloud.Master.DataModels.Entities;
using FLS.LC.Cloud.Master.DTO.Common;
using FLS.LC.Cloud.Master.DTO.GarmentRemark;
using FLS.LC.Cloud.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LS.Cloud.Master.BusinessService.Interfaces
{
    public interface IGarmentRemark
    {
        Task<OperationResponse<GarmentsRemark>> AddGarmentRemarkAsync(GarmentRemarkPostRequestDto garmentRemarkPostRequestDto);

        Task<OperationResponse<GarmentsRemark>> InactivateGarmentRemarkAsync(GarmentRemarkRequestDto garmentRemarkRequestDto);
        Task<ActionReturnType> GetRegisteredGarmentRemarkAsync();
    }
}

﻿using FLS.LC.Cloud.Master.DataModels.Entities;
using FLS.LC.Cloud.Master.DTO;
using FLS.LC.Cloud.Master.DTO.Common;
using FLS.LC.Cloud.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LS.Cloud.Master.BusinessService.Interfaces
{
    public interface IStore
    {
        Task<OperationResponse<UserStoreDetail>> StoreOperationAsync(UserStoreDetailDto userStore);
        Task<ActionReturnType> RegisteredStoresAsync(string userEmail);
        Task<ActionReturnType> SelectedStoresAsync(string userEmail,string UstStoreName);
        Task<OperationResponse<UserStoreDetail>> InActivateSelectedStoreAsync(string userEmail, string UstStoreName);
    }
}

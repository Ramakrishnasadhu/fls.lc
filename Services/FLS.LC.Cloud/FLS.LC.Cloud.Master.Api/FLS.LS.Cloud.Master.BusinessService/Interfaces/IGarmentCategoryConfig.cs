﻿using FLS.LC.Cloud.Master.DTO;
using FLS.LC.Cloud.Master.DTO.Common;
using FLS.LC.Cloud.Master.DTO.GarmentCategoryConfig;
using FLS.LC.Cloud.Master.DTO.GarmentConfig;
using FLS.LC.Cloud.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LS.Cloud.Master.BusinessService.Interfaces
{
    public interface IGarmentCategoryConfig
    {
        Task<OperationResponse<GarmentsCategoryDto>> GarmentCategoryConfigAsync(GarmentsCategoryDto GarmentsCategoryInput);
        Task<ActionReturnType> RegisteredGarmentNamesListAsync(GarmentCategoryDto GarmentCategoryDto);
        Task<ActionReturnType> RegisteredGarmentCategorysAsync(GarmentCategoryDto GarmentCategoryDto);
        Task<ActionReturnType> RegisteredGarmentListServiceAsync(GarmentCategoryDto GarmentCategoryDto);
        Task<OperationResponse<GarmentspriceConfigurationReadDto>> GetRequiredGarmentAsync(RequiredGarmentDto requiredGarmentDto);

    }
}

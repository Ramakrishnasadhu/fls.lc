﻿using FLS.LC.Cloud.Master.DataModels.Entities;
using FLS.LC.Cloud.Master.DTO.Common;
using FLS.LC.Cloud.Master.DTO.GarmentCheckingStaff;
using FLS.LC.Cloud.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LS.Cloud.Master.BusinessService.Interfaces
{
    public interface IGarmentCheckingStaff
    {
        Task<OperationResponse<GarmentCheckingStaff>> AddGarmentCheckingStaffAsync(GarmentCheckingStaffRequestDto garmentCheckingStaffRequestDto);

        Task<ActionReturnType> GetRegisteredGarmentCheckingStaffAsync();

    }
}

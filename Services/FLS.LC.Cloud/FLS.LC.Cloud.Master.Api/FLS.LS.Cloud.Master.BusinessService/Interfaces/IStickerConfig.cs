﻿using FLS.LC.Cloud.Master.DataModels.Entities;
using FLS.LC.Cloud.Master.DTO;
using FLS.LC.Cloud.Master.DTO.Common;
using FLS.LC.Cloud.Master.DTO.StickerConfig;
using FLS.LC.Cloud.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LS.Cloud.Master.BusinessService.Interfaces
{
    public interface IStickerConfig
    {
        Task<OperationResponse<List<StickerConfiguration>>> PostStickerConfigAsync(StickerConfigurationObject StickerConfigurationObject);
        Task<List<string>> GetAllStickerConfiConfigAsync(StickerConfigDto StickerConfigDto);
        Task<ActionReturnType> SelectedStoreStickerConfigAsync(string userEmail, string UstStoreName);

    }
}

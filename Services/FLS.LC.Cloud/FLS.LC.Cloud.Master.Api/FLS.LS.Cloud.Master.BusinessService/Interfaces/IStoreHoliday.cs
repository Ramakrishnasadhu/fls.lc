﻿using FLS.LC.Cloud.Master.DataModels.Entities;
using FLS.LC.Cloud.Master.DTO.Common;
using FLS.LC.Cloud.Master.DTO.StoreHoliday;
using FLS.LC.Cloud.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LS.Cloud.Master.BusinessService.Interfaces
{
    public interface IStoreHoliday
    {
        Task<ActionReturnType> GetRegisteredStoreHolidayAsync(string userEmail);
        Task<ActionReturnType> GetRegisteredStoreHolidayBySelectedStoreAsync(string userEmail, string userStore);
        Task<ActionReturnType> GetStoreWeekOffHolidayAsync(string userEmail, string userStore);
        Task<OperationResponse<UserStoreWeekOffHoliday>> AddStoreWeekOffHolidayAsync(StoreWeekOffHolidayRequestDto storeWeekOffHolidayRequestDto);
        Task<OperationResponse<UserStoreHoliday>> AddStoreHolidayAsync(StoreHolidayRequestDto StoreHolidayRequestDto);
    }
}

﻿using AutoMapper;
using FLS.LC.Cloud.Master.DataModels.Entities;
using FLS.LC.Cloud.Master.DTO.Common;
using FLS.LC.Cloud.Master.DTO.GarmentCheckingStaff;
using FLS.LC.Cloud.Repository.UnitOfWork;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LS.Cloud.Master.BusinessService.Services
{
    public class GarmentCheckingStaffService : IGarmentCheckingStaff
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly string _timeZone;
        private readonly string _sharedSecretkey;
        private readonly IHostingEnvironment _hostingEnv;

        public GarmentCheckingStaffService(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration, IHostingEnvironment hostingEnv)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _configuration = configuration;
            _timeZone = _configuration.GetValue<string>("TimeZone:DatabaseTimeZone");
            _sharedSecretkey = _configuration.GetValue<string>("SharedKey:SharedSecretkey");
            _hostingEnv = hostingEnv;
        }
        public async Task<OperationResponse<GarmentCheckingStaff>> AddGarmentCheckingStaffAsync(GarmentCheckingStaffRequestDto garmentCheckingStaffRequestDto)
        {
            GarmentCheckingStaff garmentCheckingStaff = new GarmentCheckingStaff();
            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == garmentCheckingStaffRequestDto.GpcUsremail).FirstOrDefaultAsync());
            if (userDetails == null)
                return new OperationResponse<GarmentCheckingStaff>()
                                          .SetAsFailureResponse(OperationErrorDictionary.GarmentCategoryNotExist.UserDoesNotExist());
            else
            {
                Users usersResponseItem = _mapper.Map<Users>(userDetails);
                var garmentCheckingStaffResponseItem = await (_unitOfWork.GetRepository<GarmentCheckingStaff>().Get(x => x.GpcUsremail == garmentCheckingStaffRequestDto.GpcUsremail
                      && x.GpcUstname == garmentCheckingStaffRequestDto.GpcUstname).ToListAsync());
                if (garmentCheckingStaffResponseItem.IsListNullOrEmpty())
                {
                    garmentCheckingStaff.GpcUsremail = garmentCheckingStaffRequestDto.GpcUsremail;
                    garmentCheckingStaff.GpcUstname = garmentCheckingStaffRequestDto.GpcUstname;
                    garmentCheckingStaff.GcsEmployeeName = garmentCheckingStaffRequestDto.GcsEmployeeName;
                    garmentCheckingStaff.GcsEmployeeEmail = garmentCheckingStaffRequestDto.GcsEmployeeEmail;
                    garmentCheckingStaff.GcsEmployeeAddress = garmentCheckingStaffRequestDto.GcsEmployeeAddress;
                    garmentCheckingStaff.GcsEmployeeMobileNo = garmentCheckingStaffRequestDto.GcsEmployeeMobileNo;
                    garmentCheckingStaff.CreatedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                    garmentCheckingStaff.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                    await _unitOfWork.GetRepository<GarmentCheckingStaff>().InsertAsync(garmentCheckingStaff);
                    await _unitOfWork.SaveChangesAsync();
                    return new OperationResponse<GarmentCheckingStaff>(garmentCheckingStaff);
                }
                else 
                {
                    GarmentCheckingStaff responseItem = await (_unitOfWork.GetRepository<GarmentCheckingStaff>().Get(x => x.GpcUsremail == garmentCheckingStaffRequestDto.GpcUsremail && x.GpcUstname == garmentCheckingStaffRequestDto.GpcUstname
                      && x.GpcUstname == garmentCheckingStaffRequestDto.GpcUstname 
                      && x.GcsEmployeeName.Contains(garmentCheckingStaffRequestDto.GcsEmployeeName))).FirstOrDefaultAsync();
                    responseItem.GcsEmployeeAddress = garmentCheckingStaffRequestDto.GcsEmployeeAddress;
                    responseItem.GcsEmployeeMobileNo = garmentCheckingStaffRequestDto.GcsEmployeeMobileNo;
                    responseItem.GcsEmployeeAddress = garmentCheckingStaffRequestDto.GcsEmployeeAddress;
                    responseItem.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                    _unitOfWork.GetRepository<GarmentCheckingStaff>().Update(responseItem);
                    await _unitOfWork.SaveChangesAsync();
                    return new OperationResponse<GarmentCheckingStaff>(garmentCheckingStaff);
                }
            }
        }
        public async Task<ActionReturnType> GetRegisteredGarmentCheckingStaffAsync()
        {

            var resultRegGarmentCheckingStaff = await (_unitOfWork.GetRepository<GarmentCheckingStaff>().Get().ToListAsync());
            var result = _mapper.Map<List<GarmentCheckingStaffResponseDto>>(resultRegGarmentCheckingStaff);

            if (resultRegGarmentCheckingStaff.IsListNullOrEmpty())
            {
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
            }
            else
            {

                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, result, result.Count);
            }
        }

    }
}

﻿using AutoMapper;
using FLS.LC.Cloud.Master.DataModels.Entities;
using FLS.LC.Cloud.Master.DTO;
using FLS.LC.Cloud.Master.DTO.Common;
using FLS.LC.Cloud.Repository.UnitOfWork;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;

namespace FLS.LS.Cloud.Master.BusinessService.Services
{
    public class StoreService : IStore
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly string _timeZone;
        private readonly string _sharedSecretkey;
        public StoreService(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _configuration = configuration;
            _timeZone = _configuration.GetValue<string>("TimeZone:DatabaseTimeZone");
            _sharedSecretkey = _configuration.GetValue<string>("SharedKey:SharedSecretkey");
        }

        public async Task<OperationResponse<UserStoreDetail>> StoreOperationAsync(UserStoreDetailDto userStore)
        {
            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == userStore.UsrEmail).FirstOrDefaultAsync());
            if (userDetails == null)
                return new OperationResponse<UserStoreDetail>()
                                          .SetAsFailureResponse(OperationErrorDictionary.GarmentCategoryNotExist.UserDoesNotExist());
            else
            {
                Users userDetailsResponseItem = _mapper.Map<Users>(userDetails);
                //  var userstoreDetailsItem = _mapper.Map<UserStoreDetail>(userStore);

                if (userStore.Operation == 1)//add
                {
                    var storeDetailsResponse = await (_unitOfWork.GetRepository<UserStoreDetail>().Get(x => x.UsrId == userDetailsResponseItem.UsrId
                      && x.UstName == userStore.UstName && x.UstIsdelete!=true).FirstOrDefaultAsync());

                    if (storeDetailsResponse != null)
                    {
                        return new OperationResponse<UserStoreDetail>()
                                              .SetAsFailureResponse(OperationErrorDictionary.GarmentCategoryNotExist.UserStoreAlreadyExist());
                    }
                    else
                    {
                        UserStoreDetail UserStorePostDetail = new UserStoreDetail();
                        string[] saAllowedCharacters = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V" };
                        string sRandomStoreCode = GeneratePassword.GenerateRandomOTP(4, saAllowedCharacters);
                        MemoryStream ms = new MemoryStream();
                        userStore.UstLogo.CopyTo(ms);
                        UserStorePostDetail.UstLogo = ms.ToArray();
                        UserStorePostDetail.UstName = userStore.UstName;
                        UserStorePostDetail.UstEmail = userStore.UstEmail;
                        UserStorePostDetail.UsrId = userDetailsResponseItem.UsrId;
                        UserStorePostDetail.UstIsdelete = false;
                        if(userStore.UstName.Length>4)
                        UserStorePostDetail.UstCode = userStore.UstName.Substring(0, 4).ToUpper() + sRandomStoreCode;
                        else
                            UserStorePostDetail.UstCode = userStore.UstName.ToUpper() + sRandomStoreCode;
                        UserStorePostDetail.UstBusinessName = userStore.UstBusinessName;
                        UserStorePostDetail.UstAddress = userStore.UstAddress;
                        UserStorePostDetail.UstMobileNo = userStore.UstMobileNo;
                        UserStorePostDetail.UstAreaLocation = userStore.UstAreaLocation;
                        UserStorePostDetail.UstCity = userStore.UstCity;
                        UserStorePostDetail.UstState = userStore.UstState;
                        UserStorePostDetail.UstOpeningTime = userStore.UstOpeningTime;
                        UserStorePostDetail.UstClosingTime = userStore.UstClosingTime;
                        UserStorePostDetail.UstWebsiteUrl = userStore.UstWebsiteUrl;
                        UserStorePostDetail.UstWeekOff = userStore.UstWeekOff;
                        if (userStore.UstOpeningTime != string.Empty && userStore.UstClosingTime != string.Empty)
                            UserStorePostDetail.UstOperatingtime = true;
                        else
                            UserStorePostDetail.UstOperatingtime = false;
                        UserStorePostDetail.UstBusinessSlogan = userStore.UstBusinessSlogan;
                        UserStorePostDetail.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                        UserStorePostDetail.CreatedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                        await _unitOfWork.GetRepository<UserStoreDetail>().InsertAsync(UserStorePostDetail);
                        await _unitOfWork.SaveChangesAsync();
                        return new OperationResponse<UserStoreDetail>(UserStorePostDetail);
                    }
                }
                else if (userStore.Operation == 2) //edit
                {

                    var storeDetailsResponse = await (_unitOfWork.GetRepository<UserStoreDetail>().Get(x => x.UsrId == userDetailsResponseItem.UsrId && x.UstCode == userStore.UstCode).FirstOrDefaultAsync());

                   // var Item = _mapper.Map<UserStoreDetail>(storeDetailsResponse);
                    if (storeDetailsResponse != null)
                    {
                        MemoryStream ms = new MemoryStream();
                        userStore.UstLogo.CopyTo(ms);

                        storeDetailsResponse.UstLogo = ms.ToArray();
                        storeDetailsResponse.UstName = userStore.UstName;
                        storeDetailsResponse.UstEmail = userStore.UstEmail;
                        storeDetailsResponse.UstBusinessName = userStore.UstBusinessName;
                        storeDetailsResponse.UstAddress = userStore.UstAddress;
                        storeDetailsResponse.UstMobileNo = userStore.UstMobileNo;
                        storeDetailsResponse.UstAreaLocation = userStore.UstAreaLocation;
                        storeDetailsResponse.UstCity = userStore.UstCity;
                        storeDetailsResponse.UstState = userStore.UstState;
                        storeDetailsResponse.UstOpeningTime = userStore.UstOpeningTime;
                        storeDetailsResponse.UstClosingTime = userStore.UstClosingTime;
                        storeDetailsResponse.UstWebsiteUrl = userStore.UstWebsiteUrl;
                        if (userStore.UstOpeningTime != string.Empty && userStore.UstClosingTime != string.Empty)
                            storeDetailsResponse.UstOperatingtime = true;
                        else
                            storeDetailsResponse.UstOperatingtime = false;
                        storeDetailsResponse.UstBusinessSlogan = userStore.UstBusinessSlogan;
                        storeDetailsResponse.UstWeekOff = userStore.UstWeekOff;
                        storeDetailsResponse.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                        _unitOfWork.GetRepository<UserStoreDetail>().Update(storeDetailsResponse);
                        await _unitOfWork.SaveChangesAsync();
                        return new OperationResponse<UserStoreDetail>(storeDetailsResponse);
                    }
                }
                return new OperationResponse<UserStoreDetail>()
                                              .SetAsFailureResponse(OperationErrorDictionary.GarmentCategoryNotExist.UserStoreInvalidOperation());
            }
        }

        public async Task<OperationResponse<UserStoreDetail>> InActivateSelectedStoreAsync(string userEmail, string UstStoreName)
        {
            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == userEmail).SingleOrDefaultAsync());
            if (userDetails == null)
                return new OperationResponse<UserStoreDetail>()
                                    .SetAsFailureResponse(OperationErrorDictionary.GarmentCategoryNotExist.UserDoesNotExist());
            else
            {       
                var resultRegStore = await (_unitOfWork.GetRepository<UserStoreDetail>().Get(x => x.UsrId == userDetails.UsrId && x.UstName == UstStoreName).FirstOrDefaultAsync());
                if (resultRegStore==null)
                {
                    return new OperationResponse<UserStoreDetail>()
                                    .SetAsFailureResponse(OperationErrorDictionary.GarmentCategoryNotExist.UserStoreNotExist());
                }
                else
                {
                    resultRegStore.UstIsdelete = true;
                    resultRegStore.DeletedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                    resultRegStore.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                    _unitOfWork.GetRepository<UserStoreDetail>().Update(resultRegStore);
                    await _unitOfWork.SaveChangesAsync();
                    return new OperationResponse<UserStoreDetail>(resultRegStore);
                }
            }
        }

        public async Task<ActionReturnType> SelectedStoresAsync(string userEmail, string UstStoreName)
        {
            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == userEmail).SingleOrDefaultAsync());
            if (userDetails == null)
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
            else
            {
                var resultRegStores = await (_unitOfWork.GetRepository<UserStoreDetail>().Get(x => x.UsrId == userDetails.UsrId && x.UstName == UstStoreName).ToListAsync());

                if (resultRegStores.IsListNullOrEmpty())
                {
                    return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
                }
                else
                {
                    return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, resultRegStores, resultRegStores.Count);
                }
            }
        }
        public async Task<ActionReturnType> RegisteredStoresAsync(string userEmail)
        {
            try
            {
                var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == userEmail).SingleOrDefaultAsync());
                if (userDetails == null)
                    return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
                else
                {
                    var resultRegStores = await (_unitOfWork.GetRepository<UserStoreDetail>().Get(x => x.UsrId == userDetails.UsrId && x.UstIsdelete == false).ToListAsync());

                    if (resultRegStores.IsListNullOrEmpty())
                    {
                        return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
                    }
                    else
                    {
                        return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, resultRegStores, resultRegStores.Count);
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

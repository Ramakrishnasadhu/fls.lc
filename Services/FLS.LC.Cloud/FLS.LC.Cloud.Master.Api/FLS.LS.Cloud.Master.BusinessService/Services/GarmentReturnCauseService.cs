﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using FLS.LC.Cloud.Master.DataModels.Entities;
using FLS.LC.Cloud.Master.DTO.GarmentReturnCause;
using FLS.LC.Cloud.Repository.UnitOfWork;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FLS.LC.Cloud.Master.DTO.Common;

namespace FLS.LS.Cloud.Master.BusinessService.Services
{
    public class GarmentReturnCauseService : IGarmentReturnCause
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly string _timeZone;
        private readonly string _sharedSecretkey;
        private readonly IHostingEnvironment _hostingEnv;
        public GarmentReturnCauseService(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration, IHostingEnvironment hostingEnv)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _configuration = configuration;
            _timeZone = _configuration.GetValue<string>("TimeZone:DatabaseTimeZone");
            _sharedSecretkey = _configuration.GetValue<string>("SharedKey:SharedSecretkey");
            _hostingEnv = hostingEnv;
        }
        public async Task<ActionReturnType> GetRegisteredGarmentReturnCauseAsync()
        {

            var resultRegGarmentsRemarks = await (_unitOfWork.GetRepository<GarmentReturnCause>().Get().ToListAsync());
            var result = _mapper.Map<List<GarmentReturnCauseResponseDto>>(resultRegGarmentsRemarks);

            if (resultRegGarmentsRemarks.IsListNullOrEmpty())
            {
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
            }
            else
            {

                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, result, result.Count);
            }
        }
        public async Task<OperationResponse<GarmentReturnCause>> AddGarmentReturnCauseAsync(GarmentReturnCausePostDto garmentReturnCausePostDto)
        {
            var garmentReturnCauseResponse = await (_unitOfWork.GetRepository<GarmentReturnCause>().Get(x => x.GreReturnCauseDescription == garmentReturnCausePostDto.garmentReturnCauseOldValue
            && x.GrecId == garmentReturnCausePostDto.GrecId).FirstOrDefaultAsync());

            if (garmentReturnCauseResponse == null)
            {
                GarmentReturnCause garmentReturnCause = new GarmentReturnCause();
                garmentReturnCause.GreReturnCauseDescription = garmentReturnCausePostDto.garmentReturnCauseNewValue;
                garmentReturnCause.GrecIsActive = true;
                garmentReturnCause.CreatedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                garmentReturnCause.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                await _unitOfWork.GetRepository<GarmentReturnCause>().InsertAsync(garmentReturnCause);
                await _unitOfWork.SaveChangesAsync();
                return new OperationResponse<GarmentReturnCause>(garmentReturnCause);
            }
            else if (garmentReturnCauseResponse != null)
            {
                garmentReturnCauseResponse.GreReturnCauseDescription = garmentReturnCausePostDto.garmentReturnCauseNewValue;
                garmentReturnCauseResponse.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                _unitOfWork.GetRepository<GarmentReturnCause>().Update(garmentReturnCauseResponse);
                await _unitOfWork.SaveChangesAsync();
                return new OperationResponse<GarmentReturnCause>(garmentReturnCauseResponse);
            }
            return new OperationResponse<GarmentReturnCause>(garmentReturnCauseResponse);
        }
    }
}

﻿using AutoMapper;
using FLS.LC.Cloud.Master.DataModels.Entities;
using FLS.LC.Cloud.Master.DTO;
using FLS.LC.Cloud.Master.DTO.Common;
using FLS.LC.Cloud.Master.DTO.OrderConfig;
using FLS.LC.Cloud.Master.DTO.StickerConfig;
using FLS.LC.Cloud.Repository.UnitOfWork;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LS.Cloud.Master.BusinessService.Services
{
    public class StickerConfigService : IStickerConfig
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly string _timeZone;
        private readonly string _sharedSecretkey;
        public StickerConfigService(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _configuration = configuration;
            _timeZone = _configuration.GetValue<string>("TimeZone:DatabaseTimeZone");
            _sharedSecretkey = _configuration.GetValue<string>("SharedKey:SharedSecretkey");
        }
        private async Task<bool> UpdateStickerConfiguration(StickerConfigurationDto StickerConfigurationDto, string usrEmail, string ustStoreName, string Option)
        {
            var resultRegStickerConfig = await (_unitOfWork.GetRepository<StickerConfiguration>().Get(x => x.StcUsremail == usrEmail
                                         && x.StcUstname == ustStoreName && x.StcOption == Option).SingleOrDefaultAsync());
            StickerConfiguration StickerConfigurationResponseItem = _mapper.Map<StickerConfiguration>(resultRegStickerConfig);
            StickerConfigurationResponseItem.StcUsremail = usrEmail;
            StickerConfigurationResponseItem.StcUstname = ustStoreName;
            StickerConfigurationResponseItem.StcOption = StickerConfigurationDto.StcOption;
            StickerConfigurationResponseItem.StcPrintIsenable = StickerConfigurationDto.StcPrintIsenable;
            StickerConfigurationResponseItem.StcFontName = StickerConfigurationDto.StcFontName;
            StickerConfigurationResponseItem.StcTextAlign = StickerConfigurationDto.StcTExtAlign;
            StickerConfigurationResponseItem.StcTextSize = StickerConfigurationDto.StcTextSize;
            StickerConfigurationResponseItem.StcTextStyle = StickerConfigurationDto.StcTextStyle;
            StickerConfigurationResponseItem.CreatedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
            StickerConfigurationResponseItem.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);

            _unitOfWork.GetRepository<StickerConfiguration>().Update(StickerConfigurationResponseItem);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }
        private async Task<bool> InsertStickerConfiguration(StickerConfigurationDto StickerConfigurationDto, string usrEmail, string ustStoreName)
        {
            try
            {
                var StickerConfiguration = new StickerConfiguration()
                {
                    StcUsremail = usrEmail,
                    StcUstname = ustStoreName,
                    StcOption = StickerConfigurationDto.StcOption,
                    StcPrintIsenable = StickerConfigurationDto.StcPrintIsenable,
                    StcFontName = StickerConfigurationDto.StcFontName,
                    StcTextAlign = StickerConfigurationDto.StcTExtAlign,
                    StcTextSize = StickerConfigurationDto.StcTextSize,
                    StcTextStyle = StickerConfigurationDto.StcTextStyle,
                    CreatedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone),
                    ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone),
                };
                await _unitOfWork.GetRepository<StickerConfiguration>().InsertAsync(StickerConfiguration);
                await _unitOfWork.SaveChangesAsync();
                return true;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public async Task<OperationResponse<List<StickerConfiguration>>> PostStickerConfigAsync(StickerConfigurationObject StickerConfigurationDto)
        {
            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == StickerConfigurationDto.StcUsremail).SingleOrDefaultAsync());

            var stickerConfigurationDetails = await (_unitOfWork.GetRepository<StickerConfiguration>().Get(x => x.StcUsremail == StickerConfigurationDto.StcUsremail &&
               x.StcUstname == StickerConfigurationDto.StcUstname).ToListAsync());
            bool status = false;
            if (StickerConfigurationDto.Operation == 1 && stickerConfigurationDetails.Count == 0)
            {
                foreach (var itemmaster in StickerConfigurationDto.listStickerConfigurationDto)
                {
                    if (itemmaster.StcOption.Trim().ToLower() == "barcode")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";
                        status = await InsertStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname);
                    }
                    else if (itemmaster.StcOption.Trim().ToLower() == "bookingno")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";

                        status = await InsertStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname);
                    }
                    else if (itemmaster.StcOption.Trim().ToLower() == "process")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";

                        status = await InsertStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname);
                    }
                    else if (itemmaster.StcOption.Trim().ToLower() == "garmentname")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";

                        status = await InsertStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname);
                    }
                    else if (itemmaster.StcOption.Trim().ToLower() == "customername")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";

                        status = await InsertStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname);
                    }
                    else if (itemmaster.StcOption.Trim().ToLower() == "customeraddress")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";

                        status = await InsertStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname);
                    }
                    else if (itemmaster.StcOption.Trim().ToLower() == "color")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";

                        status = await InsertStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname);
                    }
                    else if (itemmaster.StcOption.Trim().ToLower() == "remark")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";

                        status = await InsertStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname);
                    }
                    else if (itemmaster.StcOption.Trim().ToLower() == "duedate")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";

                        status = await InsertStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname);
                    }
                }
            }
            else if (StickerConfigurationDto.Operation == 2 && StickerConfigurationDto.listStickerConfigurationDto.Count > 0)
            {
                foreach (var itemmaster in StickerConfigurationDto.listStickerConfigurationDto)
                {
                    if (itemmaster.StcOption.Trim().ToLower() == "barcode")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";
                        status = await UpdateStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname, itemmaster.StcOption.Trim().ToLower());
                    }
                    else if (itemmaster.StcOption.Trim().ToLower() == "bookingno")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";
                        status = await UpdateStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname, itemmaster.StcOption.Trim().ToLower());
                    }
                    else if (itemmaster.StcOption.Trim().ToLower() == "process")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";

                        status = await UpdateStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname, itemmaster.StcOption.Trim().ToLower());
                    }
                    else if (itemmaster.StcOption.Trim().ToLower() == "garmentname")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";

                        status = await UpdateStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname, itemmaster.StcOption.Trim().ToLower());
                    }
                    else if (itemmaster.StcOption.Trim().ToLower() == "customername")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";

                        status = await UpdateStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname, itemmaster.StcOption.Trim().ToLower());
                    }
                    else if (itemmaster.StcOption.Trim().ToLower() == "customeraddress")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";

                        status = await UpdateStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname, itemmaster.StcOption.Trim().ToLower());
                    }
                    else if (itemmaster.StcOption.Trim().ToLower() == "color")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";

                        status = await UpdateStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname, itemmaster.StcOption.Trim().ToLower());
                    }
                    else if (itemmaster.StcOption.Trim().ToLower() == "remark")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";

                        status = await UpdateStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname, itemmaster.StcOption.Trim().ToLower());
                    }
                    else if (itemmaster.StcOption.Trim().ToLower() == "duedate")
                    {
                        if (itemmaster.StcBoldStyle == true)
                            itemmaster.StcTextStyle = "B";
                        else if (itemmaster.StcUnderlineStyle == true)
                            itemmaster.StcTextStyle = "U";
                        else if (itemmaster.StcItalicStyle == true)
                            itemmaster.StcTextStyle = "I";
                        status = await UpdateStickerConfiguration(itemmaster, StickerConfigurationDto.StcUsremail, StickerConfigurationDto.StcUstname, itemmaster.StcOption.Trim().ToLower());
                    }
                }
            }
            return new OperationResponse<List<StickerConfiguration>>(stickerConfigurationDetails);
        }

        public async Task<List<string>> GetAllStickerConfiConfigAsync(StickerConfigDto StickerConfigDto)
        {
            var stickerConfigurationDetails = await (_unitOfWork.GetRepository<StickerConfiguration>().Get(x => x.StcUsremail == StickerConfigDto.StcUsremail).ToListAsync());
            List<string> listresult = stickerConfigurationDetails.Select(o => o.StcUstname).Distinct().ToList();
            return listresult;
        }

        public async Task<ActionReturnType> SelectedStoreStickerConfigAsync(string userEmail, string UstStoreName)
        {
            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == userEmail).SingleOrDefaultAsync());
            if (userDetails == null)
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
            else
            {
                var resultRegStores = await (_unitOfWork.GetRepository<StickerConfiguration>().Get(x => x.StcUstname == UstStoreName).ToListAsync());

                if (resultRegStores.IsListNullOrEmpty())
                {
                    return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
                }
                else
                {
                    return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, resultRegStores, resultRegStores.Count);
                }
            }
        }
    }
}


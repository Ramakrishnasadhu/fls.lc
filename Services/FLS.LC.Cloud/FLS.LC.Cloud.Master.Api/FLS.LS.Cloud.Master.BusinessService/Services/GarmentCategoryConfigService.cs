﻿using AutoMapper;
using FLS.LC.Cloud.Master.DataModels.Entities;
using FLS.LC.Cloud.Master.DTO;
using FLS.LC.Cloud.Master.DTO.Common;
using FLS.LC.Cloud.Master.DTO.GarmentCategoryConfig;
using FLS.LC.Cloud.Master.DTO.GarmentConfig;
using FLS.LC.Cloud.Repository.UnitOfWork;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LS.Cloud.Master.BusinessService.Services
{
    public class GarmentCategoryConfigService : IGarmentCategoryConfig
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly string _timeZone;
        private readonly string _sharedSecretkey;
        public GarmentCategoryConfigService(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _configuration = configuration;
            _timeZone = _configuration.GetValue<string>("TimeZone:DatabaseTimeZone");
            _sharedSecretkey = _configuration.GetValue<string>("SharedKey:SharedSecretkey");
        }
        public async Task<OperationResponse<GarmentsCategoryDto>> GarmentCategoryConfigAsync(GarmentsCategoryDto GarmentsCategoryInput)
        {
            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == GarmentsCategoryInput.GcUsremail).SingleOrDefaultAsync());
            if (userDetails == null)
                return new OperationResponse<GarmentsCategoryDto>()
                                          .SetAsFailureResponse(OperationErrorDictionary.GarmentsListConfig.UserDoesNotExist());
            else
            {
                Users userDetailsResponseItem = _mapper.Map<Users>(userDetails);

                if (GarmentsCategoryInput.Operation == 1)//add
                {
                    var garmentCategoryConfigurationDetailsResponse = await (_unitOfWork.GetRepository<GarmentsCategory>().Get(x => x.GcGarmentCategory == GarmentsCategoryInput.GcGarmentCategory
                      && x.GcUstname == GarmentsCategoryInput.GcUstname && x.GcUsremail == GarmentsCategoryInput.GcUsremail).SingleOrDefaultAsync());

                    if (garmentCategoryConfigurationDetailsResponse != null)
                    {
                        return new OperationResponse<GarmentsCategoryDto>()
                                              .SetAsFailureResponse(OperationErrorDictionary.GarmentsCategoryConfig.GarmentCategoryAlreadyExist());
                    }
                    else
                    {
                        GarmentsCategory GarmentsCategory = new GarmentsCategory();
                        GarmentsCategory.GcUsremail = GarmentsCategoryInput.GcUsremail;
                        GarmentsCategory.GcUstname = GarmentsCategoryInput.GcUstname;
                        GarmentsCategory.GcGarmentCategory = GarmentsCategoryInput.GcGarmentCategory;

                        GarmentsCategory.CreatedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                        GarmentsCategory.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                        await _unitOfWork.GetRepository<GarmentsCategory>().InsertAsync(GarmentsCategory);
                        await _unitOfWork.SaveChangesAsync();
                        return new OperationResponse<GarmentsCategoryDto>(GarmentsCategoryInput);
                    }
                }
            }
            return null;
        }
        public async Task<ActionReturnType> RegisteredGarmentListServiceAsync(GarmentCategoryDto GarmentCategoryDto)
        {
            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == GarmentCategoryDto.GlcUsremail).SingleOrDefaultAsync());
            if (userDetails == null)
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
            else
            {
                var resultGarmentLiist = await (_unitOfWork.GetRepository<GarmentspriceConfiguration>().Get(x => x.GpcUsremail == GarmentCategoryDto.GlcUsremail && x.GpcUstname == GarmentCategoryDto.GlcUsrstore).ToListAsync());
                if (resultGarmentLiist.IsListNullOrEmpty())
                {
                    return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
                }
                else
                {
                    var finalresult = resultGarmentLiist.Select(x => x.GpcGarmentService).Distinct().ToList();
                    return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, finalresult, resultGarmentLiist.Count);
                }
            }
        }

        public async Task<ActionReturnType> RegisteredGarmentNamesListAsync(GarmentCategoryDto GarmentCategoryDto)
        {
            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == GarmentCategoryDto.GlcUsremail).SingleOrDefaultAsync());
            if (userDetails == null)
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
            else
            {
                var resultRegGarmentsCategory = await (_unitOfWork.GetRepository<GarmentspriceConfiguration>().Get(x => x.GpcUsremail == GarmentCategoryDto.GlcUsremail && x.GpcUstname == GarmentCategoryDto.GlcUsrstore).ToListAsync());

                if (resultRegGarmentsCategory.IsListNullOrEmpty())
                {
                    return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
                }
                else
                {
                    var finalresult = resultRegGarmentsCategory.Select(x => x.GpcGarmentName).Distinct().ToList();

                    return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, finalresult, finalresult.Count);
                }
            }
        }
        public async Task<ActionReturnType> RegisteredGarmentCategorysAsync(GarmentCategoryDto GarmentCategoryDto)
        {
            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == GarmentCategoryDto.GlcUsremail).SingleOrDefaultAsync());
            if (userDetails == null)
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
            else
            {
                var resultRegGarmentsCategory = await (_unitOfWork.GetRepository<GarmentspriceConfiguration>().Get(x => x.GpcUsremail == GarmentCategoryDto.GlcUsremail && x.GpcUstname == GarmentCategoryDto.GlcUsrstore).ToListAsync());

                if (resultRegGarmentsCategory.IsListNullOrEmpty())
                {
                    return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
                }
                else
                {
                    var finalresult = resultRegGarmentsCategory.Select(x => x.GpcGarmentCategory).Distinct().ToList();

                    return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, finalresult, finalresult.Count);
                }
            }
        }

        public async Task<OperationResponse<GarmentspriceConfigurationReadDto>> GetRequiredGarmentAsync(RequiredGarmentDto requiredGarmentDto)
        {
            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == requiredGarmentDto.GlcUsremail).SingleOrDefaultAsync());
            if (userDetails == null)
                return new OperationResponse<GarmentspriceConfigurationReadDto>()
                                          .SetAsFailureResponse(OperationErrorDictionary.GarmentsCategoryConfig.UserDoesNotExist());
            else
            {
                var resultrequestedGarments = await (_unitOfWork.GetRepository<GarmentspriceConfiguration>()
                    .Get(x => x.GpcUsremail == requiredGarmentDto.GlcUsremail && x.GpcUstname == requiredGarmentDto.GlcUsrstore
                    && x.GpcGarmentName == requiredGarmentDto.GlcGarmentName
                    && x.GpcGarmentCategory== requiredGarmentDto.GlcSelectedGarment && x.GpcGarmentService == requiredGarmentDto.GlcSelectedService).FirstOrDefaultAsync());

                if (resultrequestedGarments == null)
                {
                    return new OperationResponse<GarmentspriceConfigurationReadDto>()
                                          .SetAsFailureResponse(OperationErrorDictionary.GarmentsCategoryConfig.GarmentCategoryNotExist());
                }
                else
                {
                    var result= _mapper.Map<GarmentspriceConfigurationReadDto>(resultrequestedGarments);
                    return new OperationResponse<GarmentspriceConfigurationReadDto>(result);
                }
            }
        }
    }
}

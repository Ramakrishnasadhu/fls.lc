﻿using AutoMapper;
using ExcelDataReader;
using FLS.LC.Cloud.Master.DataModels.Entities;
using FLS.LC.Cloud.Master.DTO;
using FLS.LC.Cloud.Master.DTO.Common;
using FLS.LC.Cloud.Master.DTO.GarmentConfig;
using FLS.LC.Cloud.Repository.UnitOfWork;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static FLS.LC.Cloud.Master.DTO.Common.OperationErrorDictionary;
using static System.Net.WebRequestMethods;

namespace FLS.LS.Cloud.Master.BusinessService.Services
{
    public class GarmentConfigService : IGarmentConfig
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly string _timeZone;
        private readonly string _sharedSecretkey;
        private readonly IHostingEnvironment _hostingEnv;
        private const string GarmentsImageFolder = "Garments";

        public GarmentConfigService(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration, IHostingEnvironment hostingEnv)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _configuration = configuration;
            _timeZone = _configuration.GetValue<string>("TimeZone:DatabaseTimeZone");
            _sharedSecretkey = _configuration.GetValue<string>("SharedKey:SharedSecretkey");
            _hostingEnv = hostingEnv;
        }
        public async Task<OperationResponse<GarmentslistConfigurationDto>> UpdateGarmentConfigAsync(GarmentslistConfigurationDto GarmentslistConfigurationDto)
        {

            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == GarmentslistConfigurationDto.GpcUsremail).SingleOrDefaultAsync());
            if (userDetails == null)
                return new OperationResponse<GarmentslistConfigurationDto>()
                                          .SetAsFailureResponse(OperationErrorDictionary.GarmentsListConfig.UserDoesNotExist());
            else
            {
                Users userDetailsResponseItem = _mapper.Map<Users>(userDetails);

                var garmentListConfigurationDetailsResponse = await (_unitOfWork.GetRepository<GarmentspriceConfiguration>().Get(x => x.GpcGarmentName == GarmentslistConfigurationDto.GpcGarmentName
                 && x.GpcGarmentService == GarmentslistConfigurationDto.GlcGarmentServiceName
                 && x.GpcGarmentCategory == GarmentslistConfigurationDto.GpcGarmentCategory
                 && x.GpcUsremail == GarmentslistConfigurationDto.GpcUsremail
                 && x.GpcUstname == GarmentslistConfigurationDto.GpcUstname).FirstOrDefaultAsync());

                if (garmentListConfigurationDetailsResponse != null)
                {
                    //   GarmentspriceConfiguration GarmentslistConfiguration = new GarmentspriceConfiguration();
                    garmentListConfigurationDetailsResponse.GpcGarmentCode = GarmentslistConfigurationDto.GpcGarmentCode;
                    MemoryStream ms = new MemoryStream();
                    GarmentslistConfigurationDto.GlcGarmentImage.CopyTo(ms);
                    garmentListConfigurationDetailsResponse.GpcImage = ms.ToArray();
                    garmentListConfigurationDetailsResponse.GpcGarmentMeasurementUnit = GarmentslistConfigurationDto.GpcGarmentMeasurementUnit;
                    garmentListConfigurationDetailsResponse.GpcGarmentCategory = GarmentslistConfigurationDto.GpcGarmentCategory;
                    garmentListConfigurationDetailsResponse.GpcNoOfGarments = GarmentslistConfigurationDto.GpcNoOfGarments;
                    garmentListConfigurationDetailsResponse.GpcIsenable = GarmentslistConfigurationDto.GpcIsenable;
                    garmentListConfigurationDetailsResponse.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                    _unitOfWork.GetRepository<GarmentspriceConfiguration>().Update(garmentListConfigurationDetailsResponse);
                    await _unitOfWork.SaveChangesAsync();
                    return new OperationResponse<GarmentslistConfigurationDto>(GarmentslistConfigurationDto);
                }
                else
                {
                    return new OperationResponse<GarmentslistConfigurationDto>()
                                              .SetAsFailureResponse(OperationErrorDictionary.GarmentsListConfig.GarmentConfigurationNotExist());
                }

            }
        }
        public async Task<ActionReturnType> GetRegisteredPriceListByStoreAsync(GarmentListDto GarmentListDto)
        {
            List<string> listRegisteredPriceListNames = new List<string>();

            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == GarmentListDto.GlcUsremail).SingleOrDefaultAsync());
            if (userDetails == null)
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
            else
            {
                var resultRegisteredPriceList = await (_unitOfWork.GetRepository<GarmentspriceConfiguration>().Get(x => x.GpcUsremail == GarmentListDto.GlcUsremail 
                          && x.GpcUstname == GarmentListDto.GlcUsrstore).FirstOrDefaultAsync());
                if (resultRegisteredPriceList != null)
                    listRegisteredPriceListNames.Add(resultRegisteredPriceList.GpcPriceListName.ToString());
                else
                    listRegisteredPriceListNames.Add("No PriceList");
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, listRegisteredPriceListNames);
            }
        }

        public async Task<ActionReturnType> updateRegisteredPriceListByStoreAsync(updateRegisteredPriceListByStoreDto updateRegisteredPriceListByStoreDto)
        {
            List<string> listRegisteredPriceListNames = new List<string>();

            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == updateRegisteredPriceListByStoreDto.usrEmail).SingleOrDefaultAsync());
            if (userDetails == null)
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
            else
            {
                //var resultGarmentPriceLists = await (_unitOfWork.GetRepository<GarmentspriceConfiguration>().Get(x => x.GpcUsremail == updateRegisteredPriceListByStoreDto.usrEmail
                //&& x.GpcUstname == updateRegisteredPriceListByStoreDto.usrStoreName
                //&& x.GpcGarmentServicePrice>0 ).ToListAsync());
                //if (resultGarmentPriceLists.Count > 0)
                //{
                //    if(updateRegisteredPriceListByStoreDto.isIncrementChecked==true)
                //        resultGarmentPriceLists.ForEach(m => m.GpcGarmentServicePrice = m.GpcGarmentServicePrice + (m.GpcGarmentServicePrice * Convert.ToInt32(updateRegisteredPriceListByStoreDto.pricePercentage)/100));
                //    if (updateRegisteredPriceListByStoreDto.isDecrementChecked == true)
                //        resultGarmentPriceLists.ForEach(m => m.GpcGarmentServicePrice = m.GpcGarmentServicePrice - (m.GpcGarmentServicePrice * Convert.ToInt32(updateRegisteredPriceListByStoreDto.pricePercentage) / 100));
                //}


                //_unitOfWork.GetRepository<GarmentspriceConfiguration>().Update(resultGarmentPriceLists);
                //await _unitOfWork.SaveChangesAsync();

                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, updateRegisteredPriceListByStoreDto);
            }
        }

        public async Task<ActionReturnType> UpdateServicePriceByGarmentAsync(UpdateServicePriceByGarmentDto updateServicePriceByGarmentDto)
        {
            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == updateServicePriceByGarmentDto.UsrEmail).SingleOrDefaultAsync());
            if (userDetails == null)
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
            else
            {
                var resultGarment = await (_unitOfWork.GetRepository<GarmentspriceConfiguration>()
                    .Get(x => x.GpcUsremail == updateServicePriceByGarmentDto.UsrEmail
                && x.GpcUstname == updateServicePriceByGarmentDto.UsrStoreName 
                && x.GpcGarmentName == updateServicePriceByGarmentDto.Garmentname
                && x.GpcGarmentService == updateServicePriceByGarmentDto.Service
                && x.GpcGarmentCategory == updateServicePriceByGarmentDto.Garment).FirstOrDefaultAsync());
                if (resultGarment != null)
                {
                    resultGarment.GpcGarmentServicePrice = updateServicePriceByGarmentDto.Updatedprice;
                    _unitOfWork.GetRepository<GarmentspriceConfiguration>().Update(resultGarment);
                    await _unitOfWork.SaveChangesAsync();
                }
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, updateServicePriceByGarmentDto);
            }
        }

        public async Task<ActionReturnType> GetAllRegisteredGarmentsListAsync(GarmentListDto GarmentListDto)
        {
            var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == GarmentListDto.GlcUsremail).SingleOrDefaultAsync());
            if (userDetails == null)
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
            else
            {
                var resultRegGarments = await (_unitOfWork.GetRepository<GarmentspriceConfiguration>().Get(x => x.GpcUsremail == GarmentListDto.GlcUsremail 
                && x.GpcUstname== GarmentListDto.GlcUsrstore).ToListAsync());

                if (resultRegGarments.IsListNullOrEmpty())
                {
                    return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
                }
                else
                {
                    return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, resultRegGarments, resultRegGarments.Count);
                }
            }
        }

        public async Task<OperationResponse<GarmentspriceConfigurationDto>> ImportGarmentsPriceListAsync(GarmentspriceConfigurationDto GarmentspriceConfigurationDto)
        {
            DataSet dsexcelRecords = new DataSet();
            IExcelDataReader reader = null;
            var streampricelist = GarmentspriceConfigurationDto.GpcImprtPriceList.OpenReadStream();
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            reader = ExcelReaderFactory.CreateBinaryReader(streampricelist);
            dsexcelRecords = reader.AsDataSet();
            reader.Close();
            string wwwRootPath = _hostingEnv.WebRootPath + @"\images\Garments\";
            if (dsexcelRecords != null && dsexcelRecords.Tables.Count > 0)
            {
                DataTable dtGarmentRecords = dsexcelRecords.Tables[0];
                for (int i = 1; i < dtGarmentRecords.Rows.Count; i++)
                {
                    int k = 0;
                    for (; k < dtGarmentRecords.Columns.Count;)
                    {
                        for (int j = 0; j < 5; j++)
                        {
                            int l = 0;
                            GarmentspriceConfiguration garmentspriceConfiguration = new GarmentspriceConfiguration();
                            garmentspriceConfiguration.GpcUsremail = GarmentspriceConfigurationDto.GpcUsremail;
                            garmentspriceConfiguration.GpcUstname = GarmentspriceConfigurationDto.GpcUstname;
                            garmentspriceConfiguration.GpcImage = null;
                            garmentspriceConfiguration.GpcPriceListName = GarmentspriceConfigurationDto.GpcUstname + "PriceList";
                            garmentspriceConfiguration.GpcIsenable = Convert.ToBoolean(dtGarmentRecords.Rows[i][l]);
                            l++;
                            garmentspriceConfiguration.GpcGarmentCategory = Convert.ToString(dtGarmentRecords.Rows[i][l]);
                            l++;
                            string imagePathWithExtension = Regex.Replace((Convert.ToString(dtGarmentRecords.Rows[i][l]) + ".png"), @"\s+", "");
                            garmentspriceConfiguration.GpcGarmentName = Regex.Replace((Convert.ToString(dtGarmentRecords.Rows[i][l])), @"\s+", "");
                            byte[] imageBytes;
                            if (!System.IO.File.Exists(wwwRootPath + imagePathWithExtension))
                                imageBytes = System.IO.File.ReadAllBytes(wwwRootPath + "no-image-icon.jpg");
                            else
                                imageBytes = System.IO.File.ReadAllBytes(wwwRootPath + imagePathWithExtension);
                            garmentspriceConfiguration.GpcImage = imageBytes;
                            l++;
                            garmentspriceConfiguration.GpcGarmentCode = Convert.ToString(dtGarmentRecords.Rows[i][l]);
                            l++;
                            if (k == 0)
                                k = k + l;
                            else
                                k++;
                            if (k == 16)
                                break;
                            garmentspriceConfiguration.GpcGarmentService = Convert.ToString(dtGarmentRecords.Rows[0][k]);
                            l++;
                            garmentspriceConfiguration.GpcGarmentServicePrice = Convert.ToInt32(dtGarmentRecords.Rows[i][k]);
                            garmentspriceConfiguration.CreatedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                            garmentspriceConfiguration.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                            await _unitOfWork.GetRepository<GarmentspriceConfiguration>().InsertAsync(garmentspriceConfiguration);
                            await _unitOfWork.SaveChangesAsync();
                        }
                    }
                }
            }
            return new OperationResponse<GarmentspriceConfigurationDto>(GarmentspriceConfigurationDto);
        }
    }
}

﻿using AutoMapper;
using FLS.LC.Cloud.Master.DataModels.Entities;
using FLS.LC.Cloud.Master.DTO.Common;
using FLS.LC.Cloud.Master.DTO.StoreHoliday;
using FLS.LC.Cloud.Repository.UnitOfWork;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LS.Cloud.Master.BusinessService.Services
{
    public class StoreHolidayService : IStoreHoliday
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly string _timeZone;
        private readonly string _sharedSecretkey;
        private readonly IHostingEnvironment _hostingEnv;
        public StoreHolidayService(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration, IHostingEnvironment hostingEnv)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _configuration = configuration;
            _timeZone = _configuration.GetValue<string>("TimeZone:DatabaseTimeZone");
            _sharedSecretkey = _configuration.GetValue<string>("SharedKey:SharedSecretkey");
            _hostingEnv = hostingEnv;
        }
        public async Task<ActionReturnType> GetRegisteredStoreHolidayAsync(string userEmail)
        {

            var resultRegStoreHolidays = await (_unitOfWork.GetRepository<UserStoreHoliday>().Get(x => x.UsthUsremail == userEmail
              ).ToListAsync());
            var result = _mapper.Map<List<StoreHolidayResponseDto>>(resultRegStoreHolidays);

            if (result.IsListNullOrEmpty())
            {
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
            }
            else
            {

                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, result, result.Count);
            }
        }

        public async Task<ActionReturnType> GetRegisteredStoreHolidayBySelectedStoreAsync(string userEmail,string userStore)
        {

            var resultRegStoreHolidays = await (_unitOfWork.GetRepository<UserStoreHoliday>().Get(x => x.UsthUsremail == userEmail
              && x.UsthUstname == userStore).ToListAsync());
            var result = _mapper.Map<List<StoreHolidayResponseDto>>(resultRegStoreHolidays);

            if (result.IsListNullOrEmpty())
            {
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
            }
            else
            {

                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, result, result.Count);
            }
        }


        public async Task<OperationResponse<UserStoreHoliday>> AddStoreHolidayAsync(StoreHolidayRequestDto StoreHolidayRequestDto)
        {
            var storeHolidayResponse = await (_unitOfWork.GetRepository<UserStoreHoliday>().Get(x => x.UsthUsremail == StoreHolidayRequestDto.UsthUsremail
            && x.UsthUstname == StoreHolidayRequestDto.UsthUstname && x.UsthHolidayDescription == StoreHolidayRequestDto.UsthHolidayDescription).FirstOrDefaultAsync());

            if (storeHolidayResponse == null)
            {
                UserStoreHoliday userStoreHoliday = new UserStoreHoliday();
                userStoreHoliday.UsthUsremail = StoreHolidayRequestDto.UsthUsremail;
                userStoreHoliday.UsthUstname = StoreHolidayRequestDto.UsthUstname;
                userStoreHoliday.UsthHolidayDescription = StoreHolidayRequestDto.UsthHolidayDescription;
                DateTime dateAndTime = Convert.ToDateTime(StoreHolidayRequestDto.UsthHolidayDate);
                userStoreHoliday.UsthHolidayDate = dateAndTime.ToString("MM-dd-yyyy");
                userStoreHoliday.CreatedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                userStoreHoliday.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                await _unitOfWork.GetRepository<UserStoreHoliday>().InsertAsync(userStoreHoliday);
                await _unitOfWork.SaveChangesAsync();
                return new OperationResponse<UserStoreHoliday>(userStoreHoliday);
            }
            else if (storeHolidayResponse != null)
            {
                storeHolidayResponse.UsthHolidayDescription = StoreHolidayRequestDto.UsthHolidayDescription;
                storeHolidayResponse.UsthHolidayDate = (StoreHolidayRequestDto.UsthHolidayDate).ToString("MM-DD-YYYY");
                storeHolidayResponse.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                _unitOfWork.GetRepository<UserStoreHoliday>().Update(storeHolidayResponse);
                await _unitOfWork.SaveChangesAsync();
                return new OperationResponse<UserStoreHoliday>(storeHolidayResponse);
            }
            return new OperationResponse<UserStoreHoliday>(storeHolidayResponse);
        }

        public async Task<OperationResponse<UserStoreWeekOffHoliday>> AddStoreWeekOffHolidayAsync(StoreWeekOffHolidayRequestDto storeWeekOffHolidayRequestDto)
        {
            var storeWeekOffHolidayResponse = await (_unitOfWork.GetRepository<UserStoreWeekOffHoliday>().Get(x => x.UstUsremail == storeWeekOffHolidayRequestDto.UsthUsremail
            && x.UstUstname == storeWeekOffHolidayRequestDto.UsthUstname).FirstOrDefaultAsync());

            if (storeWeekOffHolidayResponse == null)
            {
                UserStoreWeekOffHoliday userStoreWeekOffHoliday = new UserStoreWeekOffHoliday();
                userStoreWeekOffHoliday.UstUsremail = storeWeekOffHolidayRequestDto.UsthUsremail;
                userStoreWeekOffHoliday.UstUstname = storeWeekOffHolidayRequestDto.UsthUstname;
                userStoreWeekOffHoliday.UstWeekOffDay = storeWeekOffHolidayRequestDto.UsthWeekOffHoliday;
                userStoreWeekOffHoliday.CreatedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                userStoreWeekOffHoliday.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                await _unitOfWork.GetRepository<UserStoreWeekOffHoliday>().InsertAsync(userStoreWeekOffHoliday);
                await _unitOfWork.SaveChangesAsync();
                return new OperationResponse<UserStoreWeekOffHoliday>(userStoreWeekOffHoliday);
            }
            else if (storeWeekOffHolidayResponse != null)
            {
                storeWeekOffHolidayResponse.UstWeekOffDay = storeWeekOffHolidayRequestDto.UsthWeekOffHoliday;
                storeWeekOffHolidayResponse.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                _unitOfWork.GetRepository<UserStoreWeekOffHoliday>().Update(storeWeekOffHolidayResponse);
                await _unitOfWork.SaveChangesAsync();
                return new OperationResponse<UserStoreWeekOffHoliday>(storeWeekOffHolidayResponse);
            }
            return new OperationResponse<UserStoreWeekOffHoliday>(storeWeekOffHolidayResponse);
        }
        public async Task<ActionReturnType> GetStoreWeekOffHolidayAsync(string userEmail, string userStore)
        {

            var resultRegStoreHolidayWeekOff = await (_unitOfWork.GetRepository<UserStoreWeekOffHoliday>().Get(x => x.UstUsremail == userEmail
              && x.UstUstname == userStore).FirstOrDefaultAsync());
            var result = _mapper.Map<StoreHolidayWeekOffResponseDto>(resultRegStoreHolidayWeekOff);

            if (result==null)
            {
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
            }
            else
            {

                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, result);
            }
        }
    }
}

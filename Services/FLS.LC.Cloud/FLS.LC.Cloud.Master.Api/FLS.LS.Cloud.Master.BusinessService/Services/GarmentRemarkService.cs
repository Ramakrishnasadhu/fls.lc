﻿using AutoMapper;
using FLS.LC.Cloud.Master.DataModels.Entities;
using FLS.LC.Cloud.Master.DTO.Common;
using FLS.LC.Cloud.Master.DTO.GarmentRemark;
using FLS.LC.Cloud.Repository.UnitOfWork;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FLS.LS.Cloud.Master.BusinessService.Services
{
    public class GarmentRemarkService : IGarmentRemark
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly string _timeZone;
        private readonly string _sharedSecretkey;
        private readonly IHostingEnvironment _hostingEnv;

        public GarmentRemarkService(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration, IHostingEnvironment hostingEnv)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _configuration = configuration;
            _timeZone = _configuration.GetValue<string>("TimeZone:DatabaseTimeZone");
            _sharedSecretkey = _configuration.GetValue<string>("SharedKey:SharedSecretkey");
            _hostingEnv = hostingEnv;
        }

        public async Task<OperationResponse<GarmentsRemark>> InactivateGarmentRemarkAsync(GarmentRemarkRequestDto garmentRemarkRequestDto)
        {
            var garmentRemarksResponse = await (_unitOfWork.GetRepository<GarmentsRemark>().Get(x => x.GreRemarkDescription == garmentRemarkRequestDto.GreRemarkDescription).FirstOrDefaultAsync());

            if (garmentRemarksResponse != null)
            {
                garmentRemarksResponse.GreIsActive = false;
                _unitOfWork.GetRepository<GarmentsRemark>().Update(garmentRemarksResponse);
                await _unitOfWork.SaveChangesAsync();
                return new OperationResponse<GarmentsRemark>(garmentRemarksResponse);
            }
            else
            {
                return new OperationResponse<GarmentsRemark>(garmentRemarksResponse);
            }
        }
        public async Task<OperationResponse<GarmentsRemark>> AddGarmentRemarkAsync(GarmentRemarkPostRequestDto garmentRemarkPostRequestDto)
        {
            var garmentRemarksResponse = await (_unitOfWork.GetRepository<GarmentsRemark>().Get(x => x.GreRemarkDescription == garmentRemarkPostRequestDto.garmentRemarkDescriptionOldValue 
            && x.GreId == garmentRemarkPostRequestDto.garmentRemarkId).FirstOrDefaultAsync());

            if (garmentRemarksResponse == null)
            {
                GarmentsRemark garmentsRemark = new GarmentsRemark();
                garmentsRemark.GreRemarkDescription = garmentRemarkPostRequestDto.garmentRemarkDescriptionNewValue;
                garmentsRemark.GreIsActive = true;
                garmentsRemark.CreatedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                garmentsRemark.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                await _unitOfWork.GetRepository<GarmentsRemark>().InsertAsync(garmentsRemark);
                await _unitOfWork.SaveChangesAsync();
                return new OperationResponse<GarmentsRemark>(garmentsRemark);
            }
            else if (garmentRemarksResponse != null)
            {
                garmentRemarksResponse.GreRemarkDescription = garmentRemarkPostRequestDto.garmentRemarkDescriptionNewValue;
                garmentRemarksResponse.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
                _unitOfWork.GetRepository<GarmentsRemark>().Update(garmentRemarksResponse);
                await _unitOfWork.SaveChangesAsync();
                return new OperationResponse<GarmentsRemark>(garmentRemarksResponse);
            }
            return new OperationResponse<GarmentsRemark>(garmentRemarksResponse);
        }
        public async Task<ActionReturnType> GetRegisteredGarmentRemarkAsync()
        {

            var resultRegGarmentsRemarks = await (_unitOfWork.GetRepository<GarmentsRemark>().Get().ToListAsync());
            var result = _mapper.Map<List<GarmentRemarkRequestDto>>(resultRegGarmentsRemarks);

            if (resultRegGarmentsRemarks.IsListNullOrEmpty())
            {
                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
            }
            else
            {

                return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, result, result.Count);
            }
        }
    }
}

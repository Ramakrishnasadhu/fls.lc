﻿using AutoMapper;
using FLS.LC.Cloud.Master.DataModels.Entities;
using FLS.LC.Cloud.Master.DTO;
using FLS.LC.Cloud.Master.DTO.Common;
using FLS.LC.Cloud.Master.DTO.OrderConfig;
using FLS.LC.Cloud.Repository.UnitOfWork;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
namespace FLS.LS.Cloud.Master.BusinessService.Services
{
    public class OrderConfigService : IOrderConfig
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly string _timeZone;
        private readonly string _sharedSecretkey;
        public OrderConfigService(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _configuration = configuration;
            _timeZone = _configuration.GetValue<string>("TimeZone:DatabaseTimeZone");
            _sharedSecretkey = _configuration.GetValue<string>("SharedKey:SharedSecretkey");
        }
        //public async Task<ActionReturnType> GetAllStoreOrderConfigAsync(string userEmail)
        //{
        //    var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == userEmail).SingleOrDefaultAsync());
        //    if (userDetails == null)
        //        return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
        //    else
        //    {
        //        var orderScreenConfigurationDetailsResponse = await (_unitOfWork.GetRepository<OrderScreenConfiguration>().Get(x => x.UsrId == userDetails.UsrId
        //              ).ToListAsync());
        //        if (orderScreenConfigurationDetailsResponse != null)
        //        {
        //            var OrderScreenConfiguration = _mapper.Map<List<OrderScreenConfigurationDto>>(orderScreenConfigurationDetailsResponse);
        //            return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, OrderScreenConfiguration);
        //        }
        //        else
        //        {
        //            return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
        //        }
        //    }
        //}

        //public async Task<ActionReturnType> SelectedStoreOrderConfigDetailsAsync(string userEmail, string UstStoreName)
        //{
        //    var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == userEmail).SingleOrDefaultAsync());
        //    if (userDetails == null)
        //        return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
        //    else
        //    {
        //        var resultRegStoreConfigDetails = await (_unitOfWork.GetRepository<OrderScreenConfiguration>().Get(x => x.UsrId == userDetails.UsrId && x.OscUstName == UstStoreName).ToListAsync());

        //        if (resultRegStoreConfigDetails.IsListNullOrEmpty())
        //        {
        //            return ActionSet.ActionReturnType(System.Net.HttpStatusCode.NoContent);
        //        }
        //        else
        //        {
        //            return ActionSet.ActionReturnType(System.Net.HttpStatusCode.OK, resultRegStoreConfigDetails, resultRegStoreConfigDetails.Count);
        //        }
        //    }
        //}
        //public async Task<OperationResponse<OrderConfigDto>> OrderConfigOperationAsync(OrderConfigDto OrderConfigDto)
        //{
        //    var userDetails = await (_unitOfWork.GetRepository<Users>().Get(x => x.UsrUserEmail == OrderConfigDto.UserEmail).SingleOrDefaultAsync());
        //    if (userDetails == null)
        //        return new OperationResponse<OrderConfigDto>()
        //                                  .SetAsFailureResponse(OperationErrorDictionary.OrderConfig.UserDoesNotExist());
        //    else
        //    {
        //        Users userDetailsResponseItem = _mapper.Map<Users>(userDetails);

        //        if (OrderConfigDto.Operation == 1)//add
        //        {
        //            var orderScreenConfigurationDetailsResponse = await (_unitOfWork.GetRepository<OrderScreenConfiguration>().Get(x => x.UsrId == userDetailsResponseItem.UsrId
        //              && x.OscUstName == OrderConfigDto.OrderScreenConfigurationDto.OscUstName).SingleOrDefaultAsync());

        //            if (orderScreenConfigurationDetailsResponse != null)
        //            {
        //                return new OperationResponse<OrderConfigDto>()
        //                                      .SetAsFailureResponse(OperationErrorDictionary.OrderConfig.OrderConfigurationStoreAlreadyExist());
        //            }
        //            else
        //            {
        //                //OrderScreenConfiguration OrderScreenConfiguration = new OrderScreenConfiguration();
        //                //OrderScreenConfiguration.OscUsrEmail = OrderConfigDto.UserEmail;
        //                //OrderScreenConfiguration.UsrId = userDetailsResponseItem.UsrId;
        //                //OrderScreenConfiguration.OscUstName = OrderConfigDto.OrderScreenConfigurationDto.OscUstName;
        //                //OrderScreenConfiguration.OscStartOrderFromPrefix = OrderConfigDto.OrderScreenConfigurationDto.OscStartOrderFromPrefix;
        //                //OrderScreenConfiguration.OscStartOrderFromPostfix = OrderConfigDto.OrderScreenConfigurationDto.OscStartOrderFromPostfix;
        //                //OrderScreenConfiguration.OscDeliveryDateOffset = OrderConfigDto.OrderScreenConfigurationDto.OscDeliveryDateOffset;
        //                //OrderScreenConfiguration.OscDeliveryTimePrefix = OrderConfigDto.OrderScreenConfigurationDto.OscDeliveryTimePrefix;
        //                //OrderScreenConfiguration.OscDeliveryTimePostfix = OrderConfigDto.OrderScreenConfigurationDto.OscDeliveryTimePostfix;
        //                //OrderScreenConfiguration.OscOrderDefaultSearch = OrderConfigDto.OrderScreenConfigurationDto.OscOrderDefaultSearch;
        //                //OrderScreenConfiguration.OscDiscountType = OrderConfigDto.OrderScreenConfigurationDto.OscDiscountType;
        //                //OrderScreenConfiguration.OscOrderNetamountType = OrderConfigDto.OrderScreenConfigurationDto.OscOrderNetamountType;
        //                //OrderScreenConfiguration.OscUrgentSameDayOrderRate = OrderConfigDto.OrderScreenConfigurationDto.OscUrgentSameDayOrderRate;
        //                //OrderScreenConfiguration.OscUrgentSameDayOrderDayOffset = OrderConfigDto.OrderScreenConfigurationDto.OscUrgentSameDayOrderDayOffset;
        //                //OrderScreenConfiguration.OscUrgentNextDayOrderRate = OrderConfigDto.OrderScreenConfigurationDto.OscUrgentNextDayOrderRate;
        //                //OrderScreenConfiguration.OscUrgentNextDayOrderDayOffset = OrderConfigDto.OrderScreenConfigurationDto.OscUrgentNextDayOrderDayOffset;
        //                //OrderScreenConfiguration.OscRule1 = OrderConfigDto.OrderScreenConfigurationDto.OscRule1;
        //                //OrderScreenConfiguration.OscRule2 = OrderConfigDto.OrderScreenConfigurationDto.OscRule2;
        //                //OrderScreenConfiguration.OscRule3 = OrderConfigDto.OrderScreenConfigurationDto.OscRule3;
        //                //OrderScreenConfiguration.OscRule4 = OrderConfigDto.OrderScreenConfigurationDto.OscRule4;
        //                //OrderScreenConfiguration.OscRule5 = OrderConfigDto.OrderScreenConfigurationDto.OscRule5;
        //                //OrderScreenConfiguration.OscRule6 = OrderConfigDto.OrderScreenConfigurationDto.OscRule6;
        //                //OrderScreenConfiguration.OscRule7 = OrderConfigDto.OrderScreenConfigurationDto.OscRule7;
        //                //OrderScreenConfiguration.OscRule8 = OrderConfigDto.OrderScreenConfigurationDto.OscRule8;
        //                //OrderScreenConfiguration.OscRule9 = OrderConfigDto.OrderScreenConfigurationDto.OscRule9;
        //                //OrderScreenConfiguration.OscRule10 = OrderConfigDto.OrderScreenConfigurationDto.OscRule10;
        //                //OrderScreenConfiguration.OscRule11 = OrderConfigDto.OrderScreenConfigurationDto.OscRule11;
        //                //OrderScreenConfiguration.OscRule12 = OrderConfigDto.OrderScreenConfigurationDto.OscRule12;
        //                //OrderScreenConfiguration.OscRule13 = OrderConfigDto.OrderScreenConfigurationDto.OscRule13;
        //                //OrderScreenConfiguration.CreatedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
        //                //OrderScreenConfiguration.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
        //                //await _unitOfWork.GetRepository<OrderScreenConfiguration>().InsertAsync(OrderScreenConfiguration);
        //                //await _unitOfWork.SaveChangesAsync();
        //                return new OperationResponse<OrderConfigDto>(OrderConfigDto);
        //            }
        //        }
        //        else if (OrderConfigDto.Operation == 2)//update
        //        {
        //            //var orderScreenConfigurationDetailsResponse = await (_unitOfWork.GetRepository<OrderScreenConfiguration>().Get(x => x.UsrId == userDetailsResponseItem.UsrId
        //            //  && x.OscUstName == OrderConfigDto.OrderScreenConfigurationDto.OscUstName).SingleOrDefaultAsync());

        //            //if (orderScreenConfigurationDetailsResponse != null)
        //            //{

        //            //    orderScreenConfigurationDetailsResponse.OscUsrEmail = OrderConfigDto.UserEmail;
        //            //    orderScreenConfigurationDetailsResponse.UsrId = userDetailsResponseItem.UsrId;
        //            //    orderScreenConfigurationDetailsResponse.OscUstName = OrderConfigDto.OrderScreenConfigurationDto.OscUstName;
        //            //    orderScreenConfigurationDetailsResponse.OscStartOrderFromPrefix = OrderConfigDto.OrderScreenConfigurationDto.OscStartOrderFromPrefix;
        //            //    orderScreenConfigurationDetailsResponse.OscStartOrderFromPostfix = OrderConfigDto.OrderScreenConfigurationDto.OscStartOrderFromPostfix;
        //            //    orderScreenConfigurationDetailsResponse.OscDeliveryDateOffset = OrderConfigDto.OrderScreenConfigurationDto.OscDeliveryDateOffset;
        //            //    orderScreenConfigurationDetailsResponse.OscDeliveryTimePrefix = OrderConfigDto.OrderScreenConfigurationDto.OscDeliveryTimePrefix;
        //            //    orderScreenConfigurationDetailsResponse.OscDeliveryTimePostfix = OrderConfigDto.OrderScreenConfigurationDto.OscDeliveryTimePostfix;
        //            //    orderScreenConfigurationDetailsResponse.OscOrderDefaultSearch = OrderConfigDto.OrderScreenConfigurationDto.OscOrderDefaultSearch;
        //            //    orderScreenConfigurationDetailsResponse.OscDiscountType = OrderConfigDto.OrderScreenConfigurationDto.OscDiscountType;
        //            //    orderScreenConfigurationDetailsResponse.OscOrderNetamountType = OrderConfigDto.OrderScreenConfigurationDto.OscOrderNetamountType;
        //            //    orderScreenConfigurationDetailsResponse.OscUrgentSameDayOrderRate = OrderConfigDto.OrderScreenConfigurationDto.OscUrgentSameDayOrderRate;
        //            //    orderScreenConfigurationDetailsResponse.OscUrgentSameDayOrderDayOffset = OrderConfigDto.OrderScreenConfigurationDto.OscUrgentSameDayOrderDayOffset;
        //            //    orderScreenConfigurationDetailsResponse.OscUrgentNextDayOrderRate = OrderConfigDto.OrderScreenConfigurationDto.OscUrgentNextDayOrderRate;
        //            //    orderScreenConfigurationDetailsResponse.OscUrgentNextDayOrderDayOffset = OrderConfigDto.OrderScreenConfigurationDto.OscUrgentNextDayOrderDayOffset;
        //            //    orderScreenConfigurationDetailsResponse.OscRule1 = OrderConfigDto.OrderScreenConfigurationDto.OscRule1;
        //            //    orderScreenConfigurationDetailsResponse.OscRule2 = OrderConfigDto.OrderScreenConfigurationDto.OscRule2;
        //            //    orderScreenConfigurationDetailsResponse.OscRule3 = OrderConfigDto.OrderScreenConfigurationDto.OscRule3;
        //            //    orderScreenConfigurationDetailsResponse.OscRule4 = OrderConfigDto.OrderScreenConfigurationDto.OscRule4;
        //            //    orderScreenConfigurationDetailsResponse.OscRule5 = OrderConfigDto.OrderScreenConfigurationDto.OscRule5;
        //            //    orderScreenConfigurationDetailsResponse.OscRule6 = OrderConfigDto.OrderScreenConfigurationDto.OscRule6;
        //            //    orderScreenConfigurationDetailsResponse.OscRule7 = OrderConfigDto.OrderScreenConfigurationDto.OscRule7;
        //            //    orderScreenConfigurationDetailsResponse.OscRule8 = OrderConfigDto.OrderScreenConfigurationDto.OscRule8;
        //            //    orderScreenConfigurationDetailsResponse.OscRule9 = OrderConfigDto.OrderScreenConfigurationDto.OscRule9;
        //            //    orderScreenConfigurationDetailsResponse.OscRule10 = OrderConfigDto.OrderScreenConfigurationDto.OscRule10;
        //            //    orderScreenConfigurationDetailsResponse.OscRule11 = OrderConfigDto.OrderScreenConfigurationDto.OscRule11;
        //            //    orderScreenConfigurationDetailsResponse.OscRule12 = OrderConfigDto.OrderScreenConfigurationDto.OscRule12;
        //            //    orderScreenConfigurationDetailsResponse.OscRule13 = OrderConfigDto.OrderScreenConfigurationDto.OscRule13;
        //            //    orderScreenConfigurationDetailsResponse.CreatedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
        //            //    orderScreenConfigurationDetailsResponse.ModifiedOn = DateTime.Now.ToSystemTimeFromUTC(_timeZone);
        //            //    _unitOfWork.GetRepository<OrderScreenConfiguration>().Update(orderScreenConfigurationDetailsResponse);
        //            //    await _unitOfWork.SaveChangesAsync();
        //                return new OperationResponse<OrderConfigDto>(OrderConfigDto);
        //            }
        //        } //edit
        //        {

        //        }
        //    }
        //   // return null;
        //}
    }
}

﻿using AutoMapper;
using FLS.LC.Cloud.Master.DataModels.Entities;
using FLS.LC.Cloud.Master.DTO;
using FLS.LC.Cloud.Master.DTO.GarmentCategoryConfig;
using FLS.LC.Cloud.Master.DTO.GarmentCheckingStaff;
using FLS.LC.Cloud.Master.DTO.GarmentRemark;
using FLS.LC.Cloud.Master.DTO.GarmentReturnCause;
using FLS.LC.Cloud.Master.DTO.StoreHoliday;
using System;
using System.Collections.Generic;
using System.Text;

namespace FLS.LS.Cloud.Master.BusinessService.Mapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            AllowNullDestinationValues = true;
            CreateMap<UserStoreDetail, UserStoreDetailDto>().ReverseMap();
            CreateMap<GarmentspriceConfiguration, GarmentspriceConfigurationReadDto>().ReverseMap();
            CreateMap<GarmentsRemark, GarmentRemarkRequestDto>().ReverseMap();
            CreateMap<GarmentCheckingStaff, GarmentCheckingStaffResponseDto>().ReverseMap();
            CreateMap<GarmentCheckingStaff, GarmentCheckingStaffRequestDto>().ReverseMap();
            CreateMap<GarmentReturnCause, GarmentReturnCauseResponseDto>().ReverseMap();
            CreateMap<UserStoreHoliday, StoreHolidayResponseDto>().ReverseMap();
            
            //CreateMap<OrderScreenConfiguration, OrderScreenConfigurationDto>().ReverseMap();
            //CreateMap<StickerConfiguration, StickerConfigurationDto>().ReverseMap();

            // CreateMap<Users, RegUsers>().ReverseMap();
            //CreateMap<Users, UserReg>().IgnoreAllPropertiesWithAnInaccessibleSetter()
            //    .ForMember(ev => ev.UserName, m => m.MapFrom(a => a.UsrUserName))
            //    .ForMember(ev => ev.Password, m => m.MapFrom(a => a.UsrPassword))
            //    .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.UsrUserEmail))
            //     .ForMember(des => des.PhoneNo, option => option.MapFrom(src => src.UsrUserPhone))
            //     .ForMember(des => des.IsActive, option => option.MapFrom(src => src.UsrIsActive));

            //   CreateMap<Sample, SampleDto>().ReverseMap();
            //CreateMap<PayorEntity, PayorEntityDto>().ReverseMap();

        }
    }
}

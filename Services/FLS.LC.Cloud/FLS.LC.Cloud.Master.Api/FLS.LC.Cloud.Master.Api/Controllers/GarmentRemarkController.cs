﻿using AutoMapper;
using FLS.LC.Cloud.Master.DTO.GarmentRemark;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Cloud.Master.Api.Controllers
{
    /// <summary>
    /// Garment Remark Controller
    /// </summary>
    [Route("GarmentRemark")]
    public class GarmentRemarkController : ControllerBase
    {
        private IGarmentRemark _garmentRemark;
        private IMapper _mapper;
        /// <summary>
        /// Constructor for GarmentRemark Controller
        /// </summary>
        public GarmentRemarkController(IGarmentRemark IGarmentRemark, IMapper mapper)
        {
            _garmentRemark = IGarmentRemark;
            _mapper = mapper;
        }
        /// <summary>
        /// API to careate new Garment Information   
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IActionResult))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("GarmentRemark")]
        public async Task<IActionResult> GarmentRemarkAsync([FromBody] GarmentRemarkPostRequestDto garmentRemarkPostRequestDto)
        {
            try
            {
                var operationResult = await _garmentRemark.AddGarmentRemarkAsync(garmentRemarkPostRequestDto);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
        /// <summary>
        /// API to Inactivate the Registered Garment Remark   
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IActionResult))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("InactivateGarmentRemark")]
        public async Task<IActionResult> InactivateGarmentRemarkAsync(GarmentRemarkRequestDto garmentRemarkRequestDto)
        {
            try
            {
                var operationResult = await _garmentRemark.InactivateGarmentRemarkAsync(garmentRemarkRequestDto);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<GarmentRemarkRequestDto>))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpGet, Route("GetRequiredGarment")]
        public async Task<IActionResult> GetRequiredGarmentAsync()
        {
            try
            {
                var operationResult = await _garmentRemark.GetRegisteredGarmentRemarkAsync();
                return StatusCode((int)StatusCodes.Status200OK, operationResult.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }

    }
}

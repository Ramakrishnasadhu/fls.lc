﻿using AutoMapper;
using FLS.LC.Cloud.Master.DTO;
using FLS.LC.Cloud.Master.DTO.StickerConfig;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace FLS.LC.Cloud.Master.Api.Controllers
{
    /// <summary>
    /// StickerConfiguration Controller
    /// </summary>
    [Route("StickerConfig")]
    [ApiController]
    public class StickerConfigController : ControllerBase
    {
        private IStickerConfig _stickerconfig;
        private IMapper _mapper;
        /// <summary>
        /// Constructor for Store Controller
        /// </summary>
        public StickerConfigController(IStickerConfig IStickerConfig, IMapper mapper)
        {
            _stickerconfig = IStickerConfig;
            _mapper = mapper;
        }
        #region C O N ST A N T S 
        private const string HEADER_CONSTANT_XCOUNT = "X-Total-Count";
        #endregion
        /// <summary>
        /// API to Get Selected Store Sticker Configuration  
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(StickerConfigurationObject))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("SelectedStoreStickerConfig")]
        public async Task<ActionResult> SelectedStoreStickerConfigAsync(UserStoreDto storesModel)
        {
            try
            {
                var response = await _stickerconfig.SelectedStoreStickerConfigAsync(storesModel.UsrEmail, storesModel.UstStoreName);
                this.Response.Headers.Add(HEADER_CONSTANT_XCOUNT, response.XTotalCount);
                return StatusCode((int)response.StatusCode, response.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
        /// <summary>
        /// API to Post Sticker Configuration Information   
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseMessage))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("PostStickerConfig")]
        public async Task<ActionResult> PostStickerConfigAsync(StickerConfigurationObject StickerConfigurationObject)
        {
            try
            {
                var operationResult = await _stickerconfig.PostStickerConfigAsync(StickerConfigurationObject);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
        /// <summary>
        /// API to Get Sticker Configuration Information   
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<string>))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("AllStickerConfig")]
        public async Task<ActionResult> GetAllStickerConfiConfigAsync(StickerConfigDto StickerConfigDto)
        {
            try
            {
                List<string> operationResult = await _stickerconfig.GetAllStickerConfiConfigAsync(StickerConfigDto);
                return StatusCode((int)HttpStatusCode.OK, operationResult);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
    }
}

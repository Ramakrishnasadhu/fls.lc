﻿using AutoMapper;
using FLS.LC.Cloud.Master.DTO;
using FLS.LC.Cloud.Master.DTO.OrderConfig;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Cloud.Master.Api.Controllers
{
    /// <summary>
    /// OrderConfiguration Controller
    /// </summary>
    [Route("OrderConfig")]
    [ApiController]
    public class OrderConfigController : ControllerBase
    {
        private IOrderConfig _orderconfig;
        private IMapper _mapper;
        /// <summary>
        /// Constructor for Store Controller
        /// </summary>
        public OrderConfigController(IOrderConfig IOrderConfig, IMapper mapper)
        {
            _orderconfig = IOrderConfig;
            _mapper = mapper;
        }
        #region C O N ST A N T S 
        private const string HEADER_CONSTANT_XCOUNT = "X-Total-Count";
        #endregion
        /// <summary>
        /// API to Get all Store Order Configuration Information   
        /// </summary>
        //[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<OrderScreenConfigurationDto>))]
        //[ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        //[ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        //[ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        //[Produces("application/json")]
        //[HttpPost, Route("GetAllStoreOrderConfig")]
        //public async Task<ActionResult> GetAllStoreOrderConfigAsync(UserStoresOrderConfigDto UserStoresOrderConfigDto)
        //{
        //    try
        //    {
        //        var response = await _orderconfig.GetAllStoreOrderConfigAsync(UserStoresOrderConfigDto.UsrEmail);
        //        this.Response.Headers.Add(HEADER_CONSTANT_XCOUNT, response.XTotalCount);
        //        return StatusCode((int)response.StatusCode, response.ResultSet);
        //    }
        //    catch (Exception ex)
        //    {
        //        var _exceptionResult = new Result<object>(ex);
        //        return StatusCode(500, _exceptionResult);
        //    }
        //}
        //[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IActionResult))]
        //[ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        //[ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        //[ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        //[Produces("application/json")]
        //[HttpPost, Route("AddEditDelete")]
        //public async Task<IActionResult> OrderConfigOperationAsync(OrderConfigDto OrderConfigDto)
        //{
        //    try
        //    {
        //        var operationResult = await _orderconfig.OrderConfigOperationAsync(OrderConfigDto);
        //        if (operationResult.CompletedWithSuccess)
        //        {
        //            return StatusCode(StatusCodes.Status200OK);
        //        }
        //        else
        //        {
        //            return BadRequest(operationResult.OperationError);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        var _exceptionResult = new Result<object>(ex);
        //        return StatusCode(500, _exceptionResult);
        //    }
        //}
        ///// <summary>
        ///// API to Get Selected Store Config Details   
        ///// </summary>
        //[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(OrderScreenConfigurationDto))]
        //[ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        //[ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        //[ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        //[Produces("application/json")]
        //[HttpPost, Route("SelectedStoreOrderConfigDetails")]
        //public async Task<ActionResult> SelectedStoreAsync(UserStoresOrderConfigDto UserStoresOrderConfigDto)
        //{
        //    try
        //    {
        //        var response = await _orderconfig.SelectedStoreOrderConfigDetailsAsync(UserStoresOrderConfigDto.UsrEmail, UserStoresOrderConfigDto.UstStoreName);
        //        this.Response.Headers.Add(HEADER_CONSTANT_XCOUNT, response.XTotalCount);
        //        return StatusCode((int)response.StatusCode, response.ResultSet);
        //    }
        //    catch (Exception ex)
        //    {
        //        var _exceptionResult = new Result<object>(ex);
        //        return StatusCode(500, _exceptionResult);
        //    }
        //}
    }
}

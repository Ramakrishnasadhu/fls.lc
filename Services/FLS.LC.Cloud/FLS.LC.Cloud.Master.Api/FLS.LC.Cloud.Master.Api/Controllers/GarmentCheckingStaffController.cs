﻿using AutoMapper;
using FLS.LC.Cloud.Master.DTO.GarmentCheckingStaff;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Cloud.Master.Api.Controllers
{
    /// <summary>
    /// Garment Checking Staff Controller
    /// </summary>
    [Route("GarmentCheckingStaff")]

    public class GarmentCheckingStaffController : ControllerBase
    {
        private IGarmentCheckingStaff _garmentCheckingStaff;
        private IMapper _mapper;
        /// <summary>
        /// Constructor for GarmentRemark Controller
        /// </summary>
        public GarmentCheckingStaffController(IGarmentCheckingStaff IGarmentCheckingStaff, IMapper mapper)
        {
            _garmentCheckingStaff = IGarmentCheckingStaff;
            _mapper = mapper;
        }
        /// <summary>
        /// API to careate new Garment Checking Staff Information   
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IActionResult))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("GarmentCheckingStaff")]
        public async Task<IActionResult> GarmentCheckingStaff([FromBody] GarmentCheckingStaffRequestDto garmentCheckingStaffRequestDto)
        {
            try
            {
                var operationResult = await _garmentCheckingStaff.AddGarmentCheckingStaffAsync(garmentCheckingStaffRequestDto);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<GarmentCheckingStaffResponseDto>))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpGet, Route("GetRegisteredGarmentCheckingStaff")]
        public async Task<IActionResult> GetRegisteredGarmentCheckingStaffAsync()
        {
            try
            {
                var operationResult = await _garmentCheckingStaff.GetRegisteredGarmentCheckingStaffAsync();
                return StatusCode((int)StatusCodes.Status200OK, operationResult.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
    }
}

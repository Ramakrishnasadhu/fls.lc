﻿using AutoMapper;
using FLS.LC.Cloud.Master.DTO;
using FLS.LC.Cloud.Master.DTO.GarmentConfig;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace FLS.LC.Cloud.Master.Api.Controllers
{
    /// <summary>
    /// OrderConfiguration Controller
    /// </summary>
    [Route("GarmentConfig")]
    [ApiController]
    public class GarmentConfigController : ControllerBase
    {
        private IGarmentConfig _garmentconfig;
        private IMapper _mapper;
        /// <summary>
        /// Constructor for Store Controller
        /// </summary>
        public GarmentConfigController(IGarmentConfig IGarmentConfig, IMapper mapper)
        {
            _garmentconfig = IGarmentConfig;
            _mapper = mapper;
        }
        #region C O N ST A N T S 
        private const string HEADER_CONSTANT_XCOUNT = "X-Total-Count";
        #endregion
        /// <summary>
        /// API to careate new Garment Information   
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IActionResult))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("PostGarment")]
        public async Task<IActionResult> PostGarmentConfigAsync([FromForm] GarmentslistConfigurationInput GarmentslistConfigurationInput)
        {
            try
            {
                var GlcGarmentImage = GarmentslistConfigurationInput.GlcGarmentImage.OpenReadStream();

                var GarmentslistConfigurationDto = new GarmentslistConfigurationDto()
                {
                    GpcUsremail = GarmentslistConfigurationInput.GlcUsremail,
                    GpcUstname = GarmentslistConfigurationInput.GlcUstname,
                    GpcGarmentName = GarmentslistConfigurationInput.GlcGarmentName,
                    GlcGarmentServiceName = GarmentslistConfigurationInput.GlcGarmentServiceName,
                    GpcGarmentCode = GarmentslistConfigurationInput.GlcGarmentShortName,
                    GlcGarmentImage = GlcGarmentImage,
                    GpcGarmentMeasurementUnit = GarmentslistConfigurationInput.GlcGarmentMeasurementUnit,
                    GpcGarmentCategory = GarmentslistConfigurationInput.GlcGarmentCategory,
                    GpcNoOfGarments = GarmentslistConfigurationInput.GlcGarmentNoOfGarments,
                    GpcIsenable = GarmentslistConfigurationInput.GpcIsenable,
                };
                var operationResult = await _garmentconfig.UpdateGarmentConfigAsync(GarmentslistConfigurationDto);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }

        /// <summary>
        /// API to Import Garments Price List   
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IActionResult))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("ImportGarmentsPriceList")]
        public async Task<IActionResult> ImportGarmentsPriceList([FromForm] GarmentspriceConfigurationDto GarmentspriceConfigurationDto)
        {
            try
            {
                var operationResult = await _garmentconfig.ImportGarmentsPriceListAsync(GarmentspriceConfigurationDto);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }

            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }

        /// <summary>
        /// API to Get All Registered Garments  
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<GarmentslistConfigurationDto>))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("AllRegisteredGarments")]
        public async Task<ActionResult> GetAllRegisteredGarmentsListAsync(GarmentListDto GarmentListDto)
        {
            try
            {
                var operationResult = await _garmentconfig.GetAllRegisteredGarmentsListAsync(GarmentListDto);
                this.Response.Headers.Add(HEADER_CONSTANT_XCOUNT, operationResult.XTotalCount);
                return StatusCode((int)HttpStatusCode.OK, operationResult.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }


        /// <summary>
        /// API to Get Registered Price List Name based on registered Store  
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseMessage))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("RegisteredPriceListByStore")]
        public async Task<IActionResult> GetRegisteredPriceListByStoreAsync(GarmentListDto GarmentListDto)
        {
            try
            {
                var operationResult = await _garmentconfig.GetRegisteredPriceListByStoreAsync(GarmentListDto);
                this.Response.Headers.Add(HEADER_CONSTANT_XCOUNT, operationResult.XTotalCount);
                return StatusCode((int)HttpStatusCode.OK, operationResult.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
        /// <summary>
        /// API to Modify by percentage Registered Price List  based on registered Store  
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseMessage))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("updateRegisteredPriceListByStore")]
        public async Task<IActionResult> updateRegisteredPriceListByStoreAsync(updateRegisteredPriceListByStoreDto updateRegisteredPriceListByStoreDto)
        {
            try
            {
                var operationResult = await _garmentconfig.updateRegisteredPriceListByStoreAsync(updateRegisteredPriceListByStoreDto);
                this.Response.Headers.Add(HEADER_CONSTANT_XCOUNT, operationResult.XTotalCount);
                return StatusCode((int)HttpStatusCode.OK, operationResult.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }

        /// <summary>
        /// API to Update Registered GarmentPrice based on registered Store  
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseMessage))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("UpdateServicePriceByGarment")]
        public async Task<IActionResult> UpdateServicePriceByGarmentAsync(UpdateServicePriceByGarmentDto updateServicePriceByGarmentDto)
        {
            try
            {
                var operationResult = await _garmentconfig.UpdateServicePriceByGarmentAsync(updateServicePriceByGarmentDto);
                this.Response.Headers.Add(HEADER_CONSTANT_XCOUNT, operationResult.XTotalCount);
                return StatusCode((int)HttpStatusCode.OK, operationResult.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }

    }
}

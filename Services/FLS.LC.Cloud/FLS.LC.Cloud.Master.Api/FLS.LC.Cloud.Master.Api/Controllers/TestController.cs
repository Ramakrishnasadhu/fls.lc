﻿using FLS.LC.Cloud.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Cloud.Master.Api.Controllers
{
    [Route("Test")]
    [ApiController]
    public class TestController : ControllerBase
    {
        /// <summary>
        /// API to test service
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpGet(nameof(TestServicesync))]
        public string TestServicesync()
        {
            return "I am Live";
        }
    }
}

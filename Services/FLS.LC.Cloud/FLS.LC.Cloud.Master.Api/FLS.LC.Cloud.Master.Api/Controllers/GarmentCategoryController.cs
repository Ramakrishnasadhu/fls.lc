﻿using AutoMapper;
using FLS.LC.Cloud.Master.DTO;
using FLS.LC.Cloud.Master.DTO.GarmentCategoryConfig;
using FLS.LC.Cloud.Master.DTO.GarmentConfig;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Cloud.Master.Api.Controllers
{
    /// <summary>
    /// OrderConfiguration Controller
    /// </summary>
    [Route("GarmentCategory")]
    [ApiController]
    public class GarmentCategoryController : ControllerBase
    {
        private IGarmentCategoryConfig _garmentcategoryconfig;
        private IMapper _mapper;
        /// <summary>
        /// Constructor for Store Controller
        /// </summary>
        public GarmentCategoryController(IGarmentCategoryConfig IGarmentCategoryConfig, IMapper mapper)
        {
            _garmentcategoryconfig = IGarmentCategoryConfig;
            _mapper = mapper;
        }

        #region C O N ST A N T S 
        private const string HEADER_CONSTANT_XCOUNT = "X-Total-Count";
        #endregion

        /// <summary>
        /// API to careate new Garment Information   
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IActionResult))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("ProcessGarmentCategory")]
        public async Task<IActionResult> GarmentCategoryConfigAsync(GarmentsCategoryDto GarmentsCategoryInput)
        {
            try
            {
                var operationResult = await _garmentcategoryconfig.GarmentCategoryConfigAsync(GarmentsCategoryInput);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GarmentspriceConfigurationReadDto))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("GetRequiredGarment")]
        public async Task<IActionResult> GetRequiredGarmentAsync(RequiredGarmentDto requiredGarmentDto)
        {
            try
            {
                var operationResult = await _garmentcategoryconfig.GetRequiredGarmentAsync(requiredGarmentDto);
                return StatusCode((int)StatusCodes.Status200OK, operationResult.Result);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }

        /// <summary>
        /// API to Get Registered All Garments List Categories   
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<RegisteredGarmentspriceConfigurationDto>))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("RegisteredGarmentListService")]
        public async Task<ActionResult> RegisteredGarmentListServiceAsync(GarmentCategoryDto GarmentCategoryDto)
        {
            try
            {
                var response = await _garmentcategoryconfig.RegisteredGarmentListServiceAsync(GarmentCategoryDto);
                this.Response.Headers.Add(HEADER_CONSTANT_XCOUNT, response.XTotalCount);
                return StatusCode((int)response.StatusCode, response.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }

        /// <summary>
        /// API to Get All Registered Garments Names List    
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<RegisteredGarmentspriceConfigurationDto>))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("SearchRegisteredGarmentNamesListService")]
        public async Task<ActionResult> RegisteredGarmentNamesListServiceAsync(GarmentCategoryDto GarmentCategoryDto)
        {
            try
            {
                var response = await _garmentcategoryconfig.RegisteredGarmentNamesListAsync(GarmentCategoryDto);
                this.Response.Headers.Add(HEADER_CONSTANT_XCOUNT, response.XTotalCount);
                return StatusCode((int)response.StatusCode, response.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }

        /// <summary>
        /// API to Get Registered All Garments Categories List    
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<string>))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("RegisteredGarmentCategory")]
        public async Task<ActionResult> RegisteredGarmentNamesListCategoryAsync(GarmentCategoryDto GarmentCategoryDto)
        {
            try
            {
                var response = await _garmentcategoryconfig.RegisteredGarmentCategorysAsync(GarmentCategoryDto);
                this.Response.Headers.Add(HEADER_CONSTANT_XCOUNT, response.XTotalCount);
                return StatusCode((int)response.StatusCode, response.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
    }
}

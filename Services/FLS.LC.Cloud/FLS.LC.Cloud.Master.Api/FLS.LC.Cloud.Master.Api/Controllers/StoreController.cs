﻿using AutoMapper;
using FLS.LC.Cloud.Master.DTO;
using FLS.LC.Cloud.Master.DTO.Store;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FLS.LC.Cloud.Master.Api.Controllers
{
    /// <summary>
    /// Store Controller
    /// </summary>
    [Route("Store")]
    [ApiController]
    public class StoreController : ControllerBase
    {
        private IStore _store;
        private IMapper _mapper;
        /// <summary>
        /// Constructor for Store Controller
        /// </summary>
        public StoreController(IStore IStore, IMapper mapper)
        {
            _store = IStore;
            _mapper = mapper;
        }
        #region C O N ST A N T S 
        private const string HEADER_CONSTANT_XCOUNT = "X-Total-Count";
        #endregion
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IActionResult))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("AddEditDelete")]
        public async Task<IActionResult> StoreOperationAsync([FromForm]  StoreInput StoreInput)
        {
            try
            {
                var fileStream = StoreInput.LogoImage.OpenReadStream();

                var userStore = new UserStoreDetailDto()
                {
                    UsrEmail = StoreInput.UsrEmail,
                    UstEmail= StoreInput.UstEmail,
                    Operation = StoreInput.Operation,
                    UstLogo = fileStream,
                    UstName= StoreInput.UstName,
                    UstCode= StoreInput.UstCode,
                    UstWebsiteUrl= StoreInput.UstWebsiteUrl,
                    UstBusinessName = StoreInput.UstBusinessName,
                    UstAddress= StoreInput.UstAddress,
                    UstMobileNo= StoreInput.UstMobileNo,
                    UstAreaLocation=StoreInput.UstAreaLocation,
                    UstCity = StoreInput.UstCity,
                    UstState = StoreInput.UstState,
                    UstOpeningTime= StoreInput.UstOpeningTime,
                    UstClosingTime= StoreInput.UstClosingTime,
                    UstBusinessSlogan = StoreInput.UstBusinessSlogan,
                    UstWeekOff= StoreInput.UstWeekOff,
                };
                var operationResult = await _store.StoreOperationAsync(userStore);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }

        /// <summary>
        /// API to Get Registered Stores   
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<UserStoreDetailDto>))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("RegisteredStores")]
        public async Task<ActionResult> RegisteredStoresAsync(UserStoreDto storesModel)
        {
            try
            {
                var response = await _store.RegisteredStoresAsync(storesModel.UsrEmail);
                this.Response.Headers.Add(HEADER_CONSTANT_XCOUNT, response.XTotalCount);
                return StatusCode((int)response.StatusCode, response.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
        /// <summary>
        /// API to Get Selected Store   
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserStoreDetailDto))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("SelectedStore")]
        public async Task<ActionResult> SelectedStoreAsync(UserStoreDto storesModel)
        {
            try
            {
                var response = await _store.SelectedStoresAsync(storesModel.UsrEmail, storesModel.UstStoreName);
                this.Response.Headers.Add(HEADER_CONSTANT_XCOUNT, response.XTotalCount);
                return StatusCode((int)response.StatusCode, response.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
        /// <summary>
        /// API to Inactivate Selected Store   
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseMessage))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("InActivateSelectedStore")]
        public async Task<ActionResult> InActivateSelectedStoreAsync(UserStoreDto storesModel)
        {
            try
            {
                var operationResult = await _store.InActivateSelectedStoreAsync(storesModel.UsrEmail, storesModel.UstStoreName);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
    }
}

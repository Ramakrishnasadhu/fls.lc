﻿using AutoMapper;
using FLS.LC.Cloud.Master.DTO.StoreHoliday;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Cloud.Master.Api.Controllers
{
    /// <summary>
    /// Garment Remark Controller
    /// </summary>
    [Route("StoreHoliday")]
    public class StoreHolidayController : ControllerBase
    {
        private IStoreHoliday _storeholiday;
        private IMapper _mapper;
        /// <summary>
        /// Constructor for StoreHoliday Controller
        /// </summary>
        public StoreHolidayController(IStoreHoliday storeholiday, IMapper mapper)
        {
            _storeholiday = storeholiday;
            _mapper = mapper;
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<StoreHolidayResponseDto>))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("GetStoreHoliday")]
        public async Task<IActionResult> GetStoreHolidayAsync([FromBody] StoreHolidayRequestDto storeHolidayRequestDto)
        {
            try
            {
                var operationResult = await _storeholiday.GetRegisteredStoreHolidayAsync(storeHolidayRequestDto.UsthUsremail);
                return StatusCode((int)StatusCodes.Status200OK, operationResult.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }


        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<StoreHolidayResponseDto>))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("GetStoreHolidayByStore")]
        public async Task<IActionResult> GetStoreHolidayByStoreAsync([FromBody] StoreHolidayRequestDto storeHolidayRequestDto)
        {
            try
            {
                var operationResult = await _storeholiday.GetRegisteredStoreHolidayBySelectedStoreAsync(storeHolidayRequestDto.UsthUsremail, storeHolidayRequestDto.UsthUstname);
                return StatusCode((int)StatusCodes.Status200OK, operationResult.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<StoreHolidayResponseDto>))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("PostStoreWeekOffHoliday")]
        public async Task<IActionResult> AddStoreWeekOffHolidayAsync([FromBody] StoreWeekOffHolidayRequestDto storeWeekOffHolidayRequestDto)
        {
            try
            {
                var operationResult = await _storeholiday.AddStoreWeekOffHolidayAsync(storeWeekOffHolidayRequestDto);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(StoreHolidayWeekOffResponseDto))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("GetStoreWeekOffHoliday")]
        public async Task<IActionResult> GetStoreWeekOffHolidayAsync([FromBody] StoreHolidayRequestDto storeWeekOffRequestDto)
        {
            try
            {
                var operationResult = await _storeholiday.GetStoreWeekOffHolidayAsync(storeWeekOffRequestDto.UsthUsremail, storeWeekOffRequestDto.UsthUstname);
                return StatusCode((int)StatusCodes.Status200OK, operationResult.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }

        /// <summary>
        /// API to careate new Add Store Holiday Information   
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IActionResult))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("AddStoreHoliday")]
        public async Task<IActionResult> AddStoreHolidayAsync([FromBody] StoreHolidayRequestDto StoreHolidayRequestDto)
        {
            try
            {
                var operationResult = await _storeholiday.AddStoreHolidayAsync(StoreHolidayRequestDto);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }

    }
}

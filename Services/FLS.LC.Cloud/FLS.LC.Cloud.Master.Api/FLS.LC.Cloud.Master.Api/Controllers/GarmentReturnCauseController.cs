﻿using AutoMapper;
using FLS.LC.Cloud.Master.DTO.GarmentReturnCause;
using FLS.LC.Cloud.Utilities;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FLS.LC.Cloud.Master.Api.Controllers
{
    /// <summary>
    /// Garment Remark Controller
    /// </summary>
    [Route("GarmentReturnCause")]
    public class GarmentReturnCauseController : ControllerBase
    {
        private IGarmentReturnCause _garmentReturnCause;
        private IMapper _mapper;
        /// <summary>
        /// Constructor for GarmentRemark Controller
        /// </summary>
        public GarmentReturnCauseController(IGarmentReturnCause garmentReturnCause, IMapper mapper)
        {
            _garmentReturnCause = garmentReturnCause;
            _mapper = mapper;
        }
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<GarmentReturnCauseResponseDto>))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpGet, Route("GetGarmentReturnCause")]
        public async Task<IActionResult> GetRequiredGarmentAsync()
        {
            try
            {
                var operationResult = await _garmentReturnCause.GetRegisteredGarmentReturnCauseAsync();
                return StatusCode((int)StatusCodes.Status200OK, operationResult.ResultSet);
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
        /// <summary>
        /// API to careate new Garment Information   
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IActionResult))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<object>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<object>))]
        [Produces("application/json")]
        [HttpPost, Route("GarmentReturnCause")]
        public async Task<IActionResult> GarmentReturnCauseAsync([FromBody] GarmentReturnCausePostDto garmentReturnCausePostDto)
        {
            try
            {
                var operationResult = await _garmentReturnCause.AddGarmentReturnCauseAsync(garmentReturnCausePostDto);
                if (operationResult.CompletedWithSuccess)
                {
                    return StatusCode(StatusCodes.Status200OK);
                }
                else
                {
                    return BadRequest(operationResult.OperationError);
                }
            }
            catch (Exception ex)
            {
                var _exceptionResult = new Result<object>(ex);
                return StatusCode(500, _exceptionResult);
            }
        }
    }
}

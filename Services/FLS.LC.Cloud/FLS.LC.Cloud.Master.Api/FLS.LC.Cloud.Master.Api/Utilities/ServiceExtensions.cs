﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using FLS.LC.Cloud.Master.DataModels.DataContext;
using FLS.LC.Cloud.Repository.UnitOfWork;
using FLS.LS.Cloud.Master.BusinessService.Interfaces;
using FLS.LS.Cloud.Master.BusinessService.Services;

namespace FLS.LC.Cloud.Master.Api.Utilities
{
    public static class ServiceExtensions
    {
        /// <summary>
        /// Register All the Components with this Extension Method, All the Business Services and Repositories used in this Project
        /// must be registered here for Enabling Dependency Injection
        /// </summary>
        /// <param name="services"></param>
        /// <returns>Service Collection</returns>
        public static IServiceCollection RegisterServices(
            this IServiceCollection services)
        {
            services.AddTransient<IOrderConfig, OrderConfigService>();
            services.AddTransient<IStore, StoreService>();
            services.AddTransient<IStickerConfig, StickerConfigService>();
            services.AddTransient<IGarmentConfig, GarmentConfigService>();
            services.AddTransient<IGarmentCategoryConfig, GarmentCategoryConfigService>();
            services.AddTransient<IGarmentRemark, GarmentRemarkService>();
            services.AddTransient<IGarmentCheckingStaff, GarmentCheckingStaffService>();
            services.AddTransient<IGarmentReturnCause, GarmentReturnCauseService>();
            services.AddTransient<IStoreHoliday, StoreHolidayService>();
            return services;
        }

        /// <summary>
        /// Register DB Context for the Project in this Extension Method.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="connectionString">Connection String for the Main Moduel DB</param>
        /// <returns></returns>
        public static IServiceCollection RegisterDatabaseContext(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<MasterContext>(options =>
            {
                options.UseSqlServer(connectionString);
            })
           .AddUnitOfWork<MasterContext>();
            return services;
        }
    }
}

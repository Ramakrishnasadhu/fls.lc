﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FLS.LC.Cloud.Master.DTO.GarmentCheckingStaff
{
    public class GarmentCheckingStaffRequestDto
    {
        public string GpcUsremail { get; set; }
        public string GpcUstname { get; set; }
        public string GcsEmployeeName { get; set; }
        public string GcsEmployeeAddress { get; set; }
        public long? GcsEmployeeMobileNo { get; set; }
        public string GcsEmployeeEmail { get; set; }
    }
}

using System;
using System.Linq;
namespace FLS.LC.Cloud.Master.DTO
{
    public class GarmentsCategoryDto
    {
        public int Operation { get; set; }
        public long GcId { get; set; }

        public string GcUsremail { get; set; }

        public string GcUstname { get; set; }

        public string GcGarmentCategory { get; set; }

        public DateTime? CreatedOn { get; set; }

        public long? CreatedBy { get; set; }

        public DateTime? DeletedOn { get; set; }

        public long? DeletedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public long? ModifiedBy { get; set; }
    }
}
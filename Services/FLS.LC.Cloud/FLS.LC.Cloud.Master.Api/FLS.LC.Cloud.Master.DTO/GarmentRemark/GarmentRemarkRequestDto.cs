﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FLS.LC.Cloud.Master.DTO.GarmentRemark
{
    public class GarmentRemarkRequestDto
    {
        public string GreRemarkDescription { get; set; }
        public int Gre_ID { get; set; }
    }

    public class GarmentRemarkPostRequestDto
    {
        public string garmentRemarkDescriptionOldValue { get; set; }
        public string garmentRemarkDescriptionNewValue { get; set; }
        public int garmentRemarkId { get; set; }
    }
}

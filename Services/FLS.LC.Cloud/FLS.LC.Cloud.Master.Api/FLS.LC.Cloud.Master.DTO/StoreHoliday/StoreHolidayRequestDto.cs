﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FLS.LC.Cloud.Master.DTO.StoreHoliday
{
    public class StoreHolidayRequestDto
    {
        public string UsthUsremail { get; set; }
        public string UsthUstname { get; set; }
        public string UsthHolidayDate { get; set; }
        public string UsthHolidayDescription { get; set; }
    }

    public class StoreWeekOffHolidayRequestDto
    {
        public string UsthUsremail { get; set; }
        public string UsthUstname { get; set; }
        public string UsthWeekOffHoliday { get; set; }
    }
}

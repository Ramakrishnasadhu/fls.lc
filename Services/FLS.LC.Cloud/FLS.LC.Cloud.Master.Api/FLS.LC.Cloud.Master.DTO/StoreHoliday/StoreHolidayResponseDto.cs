﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FLS.LC.Cloud.Master.DTO.StoreHoliday
{
    public class StoreHolidayResponseDto
    {
        public string UsthUsremail { get; set; }
        public string UsthUstname { get; set; }
        public string UsthHolidayDescription { get; set; }
        public string UsthHolidayDate { get; set; }
    }
    public class StoreHolidayWeekOffResponseDto
    {
        public string UsthUsremail { get; set; }
        public string UsthUstname { get; set; }
        public string UstWeekOffDay { get; set; }
        public int? UstWeekOffDayNo { get; set; }
    }

}

﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace FLS.LC.Cloud.Master.DTO.Store
{
    public class StoreInput
    {
        public IFormFile LogoImage { get; set; }
        public int Operation { get; set; }
        public string UsrEmail { get; set; }
        public string UstEmail { get; set; }
        public string UstName { get; set; }
        public string UstCode { get; set; }
        public string UstWebsiteUrl { get; set; }
        public string UstBusinessName { get; set; }
        public string UstAddress { get; set; }
        public long UstMobileNo { get; set; }
        public string UstAreaLocation { get; set; }
        public string UstCity { get; set; }
        public string UstState { get; set; }
        public string UstOpeningTime { get; set; }
        public string UstClosingTime { get; set; }
        public string UstBusinessSlogan { get; set; }
        public string UstWeekOff { get; set; }
    }
}

using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;


namespace FLS.LC.Cloud.Master.DTO
{
    public class UserStoreDto
    {
        public string UsrEmail { get; set; }
        public string UstStoreName { get; set; }
    }
    public class UserStoresOrderConfigDto
    {
        public string UsrEmail { get; set; }
        public string UstStoreName { get; set; }
    }
    public class UserStoreDetailDto
    {
        public int Operation { get; set; }
        public long UstId { get; set; }

        public string UstName { get; set; }

        public string UstCode { get; set; }

        public string UstBusinessName { get; set; }

        public string UstAddress { get; set; }

        public long? UstMobileNo { get; set; }
        public string UsrEmail { get; set; }

        public string UstEmail { get; set; }

        public string UstAreaLocation { get; set; }

        public string UstCity { get; set; }

        public string UstState { get; set; }

        public string UstCountry { get; set; }

        public string UstOpeningTime { get; set; }

        public string UstClosingTime { get; set; }

        public string UstBusinessSlogan { get; set; }

        public Stream UstLogo { get; set; }

        public bool? UstISdelete { get; set; }

        public string UstWebsiteUrl { get; set; }
        public string UstWeekOff { get; set; }

        public bool? UstOperatingtime { get; set; }

        public long? UsrId { get; set; }

        public DateTime? CreatedOn { get; set; }

        public long? CreatedBy { get; set; }

        public DateTime? DeletedOn { get; set; }

        public long? DeletedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public long? ModifiedBy { get; set; }
    }
}
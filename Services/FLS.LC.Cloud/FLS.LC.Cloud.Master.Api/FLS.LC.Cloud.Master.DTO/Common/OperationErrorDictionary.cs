﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FLS.LC.Cloud.Master.DTO.Common
{
    public static class OperationErrorDictionary
    {
        public static class GarmentCategoryNotExist
        {
            public static OperationError UserDoesNotExist() =>
               new OperationError("Unfortunately the user specified in the system does not exist.");
            public static OperationError UserStoreAlreadyExist() =>
              new OperationError("Unfortunately the user specified Store Name Already Exist in the system.");

            public static OperationError UserStoreNotExist() =>
              new OperationError("Unfortunately the user specified Store Name Not Exist in the system.");

            public static OperationError UserStoreInvalidOperation() =>
              new OperationError("Unfortunately Invalid Operation Exist in the system.");
        }

        public static class GarmentCheckingStaff
        {
            public static OperationError UserDoesNotExist() =>
               new OperationError("Unfortunately the user specified in the system does not exist.");
            public static OperationError GarmentCheckingStaffAlreadyExist() =>
              new OperationError("Unfortunately the user specified Employee Name Already Exist in the system.");

            public static OperationError GarmentCheckingStaffNotExist() =>
              new OperationError("Unfortunately the user specified Emploee Not Exist in the system.");

            public static OperationError GarmentCheckingStaffInvalidOperation() =>
              new OperationError("Unfortunately Invalid Operation Exist in the system.");
        }
        public static class StickerConfig
        {
            public static OperationError StickerConfigurationStoreAlreadyExist() =>
              new OperationError("Unfortunately the user specified Store Sticker Configuration Already Exist in the system.");

            public static OperationError UserDoesNotExist() =>
               new OperationError("Unfortunately the user specified in the system does not exist.");
            public static OperationError UserStoreAlreadyExist() =>
              new OperationError("Unfortunately the user specified Store Name Already Exist in the system.");

            public static OperationError UserStoreNotExist() =>
              new OperationError("Unfortunately the user specified Store Name Not Exist in the system.");

            public static OperationError UserStoreInvalidOperation() =>
              new OperationError("Unfortunately Invalid Operation Exist in the system.");
        }

        public static class OrderConfig
        {
            public static OperationError UserDoesNotExist() =>
               new OperationError("Unfortunately the user specified in the system does not exist.");

            public static OperationError OrderConfigurationStoreAlreadyExist() =>
              new OperationError("Unfortunately the user specified Store Order Configuration Already Exist in the system.");

            
        }
        public static class GarmentsListConfig
        {
            public static OperationError UserDoesNotExist() =>
               new OperationError("Unfortunately the user specified in the system does not exist.");

            public static OperationError GarmentConfigurationAlreadyExist() =>
              new OperationError("Unfortunately the user specified Garment Configuration Already Exist in the system.");

            public static OperationError GarmentConfigurationNotExist() =>
              new OperationError("Unfortunately the user specified Garment Configuration Not Exist in the system.");

        }
        public static class GarmentsCategoryConfig
        {
            public static OperationError UserDoesNotExist() =>
               new OperationError("Unfortunately the user specified in the system does not exist.");

            public static OperationError GarmentCategoryAlreadyExist() =>
              new OperationError("Unfortunately the user specified Garment Category Already Exist in the system.");

            public static OperationError GarmentCategoryNotExist() =>
              new OperationError("Unfortunately the user specified Garment Category Not Exist in the system.");

        }

    }

    }

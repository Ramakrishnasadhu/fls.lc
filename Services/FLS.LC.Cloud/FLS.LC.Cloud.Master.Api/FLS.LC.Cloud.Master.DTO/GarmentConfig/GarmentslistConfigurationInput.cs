﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FLS.LC.Cloud.Master.DTO.GarmentConfig
{
    public class GarmentslistConfigurationInput
    {
        public long GpcId { get; set; }
        public IFormFile GlcGarmentImage { get; set; }
        public string GlcUstname { get; set; }
        public string GlcUsremail { get; set; }
        public string GlcGarmentName { get; set; }
        public string GlcGarmentServiceName { get; set; }
        public string GlcGarmentShortName { get; set; }
        public string GlcGarmentMeasurementUnit { get; set; }
        public string GlcGarmentCategory { get; set; }
        public int? GlcGarmentNoOfGarments { get; set; }
        public bool GlcGarmentIsSingle { get; set; }
        public string GpcPriceListName { get; set; }
        public bool? GpcIsenable { get; set; }
        public bool? GlcGarmentIsInuse { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FLS.LC.Cloud.Master.DTO.GarmentConfig
{
    public class updateRegisteredPriceListByStoreDto
    {
        public string usrEmail { get; set; }
        public string usrStoreName { get; set; }
        public bool isIncrementChecked { get; set; }
        public bool isDecrementChecked { get; set; }
        public string pricePercentage { get; set; }
    }

    public class UpdateServicePriceByGarmentDto
    {
        public string UsrEmail { get; set; }
        public string UsrStoreName { get; set; }
        public int Updatedprice { get; set; }
        public string Garmentname { get; set; }
        public string Service { get; set; }
        public string Garment { get; set; }

    }

    
}

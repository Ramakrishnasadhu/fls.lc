using Microsoft.AspNetCore.Http;
using System;
using System.Linq;


namespace FLS.LC.Cloud.Master.DTO
{
    public class GarmentspriceConfigurationDto
    {
        public string GpcUsremail { get; set; }

        public string GpcUstname { get; set; }

        public IFormFile GpcImprtPriceList { get; set; }
    }
}
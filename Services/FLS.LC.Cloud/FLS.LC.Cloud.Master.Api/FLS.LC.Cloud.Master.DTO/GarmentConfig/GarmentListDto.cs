﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FLS.LC.Cloud.Master.DTO.GarmentConfig
{
    public class GarmentListDto
    {
        public string GlcUsremail { get; set; }
        public string GlcUsrstore { get; set; }
    }
    public class GarmentCategoryDto
    {
        public string GlcUsremail { get; set; }
        public string GlcUsrstore { get; set; }
    }
    public class RequiredGarmentDto
    {
        public string GlcUsremail { get; set; }
        public string GlcUsrstore { get; set; }
        public string GlcGarmentName { get; set; }
        public string GlcSelectedGarment { get; set; }
        public string GlcSelectedService { get; set; }
    }

}

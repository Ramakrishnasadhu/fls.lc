using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;

namespace FLS.LC.Cloud.Master.DTO
{
    public class GarmentslistConfigurationDto
    {
        public long GpcId { get; set; }
        public string GpcUsremail { get; set; }
        public string GpcUstname { get; set; }
        public string GpcPriceListName { get; set; }
        public bool? GpcIsenable { get; set; }
        public int? GpcNoOfGarments { get; set; }
        public string GpcGarmentMeasurementUnit { get; set; }
        public string GpcGarmentCategory { get; set; }
        public string GpcGarmentName { get; set; }
        public string GlcGarmentServiceName { get; set; }
        public Stream GlcGarmentImage { get; set; }
        public string GpcGarmentCode { get; set; }
        public int? GpcGarmentServicePrice { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FLS.LC.Cloud.Master.DTO.GarmentReturnCause
{
    public class GarmentReturnCauseResponseDto
    {
        public long GrecId { get; set; }
        public string GreReturnCauseDescription { get; set; }
    }

    public class GarmentReturnCausePostDto
    {
        public string garmentReturnCauseOldValue { get; set; }
        public string garmentReturnCauseNewValue { get; set; }
        public int GrecId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FLS.LC.Cloud.Master.DTO.StickerConfig
{
    public class StickerConfigurationObject
    {
        public string StcUsremail { get; set; }
        public string StcUstname { get; set; }
        public int Operation { get; set; }
        public List<StickerConfigurationDto> listStickerConfigurationDto { get; set; }
    }
}

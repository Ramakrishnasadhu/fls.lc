using System;

namespace FLS.LC.Cloud.Master.DTO
{
    public class LobRulesDto
    {
        public long LobId { get; set; }

        public string LobRuleDescription { get; set; }

        public bool? LobRuleStatus { get; set; }

        public DateTime? CreatedOn { get; set; }

        public long? CreatedBy { get; set; }

        public DateTime? DeletedOn { get; set; }

        public long? DeletedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public long? ModifiedBy { get; set; }
    }
}